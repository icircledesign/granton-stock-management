@extends('backend.layouts.admin')

@section('title',(!empty($id))? "Update" : "Create New"." Role" )

@section('pageTitle',(!empty($id))? "Update" : "Create New"." Role" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/roles')}}">Roles</a>
        </li>

		<li class="active">
			<a href="{{url()->current()}}">{{ (!empty($id))? "Update" : "Create New" }} Role</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{ (!empty($id))? url($currentUrl.'/roles/update') : url($currentUrl.'/roles/store')}}" method="POST" id="formRole">
                    {{ csrf_field() }}
                    {{ (!empty($id))? method_field('PATCH') : method_field('POST') }}
                    <input type="hidden" name="id" id="id" value="{{ (!empty($id))? $id : '' }}">
                    <div class="ibox-title">
                        <h5>{{ (!empty($id))? "Update" : "Create New" }} Role</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">
                        <div class="col-sm-5">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Name <span style="color: red;">*</span></label>
                                    <label id="name-error" class="text-danger" for="name"></label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Role Name" value="{{(!empty($role->name))? $role->name : old('name') }}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Description <span style="color: red;"></span></label>
                                    <label id="description-error" class="text-danger" for="description"></label>
                                    <textarea class="form-control" placeholder="Enter Description for this Role" name="description" cols="50" rows="10" id="description">{{(!empty($role->description))? $role->description : old('description') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Permissions(s) <span style="color: red;">*</span></label>

                                    <label id="permissionId-error" class="text-danger" for="permissionId"></label><br>
                                    @foreach ($permissions  as $permission)
                                        <div class="col-md-6">
                                            <input type="checkbox" name="permissionId[]" style="cursor: pointer;" value="{{$permission->id}}" {{(!empty($id))? in_array($permission->id,$userPermission)?"checked":""  : '' }}>
                                            <label for="permissionId" style='{{(!empty($id))? in_array($permission->id,$userPermission)?'color:#999999":"color:#000;cursor:pointer':""  : '' }} '>{{$permission->display_name}}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{ (!empty($id))? "Update" : "Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <a href="{{url($currentUrl.'/roles')}}" class="btn btn-success">Go Back</a>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>
                    
        </div>

            
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/role/form.js') }}"></script>
@endsection
