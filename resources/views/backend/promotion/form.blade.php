@extends('backend.layouts.admin')

@section('title',(!empty($id))? "Update" : "Create New"." Promotion" )

@section('pageTitle',(!empty($id))? "Update" : "Create New"." Promotion" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/promotions')}}">Promotions</a>
        </li>

		<li class="active">
			<a href="{{url()->current()}}">{{ (!empty($id))? "Update" : "Create New" }} Promotion</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{ (!empty($id))? url($currentUrl.'/promotions/update') : url($currentUrl.'/promotions/store')}}" method="POST" id="formPromotion">
                    {{ csrf_field() }}
                    {{ (!empty($id))? method_field('PATCH') : method_field('POST') }}
                    <input type="hidden" name="id" id="id" value="{{ (!empty($id))? $id : '' }}">
                    <div class="ibox-title">
                        <h5>{{ (!empty($id))? "Update" : "Create New" }} Promotion</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Promotion Name <span style="color: red;">*</span></label>
                                <label id="name-error" class="text-danger" for="name"></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter Promotion Name" value="{{(!empty($promotion->name))? $promotion->name : old('name') }}">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Promotion Code <span style="color: red;">*</span></label>
                                <label id="promotion_code-error" class="text-danger" for="promotion_code"></label>
                                <input type="text" class="form-control" name="promotion_code" id="promotion_code" placeholder="Enter Promotion Code" value="{{(!empty($promotion->promotion_code))? $promotion->promotion_code : old('promotion_code') }}">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" id="start_date_div">
                                <label>Start Date <span style="color: red;">*</span></label>
                                <label id="start_date-error" class="text-danger" for="start_date"></label>
                                <div class="input-group date">
                                    <input type="text" placeholder="Start Date" class="form-control" id="start_date" name="start_date" value="{{(!empty($promotion->start_date))? $promotion->start_date : old('start_date') }}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" id="end_date_div">
                                <label>End Date <span style="color: red;"></span></label>
                                <label id="end_date-error" class="text-danger" for="end_date"></label>
                                <div class="input-group date">
                                    <input type="text" placeholder="End Date" class="form-control" id="end_date" name="end_date" value="{{(!empty($promotion->end_date))? $promotion->end_date : old('end_date') }}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Lister Name <span style="color: red;">*</span></label>
                                <label id="lister_id-error" class="text-danger" for="lister_id"></label>
                                <select id="lister_id" data-placeholder="Select Lister Name" name="lister_id" class="form-control">
                                  <option></option>
                                  @forelse ($listers as $lister)
                                  <option value="{{ $lister->id }}" {{(!empty($id))? ($lister->id == $promotion->lister_id)?"selected":""  : '' }}>{{ $lister->user->name }}</option>
                                  @empty

                                  @endforelse
                              </select>
                            </div>
                        </div>
                        @if(!empty($id))
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Status <span style="color: red;">*</span></label>

                                <div class="i-checks">
                                    <label class="radio-inline">
                                        <input type="radio" value="1"  name="status_id" {{(!empty($promotion->status_id))? ($promotion->status_id ==1)? 'checked' : 'unchecked' : '' }} checked> Active
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" value="3" name="status_id" {{(!empty($promotion->status_id))? ($promotion->status_id ==3)? 'checked' : 'unchecked' : '' }}> Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{ (!empty($id))? "Update" : "Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <a href="{{url($currentUrl.'/promotions')}}" class="btn btn-success">Go Back</a>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>

        </div>


    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('#start_date').keydown(function() {
            return false;
        });
        $('#end_date').keydown(function(event) {
            if((event.which == 8)||(event.which == 46) ){
                return true;
            }else{
                return false;
            }
        });

        $('#lister_id').select2();

        var start, end;
        var today =  new Date();
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();

        var today = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day).length<2 ? '0' : '') + day;

        $('#start_date').val(today);
        $('#start_date_div .input-group.date').datepicker({
            todayHighlight: true,
            startDate: today,
            format: "yyyy-mm-dd",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
        }).on('changeDate', function(selectDate) {


        });

        $('#end_date_div .input-group.date').datepicker({
            startDate: today,
            format: "yyyy-mm-dd",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });


    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/promotion/form.js') }}"></script>
@endsection
