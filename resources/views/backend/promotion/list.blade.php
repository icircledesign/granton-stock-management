@extends('backend.layouts.admin')

@section('title',"Promotion List" )

@section('pageTitle',"Promotion List")

@section('style')
	<style type="text/css">
		th,td {
		  white-space: nowrap;
		  text-overflow: ellipsis;
		}
	</style>
@endsection

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Promotions</a>
		</li>
	</ol>
@endsection


@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                	@permissions ('/promotions/create')
                    <div class="">
                        <a href="{{url($currentUrl.'/promotions/create')}}" class="btn btn-primary ">Create New Promotion</a>
                    </div>
                    @endpermissions
                    <div class="table-responsive">
                        <br/>
                        <table id="promotions_table" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            {{csrf_field()}}
                            <thead>
                            <tr>
                                <th width="20%">Name</th>
                                <th>Promotion Code</th>
                                <th width="10%">Start Date</th>
                                <th width="10%">End Date</th>
                                <th width="15%">Remaining Time</th>
                                <th >Lister Name</th>
                                <th>Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>


                        </table>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {

		    $.ajaxSetup({
		        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		    });
		    var dataTable = $('#promotions_table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{url($currentUrl.'/promotions/get_promotion_list')}}",
		        "columns": [
		        	{"data": "name"},
                    {"data": "promotion_code"},
                    {"data": "start_date"},
                    {"data": "end_date"},
                    {"data": "remaining_data"},
                    {"data": "lister_id"},
                    {"data": "status"},
		        	{"data": "action", orderable: false, searchable: false},
		        ]
		        
		    });

		    
		});
	</script>
@endsection
