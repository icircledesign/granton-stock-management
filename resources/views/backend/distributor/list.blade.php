@extends('backend.layouts.admin')

@section('title',"Distributor List" )

@section('pageTitle',"Distributor List")

@section('style')
	<style type="text/css">
		th,td {
		  white-space: nowrap;
		  text-overflow: ellipsis;
		}
	</style>
@endsection

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Distributors</a>
		</li>
	</ol>
@endsection


@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                	@permissions ('/distributors/create')
                    <div class="">
                        <a href="{{url($currentUrl.'/distributors/create')}}" class="btn btn-primary ">Create New Distributor</a>
                    </div>
                    @endpermissions
                    <div class="table-responsive">
                        <br/>
                        <table id="distributors_table" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            {{csrf_field()}}
                            <thead>
                            <tr>
                                <th>Distributor Name</th>
                                <th>NIC </th>
                                <th>Phone No</th>
                                <th>Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>


                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {

		    $.ajaxSetup({
		        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		    });
		    var dataTable = $('#distributors_table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{url($currentUrl.'/distributors/get_distributor_list')}}",
		        "columns": [
		        	{"data": "distributor_name"},
		        	{"data": "nic"},
		        	{"data": "phone_no"},
		        	{"data": "status"},
		        	{"data": "action", orderable: false, searchable: false},
		        ]

		    });


		});
	</script>
@endsection
