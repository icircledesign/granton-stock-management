@extends('backend.layouts.admin')

@section('title',(!empty($id))? "Update" : "Create New"." Distributor" )

@section('pageTitle',(!empty($id))? "Update" : "Create New"." Distributor" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/distributors')}}">Distributors</a>
        </li>

		<li class="active">
			<a href="{{url()->current()}}">{{ (!empty($id))? "Update" : "Create New" }} Distributor</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{ (!empty($id))? url($currentUrl.'/distributors/update') : url($currentUrl.'/distributors/store')}}" method="POST" id="formDistributor">
                    {{ csrf_field() }}
                    {{ (!empty($id))? method_field('PATCH') : method_field('POST') }}
                    <input type="hidden" name="id" id="id" value="{{ (!empty($id))? $id : '' }}">
                    <div class="ibox-title">
                        <h5>{{ (!empty($id))? "Update" : "Create New" }} Distributor</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Distributor Name <span style="color: red;">*</span></label>
                                <label id="distributor_name-error" class="text-danger" for="distributor_name"></label>
                                <input type="text" class="form-control" name="distributor_name" id="distributor_name" placeholder="Enter Distributor Name" value="{{(!empty($distributor->name))? $distributor->name : old('distributor_name') }}">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>NIC <span style="color: red;">*</span></label>
                                <label id="nic-error" class="text-danger" for="nic"></label>
                                <input type="text" class="form-control" name="nic" id="nic" placeholder="Enter NIC" value="{{(!empty($distributor->nic))? $distributor->nic : old('nic') }}">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Phone No <span style="color: red;">*</span></label>
                                <label id="phone_no-error" class="text-danger" for="phone_no"></label>
                                <input type="tel" class="form-control" name="phone_no" id="phone_no" placeholder="Enter Phone No" value="{{(!empty($distributor->phone_no))? $distributor->phone_no : old('phone_no') }}">
                                <input type="hidden" id="country_code" name="country_code"  value="{{ (!empty($id))? $distributor->country_code : old('country_code')}}">
                            </div>
                        </div>
                        @if(!empty($id))
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Status <span style="color: red;">*</span></label>
                                <div class="i-checks">
                                    <label class="radio-inline">
                                        <input type="radio" value="1"  name="status_id" {{(!empty($distributor->status_id))? ($distributor->status_id ==1)? 'checked' : 'unchecked' : '' }} checked> Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" value="3" name="status_id" {{(!empty($distributor->status_id))? ($distributor->status_id ==3)? 'checked' : 'unchecked' : '' }}> Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{ (!empty($id))? "Update" : "Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <a href="{{url($currentUrl.'/distributors')}}" class="btn btn-success">Go Back</a>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<script type="text/javascript" src="{{ URL::asset('js/backend/custom/distributor/form.js') }}"></script>
@endsection
