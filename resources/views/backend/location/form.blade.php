@extends('backend.layouts.admin')

@section('title',(!empty($id))? "Update" : "Create New"." Location" )

@section('pageTitle',(!empty($id))? "Update" : "Create New"." Location" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/locations')}}">Locations</a>
        </li>

		<li class="active">
			<a href="{{url()->current()}}">{{ (!empty($id))? "Update" : "Create New" }} Location</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{ (!empty($id))? url($currentUrl.'/locations/update') : url($currentUrl.'/locations/store')}}" method="POST" id="formLocation">
                    {{ csrf_field() }}
                    {{ (!empty($id))? method_field('PATCH') : method_field('POST') }}
                    <input type="hidden" name="id" id="id" value="{{ (!empty($id))? $id : '' }}">
                    <div class="ibox-title">
                        <h5>{{ (!empty($id))? "Update" : "Create New" }} Location</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">
                    	
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Location Name <span style="color: red;">*</span></label>
                                <label id="location_name-error" class="text-danger" for="location_name"></label>
                                <input type="text" class="form-control" name="location_name" id="location_name" placeholder="Enter Location Name" value="{{(!empty($location->location_name))? $location->location_name : old('location_name') }}">
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <div class="form-group">
                            	<label>Address <span style="color: red;">*</span></label>
                            	<label id="address-error" class="text-danger" for="address"></label>
                            	<textarea class="form-control" placeholder="Enter Address for this Location" name="address" cols="50" rows="8" id="address" style="resize: none;">{{(!empty($location->address))? $location->address : old('address') }}</textarea>
                            </div>
                            
                        </div>
                        @if(!empty($id))
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Status <span style="color: red;">*</span></label>
                                
                                <div class="i-checks">
                                    <label class="radio-inline">
                                        <input type="radio" value="1"  name="status_id" {{(!empty($location->status_id))? ($location->status_id ==1)? 'checked' : 'unchecked' : '' }} checked> Active
                                    </label>
                                
                                    <label class="radio-inline">
                                        <input type="radio" value="3" name="status_id" {{(!empty($location->status_id))? ($location->status_id ==3)? 'checked' : 'unchecked' : '' }}> Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                        
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{ (!empty($id))? "Update" : "Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <a href="{{url($currentUrl.'/locations')}}" class="btn btn-success">Go Back</a>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>
                    
        </div>

            
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/location/form.js') }}"></script>
@endsection