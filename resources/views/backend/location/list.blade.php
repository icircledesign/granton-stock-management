@extends('backend.layouts.admin')

@section('title',"Location List" )

@section('pageTitle',"Location List")

@section('style')
	<style type="text/css">
		th,td {
		  white-space: nowrap;
		  text-overflow: ellipsis;
		}
	</style>
@endsection

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Locations</a>
		</li>
	</ol>
@endsection


@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                	@permissions ('/locations/create')
                    <div class="">
                        <a href="{{url($currentUrl.'/locations/create')}}" class="btn btn-primary ">Create New Location</a>
                    </div>
                    @endpermissions
                    <div class="table-responsive">
                        <br/>
                        <table id="locations_table" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            {{csrf_field()}}
                            <thead>
                            <tr>
                               	<th  width="20%" >Location Name</th>
                                <th>Address</th>
                                <th  width="10%">Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>


                        </table>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {

		    $.ajaxSetup({
		        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		    });
		    var dataTable = $('#locations_table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{url($currentUrl.'/locations/get_location_list')}}",
		        "columns": [
		        	{"data": "location_name"},
		        	{"data": "address"},
		        	{"data": "status"},
		        	{"data": "action", orderable: false, searchable: false},
		        ]
		        
		    });

		    
		});
	</script>
@endsection
