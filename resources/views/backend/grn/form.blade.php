@extends('backend.layouts.admin')

@section('title',"New GRN" )

@section('pageTitle',"New GRN" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/grns/new')}}">New GRN</a>
        </li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{url($currentUrl.'/grns/store')}}" method="POST" id="formGRN">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="ibox-title">
                        <h5>New GRN</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Promotion Code <span style="color: red;">*</span></label>
                                <label id="promotion_code-error" class="text-danger" for="promotion_code"></label>
                                <select id="promotion_code" data-placeholder="Select Promotion Code" name="promotion_code" class="form-control">
                                  <option></option>
                                  @forelse ($promotions as $promotion)
                                  <option value="{{ $promotion->id }}">{{ $promotion->name }}</option>
                                  @empty

                                  @endforelse
                              </select>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Start Serial No <span style="color: red;">*</span></label>
                                <label id="start_serial_no-error" class="text-danger" for="start_serial_no"></label>
                                <input type="text" class="form-control" name="start_serial_no" id="start_serial_no" placeholder="Enter Start Serial No" value="{{old('start_serial_no')}}">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>End Serial No <span style="color: red;">*</span></label>
                                <label id="end_serial_no-error" class="text-danger" for="end_serial_no"></label>
                                <input type="text" class="form-control" name="end_serial_no" id="end_serial_no" placeholder="Enter End Serial No" value="{{old('end_serial_no')}}">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Printer Name<span style="color: red;">*</span></label>
                                <label id="printer_id-error" class="text-danger" for="printer_id"></label>
                                <select id="printer_id" data-placeholder="Select Printer Name" name="printer_id" class="form-control">
                                  <option></option>
                                  @forelse ($printers as $printer)
                                  <option value="{{ $printer->id }}">{{ $printer->printer_name }}</option>
                                  @empty

                                  @endforelse
                              </select>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{"Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>

        </div>


    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function (){
        $('#promotion_code').select2();
        $('#printer_id').select2();
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/grn/form.js') }}"></script>
@endsection
