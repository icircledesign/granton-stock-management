@extends('backend.layouts.admin')

@section('title',"Divisional Manager List" )

@section('pageTitle',"Divisional Manager List")

@section('style')
	<style type="text/css">
		th,td {
		  white-space: nowrap;
		  text-overflow: ellipsis;
		}
	</style>
@endsection

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Divisional Managers</a>
		</li>
	</ol>
@endsection


@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                	@permissions ('/divisional-managers/create')
                    <div class="">
                        <a href="{{url($currentUrl.'/divisional-managers/create')}}" class="btn btn-primary ">Create New Divisional Manager</a>
                    </div>
                    @endpermissions
                    <div class="table-responsive">
                        <br/>
                        <table id="divisionalManager_table" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            {{csrf_field()}}
                            <thead>
                            <tr>
                                <th>Divisional Manager Name</th>
                                <th>Email Address</th>
                                <th>NIC </th>
                                <th>Phone No</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>


                        </table>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {

		    $.ajaxSetup({
		        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		    });
		    var dataTable = $('#divisionalManager_table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{url($currentUrl.'/divisional-managers/get_divisionalManager_list')}}",
		        "columns": [
		        	{"data": "divisionalManager_name"},
		        	{"data": "email"},
		        	{"data": "nic"},
		        	{"data": "phone_no"},
		        	{"data": "location"},
		        	{"data": "status"},
		        	{"data": "action", orderable: false, searchable: false},
		        ]
		        
		    });

		    
		});
	</script>
@endsection
