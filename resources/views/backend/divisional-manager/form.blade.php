@extends('backend.layouts.admin')

@section('title',(!empty($id))? "Update" : "Create New"." Divisional Manager" )

@section('pageTitle',(!empty($id))? "Update" : "Create New"." Divisional Manager" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/divisional-managers')}}">Divisional Managers</a>
        </li>

		<li class="active">
			<a href="{{url()->current()}}">{{ (!empty($id))? "Update" : "Create New" }} Divisional Manager</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{ (!empty($id))? url($currentUrl.'/divisional-managers/update') : url($currentUrl.'/divisional-managers/store')}}" method="POST" id="formDivisionalManager">
                    {{ csrf_field() }}
                    {{ (!empty($id))? method_field('PATCH') : method_field('POST') }}
                    <input type="hidden" name="id" id="id" value="{{ (!empty($id))? $id : '' }}">
                    <div class="ibox-title">
                        <h5>{{ (!empty($id))? "Update" : "Create New" }} Divisional Manager</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Divisional Manager Name <span style="color: red;">*</span></label>
                                <label id="divisionalManager_name-error" class="text-danger" for="divisionalManager_name"></label>
                                <input type="text" class="form-control" name="divisionalManager_name" id="divisionalManager_name" placeholder="Enter Divisional Manager Name" value="{{(!empty($divisionalManager->user->name))? $divisionalManager->user->name : old('divisionalManager_name') }}">
                                <input type="hidden" class="form-control" name="user_id" id="user_id" value="{{(!empty($divisionalManager->user->id))? Crypt::encrypt($divisionalManager->user->id) : ''}}">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Email Address <span style="color: red;">*</span></label>
                                <label id="email-error" class="text-danger" for="email"></label>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address" value="{{(!empty($divisionalManager->user->email))? $divisionalManager->user->email : old('email') }}">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>NIC <span style="color: red;">*</span></label>
                                <label id="nic-error" class="text-danger" for="nic"></label>
                                <input type="text" class="form-control" name="nic" id="nic" placeholder="Enter NIC" value="{{(!empty($divisionalManager->nic))? $divisionalManager->nic : old('nic') }}">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Phone No <span style="color: red;">*</span></label>
                                <label id="phone_no-error" class="text-danger" for="phone_no"></label>
                                <input type="tel" class="form-control" name="phone_no" id="phone_no" placeholder="Enter Phone No" value="{{(!empty($divisionalManager->phone_no))? $divisionalManager->phone_no : old('phone_no') }}">
                                <input type="hidden" id="country_code" name="country_code"  value="{{ (!empty($id))? $divisionalManager->country_code : old('country_code')}}">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Location Name <span style="color: red;">*</span></label>
                                <label id="location_id-error" class="text-danger" for="location_id"></label>
                                <select id="location_id" data-placeholder="Select Location Name" name="location_id" class="form-control">
                                  <option></option>
                                  @forelse ($locations as $location)
                                  <option value="{{ $location->id }}" {{(!empty($id))? ($location->id == $divisionalManager->location_id)?"selected":""  : '' }}>{{ $location->location_name }}</option>
                                  @empty

                                  @endforelse
                              </select>
                            </div>
                        </div>
                        @if(!empty($id))
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Status <span style="color: red;">*</span></label>

                                <div class="i-checks">
                                    <label class="radio-inline">
                                        <input type="radio" value="1"  name="status_id" {{(!empty($divisionalManager->user->status_id))? ($divisionalManager->user->status_id ==1)? 'checked' : 'unchecked' : '' }} checked> Active
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" value="3" name="status_id" {{(!empty($divisionalManager->user->status_id))? ($divisionalManager->user->status_id ==3)? 'checked' : 'unchecked' : '' }}> Inactive
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif


                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{ (!empty($id))? "Update" : "Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <a href="{{url($currentUrl.'/divisional-managers')}}" class="btn btn-success">Go Back</a>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>

        </div>


    </div>
</div>
@endsection
@section('script')

<script type="text/javascript" src="{{ URL::asset('js/backend/custom/divisional-manager/form.js') }}"></script>
@endsection
