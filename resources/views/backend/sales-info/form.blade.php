@extends('backend.layouts.admin')

@section('title',"New Sales Info" )

@section('pageTitle',"New Sales Info" )

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/sales-info/new')}}">New Sales Info</a>
        </li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{url($currentUrl.'/sales-info/store')}}" method="POST" id="formSalesInfo">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="ibox-title">
                        <h5>New Sales Info</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Promotion Code <span style="color: red;">*</span></label>
                                    <label id="promotion_id-error" class="text-danger" for="promotion_id"></label>
                                    <select id="promotion_id" data-placeholder="Select Promotion Code" name="promotion_id" class="form-control">
                                    <option></option>
                                    @forelse ($promotions as $promotion)
                                    <option value="{{ $promotion->id }}">{{ $promotion->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Serial No <span style="color: red;">*</span></label>
                                    <label id="serial_no_id-error" class="text-danger" for="serial_no_id"></label>
                                    <select id="serial_no_id" data-placeholder="Enter Serial No" name="serial_no_id" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Customer Name <span style="color: red;">*</span></label>
                                    <label id="customer_name-error" class="text-danger" for="customer_name"></label>
                                    <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Customer Name" value="{{ old('customer_name') }}">
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Phone No <span style="color: red;">*</span></label>
                                    <label id="phone_no-error" class="text-danger" for="phone_no"></label>
                                    <input type="tel" class="form-control" name="phone_no" id="phone_no" placeholder="Enter Phone No" value="{{ old('phone_no') }}">
                                    <input type="hidden" id="country_code" name="country_code"  value="{{ old('country_code')}}">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>NIC Number <span style="color: red;">*</span></label>
                                    <label id="nic-error" class="text-danger" for="nic"></label>
                                    <input type="text" class="form-control" name="nic" id="nic" placeholder="Enter NIC Number" value="{{ old('nic') }}">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <input type="text" class="form-control" name="gender" id="gender" readonly>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date of Birth</label>
                                    <input type="text" class="form-control" name="date_of_birth" id="date_of_birth" readonly>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Email Address <span style="color: red;"> </span></label>
                                    <label id="email-error" class="text-danger" for="email"></label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address" value="{{ old('email') }}">
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-6">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Address <span style="color: red;">*</span></label>
                                    <label id="address-error" class="text-danger" for="address"></label>
                                    <textarea class="form-control" placeholder="Enter Address" name="address" cols="50" rows="8" id="address" style="resize: none;">{{old('address') }}</textarea>
                                </div>

                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>City<span style="color: red;">*</span></label>
                                    <label id="city_id-error" class="text-danger" for="city_id"></label>
                                    <select id="city_id" data-placeholder="Select City " name="city_id" class="form-control">
                                    <option></option>
                                    @forelse ($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Distributor Name<span style="color: red;">*</span></label>
                                    <label id="distributor_id-error" class="text-danger" for="distributor_id"></label>
                                    <select id="distributor_id" data-placeholder="Select Distributor" name="distributor_id" class="form-control">
                                    <option></option>
                                    @forelse ($distributors as $distributor)
                                    <option value="{{ $distributor->id }}">{{ $distributor->name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" type="submit" id="submit">{{"Save" }}</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>

        </div>


    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function (){
        $('#promotion_id').select2();
        $('#city_id').select2();
        $('#distributor_id').select2();
        $('#serial_no_id').select2({
            //minimumInputLength: 3,
            disabled: true
        });
    });
</script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/sales-info/form.js') }}"></script>
<script type="text/javascript">

    function serial_no_id() {
        $("#serial_no_id").val(null).trigger("change");
        var promotion_id = $("select[name='promotion_id']").val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "{{url($currentUrl.'/sales-info/get_serial_numbers')}}",
            method: 'POST',
            data: {promotion_id:promotion_id, _token:token},
            success: function(data) {
                $("select[name='serial_no_id']").html('');
                $("select[name='serial_no_id']").html(data.options);
            }
        });
    };

    $("select[name='promotion_id']").change(function(){
        $("#serial_no_id").prop("disabled", false);
        serial_no_id();
    });

</script>
@endsection
