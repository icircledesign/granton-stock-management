@extends('backend.layouts.admin')

@section('title',"Update User Data")

@section('pageTitle',"Update User Data") 

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

        <li>
            <a href="{{url($currentUrl.'/update-profile')}}">Users</a>
        </li>

		<li class="active">
			<a href="{{url()->current()}}">Update User Data</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <form autocomplete="off" enctype="multipart/form-data" action="{{url($currentUrl.'/update-profile/update')}}" method="POST" id="formUpdateUser">
                    {{ csrf_field() }}
                    {{ method_field('PATCH')}}
                    <div class="ibox-title">
                        <h5>Change Password</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>User Name <span style="color: red;"> </span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter User Name" value="{{Auth::user()->name }}" disabled>
                                <input type="hidden" class="form-control" name="user_id" id="user_id" value="{{Crypt::encrypt(Auth::user()->id)}}">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Email Address <span style="color: red;"> </span></label>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address" value="{{ Auth::user()->email }}" disabled>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Current Password <span style="color: red;">*</span></label>
                                <label id="current_password-error" class="text-danger" for="current_password"></label>
                                <input type="password" class="form-control" name="current_password" id="current_password" placeholder="Enter Current Password" value="">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>New Password <span style="color: red;">*</span></label>
                                <label id="password-error" class="text-danger" for="password"></label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter New Password" value="">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Confirm New Password <span style="color: red;">*</span></label>
                                <label id="password_confirm-error" class="text-danger" for="password_confirm"></label>
                                <input type="password" class="form-control" name="password_confirm" id="password_confirm" placeholder="Enter Confirm Password" value="">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">
                        <button class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Change Password" type="submit" id="submit">Change Password</button>
                        <button class="btn btn-danger" type="reset" id="reset">Reset</button>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>
                    
        </div>

            
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/update-profile/form.js') }}"></script>
@endsection