@extends('backend.layouts.manage')

@section('title','Unauthorized Access')

@php  
    $currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
@endphp
@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1 class="text-danger">401</h1>
        <h2><i class="fa fa-warning text-red"></i> Unauthorized Access</h2>

        <div class="error-desc">
            <a href="{{ url('/home') }}">return to dashboard</a> or go <a href="{{ URL::previous() }}">back</a> to previous page.

        </div>
    </div>
@endsection