@extends('backend.layouts.error')

@section('title','403 Error')

@php  
    $currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
@endphp
@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1 class="text-danger">403</h1>
        <h2><i class="fa fa-warning text-red"></i> Oops! Request Forbidden.</h2>

        <div class="error-desc">
            The page you are requested can't authorized. Check the credentials that you supplied. Perhaps, you may <a href="{{ url('/home') }}">return to dashboard</a> or try login with another account.

        </div>
    </div>
@endsection