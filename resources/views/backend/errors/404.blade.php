@extends('backend.layouts.error')


@section('title','Page Not Found')

@php  
    $currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
@endphp
@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1 class="text-danger">404</h1>
        <h2><i class="fa fa-warning text-red"></i> Oops! Page not found.</h2>

        <div class="error-desc">
            We could not find the page you were looking for. Meanwhile, you may <a href="{{url($currentUrl.'/dashboard')}}">return to dashboard</a> or go <a href="{{ URL::previous() }}">back</a> to previous page.
            
        </div>
    </div>
@endsection