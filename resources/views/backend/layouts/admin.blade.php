<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.company') }} @yield('title')</title>

    @include('backend.layouts.includes.header')

</head>

<body>

    <div id="wrapper">

        <!-- Left side column. contains the sidebar -->
        @include('backend.layouts.includes.left_nav')

        <div id="page-wrapper" class="gray-bg">

            @include('backend.layouts.includes.top_menu')
            <!-- title -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@yield('pageTitle')</h2>

                    @yield('mainBreadcrumb')
                </div>
                
            </div>
            @yield('content')
            <div class="footer">
                <div class="pull-right">
                    
                </div>
                <div>
                    <strong>Copyright</strong> {{ config('app.company') }} &copy; {{date('Y')}}
                    
                </div>
            </div>
        </div>
    </div>


    

    @include('backend.layouts.includes.footer')
</body>

</html>

