<!-- Scripts -->

<!-- Mainly scripts -->
<script src="{{ asset('js/backend/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/backend/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/backend/moment.js') }}"></script>
<script src="{{ asset('js/backend/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('js/backend/inspinia.js')}}"></script>
<script src="{{ asset('js/backend/plugins/pace/pace.min.js')}}"></script>

<!-- jQuery UI custom -->
<script src="{{ asset('js/backend/jquery-ui.custom.min.js')}}"></script>


<!-- Input Mask-->
<script src="{{ asset('js/backend/plugins/jasny/jasny-bootstrap.min.js')}}"></script>

<!-- intlTelInput -->
<script src="{{ asset('js/backend/intlTelInput.js')}}"></script>


<!-- Select2 -->
<script src="{{ asset('js/backend/plugins/select2/select2.full.min.js')}}"></script>

<!-- dataTables -->
<script src="{{ asset('js/backend/plugins/dataTables/datatables.min.js')}}"></script>

<!-- iCheck -->
<script src="{{ asset('js/backend/plugins/iCheck/icheck.min.js')}}"></script>


<!-- slick carousel-->
<script src="{{ asset('js/backend/plugins/slick/slick.min.js')}}"></script>

<!-- Sweet alert -->
<script src="{{ asset('js/backend/plugins/sweetalert/sweetalert.min.js')}}"></script>

<!-- Data picker -->
<script src="{{ asset('js/backend/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

<script>
	
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
</script>

@yield('script')
