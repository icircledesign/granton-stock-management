@php
    $currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    $currentGetPrefix = Route::getFacadeRoot()->current()->getPrefix();

@endphp
<nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img src="{{ url(Storage::url(isset(Auth::user()->image) ? Auth::user()->image : 'avatar.jpg')) }}" alt="{{ isset(Auth::user()->name) ? Auth::user()->name : '' }}" class="img-circle" width="48">

                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ isset(Auth::user()->name) ? Auth::user()->name : '' }}</strong>
                        </a>
                    </div>
                    <div class="logo-element">
                        G
                    </div>
                </li>
                @permissions('/dashboard')
                <li class="{{isActive($currentGetPrefix.'/dashboard') }}" title="Dashboard">
                    <a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span> <span class="fa arrow"></span></a>

                    <ul class="nav nav-second-level">
                        <li title="Permission" class="{{isActive($currentGetPrefix.'/dashboard') }}"><a href="{{url($currentUrl.'/dashboard')}}">Dashboard</a></li>


                    </ul>
                </li>
                @endpermissions
                @permissions('/roles|/roles/create')
                <li class="{{isActive($currentGetPrefix.'/roles*') }}" title="Roles">
                    <a href="{{url($currentUrl.'/roles')}}"><i class="fa fa-user-circle-o"></i> <span class="nav-label">Roles</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/roles')
                        <li title="Roles" class="{{isActive($currentGetPrefix.'/roles') }}"><a href="{{url($currentUrl.'/roles')}}">Roles</a></li>
                        @endpermissions
                        @permissions ('/roles/create')
                        <li title="Create Role" class="{{isActive($currentGetPrefix.'/roles/create') }}"><a href="{{url($currentUrl.'/roles/create')}}">Create Role</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/permissions')
                <li class="{{isActive($currentGetPrefix.'/permissions') }}" title="Permission">
                    <a href="{{url($currentUrl.'/permissions')}}"><i class="fa fa-unlock-alt"></i> <span class="nav-label">Permission</span> <span class="fa arrow"></span></a>

                    <ul class="nav nav-second-level">
                        <li title="Permission" class="{{isActive($currentGetPrefix.'/permissions') }}"><a href="{{url($currentUrl.'/permissions')}}">Permissions</a></li>
                    </ul>
                </li>
                @endpermissions

                @permissions('/locations|/locations/create')
                <li class="{{isActive($currentGetPrefix.'/locations*') }}" title="Locations">
                    <a href="{{url($currentUrl.'/locations')}}"><i class="fa fa-map-marker"></i> <span class="nav-label">Locations</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/locations')
                        <li title="Locations" class="{{isActive($currentGetPrefix.'/locations') }}"><a href="{{url($currentUrl.'/locations')}}">Locations</a></li>
                        @endpermissions
                        @permissions ('/locations/create')
                        <li title="Create Location" class="{{isActive($currentGetPrefix.'/locations/create') }}"><a href="{{url($currentUrl.'/locations/create')}}">Create Location</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/divisional-managers|/divisional-managers/create')
                <li class="{{isActive($currentGetPrefix.'/divisional-managers*') }}" title="Divisional Managers">
                    <a href="{{url($currentUrl.'/divisional-managers')}}"><i class="fa fa-user"></i> <span class="nav-label">Divisional Managers</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/divisional-managers')
                        <li title="Divisional Managers" class="{{isActive($currentGetPrefix.'/divisional-managers') }}"><a href="{{url($currentUrl.'/divisional-managers')}}">Divisional Managers</a></li>
                        @endpermissions
                        @permissions ('/divisional-managers/create')
                        <li title="Create Divisional Manager" class="{{isActive($currentGetPrefix.'/divisional-managers/create') }}"><a href="{{url($currentUrl.'/divisional-managers/create')}}">Create Divisional Manager</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/managers|/managers/create')
                <li class="{{isActive($currentGetPrefix.'/managers*') }}" title="Managers">
                    <a href="{{url($currentUrl.'/managers')}}"><i class="fa fa-users"></i> <span class="nav-label">Managers</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/managers')
                        <li title="Managers" class="{{isActive($currentGetPrefix.'/managers') }}"><a href="{{url($currentUrl.'/managers')}}">Managers</a></li>
                        @endpermissions
                        @permissions ('/managers/create')
                        <li title="Create Manager" class="{{isActive($currentGetPrefix.'/managers/create') }}"><a href="{{url($currentUrl.'/managers/create')}}">Create Manager</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/distributors|/distributors/create')
                <li class="{{isActive($currentGetPrefix.'/distributors*') }}" title="Distributors">
                    <a href="{{url($currentUrl.'/distributors')}}"><i class="fa fa-users"></i> <span class="nav-label">Distributors</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/distributors')
                        <li title="Distributors" class="{{isActive($currentGetPrefix.'/distributors') }}"><a href="{{url($currentUrl.'/distributors')}}">Distributors</a></li>
                        @endpermissions
                        @permissions ('/distributors/create')
                        <li title="Create Distributor" class="{{isActive($currentGetPrefix.'/distributors/create') }}"><a href="{{url($currentUrl.'/distributors/create')}}">Create Distributor</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/listers|/listers/create')
                <li class="{{isActive($currentGetPrefix.'/listers*') }}" title="Listers">
                    <a href="{{url($currentUrl.'/listers')}}"><i class="fa fa-user-plus"></i> <span class="nav-label">Listers</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/listers')
                        <li title="Listers" class="{{isActive($currentGetPrefix.'/listers') }}"><a href="{{url($currentUrl.'/listers')}}">Listers</a></li>
                        @endpermissions
                        @permissions ('/listers/create')
                        <li title="Create Lister" class="{{isActive($currentGetPrefix.'/listers/create') }}"><a href="{{url($currentUrl.'/listers/create')}}">Create Lister</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/printers|/printers/create')
                <li class="{{isActive($currentGetPrefix.'/printers*') }}" title="Printers">
                    <a href="{{url($currentUrl.'/printers')}}"><i class="fa fa-print"></i> <span class="nav-label">Printers</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/printers')
                        <li title="Printers" class="{{isActive($currentGetPrefix.'/printers') }}"><a href="{{url($currentUrl.'/printers')}}">Printers</a></li>
                        @endpermissions
                        @permissions ('/printers/create')
                        <li title="Create Printer" class="{{isActive($currentGetPrefix.'/printers/create') }}"><a href="{{url($currentUrl.'/printers/create')}}">Create Printer</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/promotions|/promotions/create')
                <li class="{{isActive($currentGetPrefix.'/promotions*') }}" title="Promotions">
                    <a href="{{url($currentUrl.'/promotions')}}"><i class="fa fa-flag"></i> <span class="nav-label">Promotions</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @permissions ('/promotions')
                        <li title="Promotions" class="{{isActive($currentGetPrefix.'/promotions') }}"><a href="{{url($currentUrl.'/promotions')}}">Promotions</a></li>
                        @endpermissions
                        @permissions ('/promotions/create')
                        <li title="Create Promotion" class="{{isActive($currentGetPrefix.'/promotions/create') }}"><a href="{{url($currentUrl.'/promotions/create')}}">Create Promotion</a></li>
                        @endpermissions
                    </ul>
                </li>
                @endpermissions

                @permissions('/grns|/grns/new')
                <li class="{{isActive($currentGetPrefix.'/grns/new') }}" title="New GRN">
                    <a href="{{url($currentUrl.'/grns/new')}}"><i class="fa fa-credit-card"></i> <span class="nav-label">New GRN</span> </a>
                </li>
                @endpermissions

                @permissions('/issue-notes|/issue-notes/new')
                <li class="{{isActive($currentGetPrefix.'/issue-notes/new') }}" title="New Issue Note">
                    <a href="{{url($currentUrl.'/issue-notes/new')}}"><i class="fa fa-credit-card"></i> <span class="nav-label">New Issue Note</span> </a>
                </li>
                @endpermissions

                @permissions('/sales-info|/sales-info/new')
                <li class="{{isActive($currentGetPrefix.'/sales-info/new') }}" title="New Sales Info">
                    <a href="{{url($currentUrl.'/sales-info/new')}}"><i class="fa fa-address-book-o"></i> <span class="nav-label">New Sales Info</span> </a>
                </li>
                @endpermissions

                @permissions('/update-profile')
                <li class="{{isActive($currentGetPrefix.'/update-profile') }}" title="Update Profile">
                    <a href="{{url($currentUrl.'/update-profile')}}"><i class="fa fa-edit"></i> <span class="nav-label">Update Profile</span> </a>
                </li>
                @endpermissions


            </ul>

        </div>
    </nav>
