<!-- Fonts -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="{{ asset('css/backend/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/backend/animate.css') }}" rel="stylesheet">
<link href="{{ asset('css/backend/style.css') }}" rel="stylesheet">

<link href="{{ asset('css/backend/plugins/iCheck/custom.css')}}" rel="stylesheet">

<link href="{{ asset('css/backend/plugins/slick/slick.css')}}" rel="stylesheet">

<link href="{{ asset('css/backend/plugins/slick/slick-theme.css')}}" rel="stylesheet">


<!-- dataTables -->
<link href="{{ asset('css/backend/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">

<!-- Select2 -->
<link href="{{ asset('css/backend/plugins/select2/select2.min.css')}}" rel="stylesheet">

<link href="{{ asset('css/backend/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}"
      rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('css/backend/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">

<!-- Datetimepicker -->
<link href="{{ asset('css/backend/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">

<!-- intlTelInput -->
<link href="{{ asset('css/backend/intlTelInput.css')}}" rel="stylesheet">
@yield('style')
