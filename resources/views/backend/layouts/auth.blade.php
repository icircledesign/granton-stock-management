<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.company') }} @yield('title')</title>

    <!-- Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/backend/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/style.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/backend/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('js/backend/bootstrap.min.js') }}"></script>

</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div style="margin: 100px 0px 30px; "><img src="{{ url(Storage::url('logo.png'))}}" alt="Broom Broom Taxi Logo" width="100%"></div>
            @yield('content')
            <p class="m-t">  <strong>Copyright</strong> {{ config('app.company') }} &copy; {{date('Y')}}</p>
        </div>
    </div>
    @yield('script')
</body>
</html>
