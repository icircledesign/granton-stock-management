@extends('backend.layouts.admin')

@section('title',"Printer List" )

@section('pageTitle',"Printer List")

@section('style')
	<style type="text/css">
		th,td {
		  white-space: nowrap;
		  text-overflow: ellipsis;
		}
	</style>
@endsection

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Printers</a>
		</li>
	</ol>
@endsection


@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                	@permissions ('/printers/create')
                    <div class="">
                        <a href="{{url($currentUrl.'/printers/create')}}" class="btn btn-primary ">Create New Printer</a>
                    </div>
                    @endpermissions
                    <div class="table-responsive">
                        <br/>
                        <table id="printers_table" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            {{csrf_field()}}
                            <thead>
                            <tr>
                                <th>Printer Name</th>
                                <th>Email Address</th>
                                <th>Contact Person </th>
                                <th>Phone No</th>
                                <th>Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>


                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {

		    $.ajaxSetup({
		        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		    });
		    var dataTable = $('#printers_table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{url($currentUrl.'/printers/get_printer_list')}}",
		        "columns": [
		        	{"data": "printer_name"},
		        	{"data": "email"},
		        	{"data": "contact_person"},
		        	{"data": "phone_no"},
		        	{"data": "status"},
		        	{"data": "action", orderable: false, searchable: false},
		        ]

		    });


		});
	</script>
@endsection
