@extends('backend.layouts.admin')

@section('title',"Manager List" )

@section('pageTitle',"Manager List")

@section('style')
	<style type="text/css">
		th,td {
		  white-space: nowrap;
		  text-overflow: ellipsis;
		}
	</style>
@endsection

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Managers</a>
		</li>
	</ol>
@endsection


@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                	@permissions ('/managers/create')
                    <div class="">
                        <a href="{{url($currentUrl.'/managers/create')}}" class="btn btn-primary ">Create New Manager</a>
                    </div>
                    @endpermissions
                    <div class="table-responsive">
                        <br/>
                        <table id="managers_table" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            {{csrf_field()}}
                            <thead>
                            <tr>
                                <th>Manager Name</th>
                                <th>Email Address</th>
                                <th>NIC </th>
                                <th>Phone No</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>


                        </table>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {

		    $.ajaxSetup({
		        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
		    });
		    var dataTable = $('#managers_table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: "{{url($currentUrl.'/managers/get_manager_list')}}",
		        "columns": [
		        	{"data": "manager_name"},
		        	{"data": "email"},
		        	{"data": "nic"},
		        	{"data": "phone_no"},
		        	{"data": "location"},
		        	{"data": "status"},
		        	{"data": "action", orderable: false, searchable: false},
		        ]
		        
		    });

		    
		});
	</script>
@endsection
