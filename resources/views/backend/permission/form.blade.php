@extends('backend.layouts.admin')

@section('title','Permissions')

@section('pageTitle','Permissions')

@section('mainBreadcrumb')
	<ol class="breadcrumb">
		<li>
			<a href="{{url($currentUrl.'/dashboard')}}"><i class="fa fa-dashboard"> </i> Dashboard</a>
		</li>

		<li class="active">
			<a href="{{url()->current()}}">Permissions</a>
		</li>
	</ol>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <form autocomplete="off" action="{{url($currentUrl.'/permissions/store')}}" method="POST" id="formPermission">
            
                    <div class="ibox-title">
                        <h5>Available Permissions</h5>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-content">
                        {{csrf_field()}}
                        {{method_field('POST')}}
                        <div class="form-group">
                            <label>Permissions(s) <span style="color: red;">*</span></label>

                            <label id="name-error" class="text-danger" for="name"></label><br>

                            @foreach ($routes  as $route)
                            <div class="col-md-4">
                                <input type="checkbox" name="name[]" style='{{in_array($route['routeName'],$userPermission)?"cursor:default":"cursor:pointer"}}'
                                value="{{$route['routeUrl']}}~{{$route['routeName']}}"
                                {{in_array($route['routeName'],$userPermission)?"disabled":""}}>
                                <label for="name" style='{{in_array($route['routeName'],$userPermission)?"color:#a6a6a6;cursor:default":"color:#000;cursor:pointer"}}'>{{$route['routeName']}}</label>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    <!-- /.box-content -->
                    <div class="ibox-footer">

                        <button class="btn btn-primary" type="submit" id="submit">Save</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                        <a href="{{url($currentUrl.'/permissions')}}" class="btn btn-success">Go Back</a>
                        <br><br><div id="messages"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/permission/form.js') }}"></script>
@endsection
