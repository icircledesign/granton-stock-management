@extends('backend.layouts.auth')

@section('title', "| Login" )

@section('content')

    <h3>Welcome to Login</h3>

    <form class="m-t" role="form" action="{{ route('login') }}" id="login" method="post">
        @csrf
        <div class="form-group">
            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" autofocus>
            <label class="text-danger email"></label>
        </div>

        <div class="form-group">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
            <label class="text-danger password"></label>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b" id="submit" >
            {{ __('Login') }}
        </button>
    </form>
    <div id="messages"></div>
    <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>


@endsection
@section('script')
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/auth/login.js') }}"></script>
@endsection
