@extends('backend.layouts.error')


@section('title','Verify Your Email Address')


@section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h2>Verify Your Email Address</h2><br>

        <div class="error-desc" style="font-size: 13px;">
            Before proceeding, please check your email for a verification link. <br>
            If you did not receive the email <br><br><a href="{{ route('verification.resend') }}" class="btn btn-primary">Click here to Verify Email</a>
            <br><br>
        </div>
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                A Verification link has been sent to your email address.
            </div>
        @endif
    </div>
@endsection