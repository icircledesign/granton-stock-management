@extends('backend.layouts.auth')

@section('title','Reset Password')


@section('content')
	<h3>Reset Password</h3>
	<form class="m-t" role="form" action="{{ route('password.email') }}" id="passwordEmail" method="post">
	    @csrf
	    <div class="form-group">
	        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" autofocus>
	        <label class="text-danger email"></label>
	    </div>

	    <button type="submit" class="btn btn-primary block full-width m-b" id="submit" >
	        {{ __('Send Password Reset Link') }}
	    </button>
	</form>
	<div id="messages"></div>
    
@endsection

@section('script')
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/auth/passwordEmail.js') }}"></script>
@endsection
