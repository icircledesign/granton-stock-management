@extends('backend.layouts.auth')

@section('title','Reset Password')

@section('content')

    <form class="m-t" role="form" action="{{ route('password.update') }}" id="passwordUpdate" method="post">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="form-group">
            <input id="email" type="text" class="form-control" name="email" value="{{ $email ?? old('email') }}" placeholder="Email Address" autofocus>
            <label class="text-danger email"></label>
        </div>

        <div class="form-group">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
            <label class="text-danger password"></label>
        </div>
        <div class="form-group">
            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
            <label class="text-danger password_confirmation"></label>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b" id="submit" >
            {{ __('Reset Password') }}
        </button>
    </form>
@endsection
@section('script')
<script type="text/javascript" src="{{ URL::asset('js/backend/custom/auth/passwordUpdate.js') }}"></script>
@endsection