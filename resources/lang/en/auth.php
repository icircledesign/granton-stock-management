<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Invalid Email Address and Password",
    'throttle' => 'Too Many Login Attempts.<br>Please Try Again in :seconds Seconds.',
    'active' => 'Your account is not active',

];
