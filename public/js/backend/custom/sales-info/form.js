$(document).ready(function () {
    CKEDITOR.replace( 'address', {
        toolbarGroups: [
          { name: 'basicstyles' }
        ],
    });
    for (var i in CKEDITOR.instances) {
    CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });
    }
    $("#phone_no").intlTelInput({
        separateDialCode: true,
        nationalMode: true,
        utilsScript: "{{ URL::asset('js/backend/utils.js') }}",
        initialCountry: 'lk'
    });

    $('#formSalesInfo').on('submit', function (event) {

        event.preventDefault();

        $('#submit').prop('disabled', true);
        $('.text-danger').html('');
        $('.form-group').removeClass('has-error');
        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
        url: $(this).attr("action"),
        type: $(this).attr("method"),
        data: formData,
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {

            $('.text-danger').html('');
            $('.form-group').removeClass('has-error');
            if (response.status == false) {
            $('#submit').prop('disabled', false);
            $('#messages').html('<div class="alert alert-danger">'+response.message+'</div>').fadeIn();
            setTimeout(function() {
                $('#messages').fadeOut();
                window.location.href =response.redirectTo;
            }, 2500);
            }else{
            $('#submit').prop('disabled', false);
            $('#messages').html('<div class="alert alert-success">' + response.message + '</div>').fadeIn();
            setTimeout(function () {
                $('#messages').fadeOut();

                window.location.href = response.redirectTo;
            }, 2000);
            }

        },
        error:function(response)
        {
            $('#submit').prop('disabled', false);
            if( response.status === 401 ){

            }
            if( response.status === 422 ) {
            var errors = response.responseJSON.errors;
            $.each(errors, function (i, m) {
                $('#' + i + '-error').html(m);
                $('#' + i + '-error').parent().addClass('has-error');
            });
            }

        }
        })
    });

    find();

    function find(){
        var NICNo = $("#nic").val();
        if((NICNo != '') && (NICNo.length == 10 || NICNo.length == 12)){
        var dayText = 0;
        var year = "";
        var month = "";
        var day = "";
        var gender = "";
        if (NICNo.length != 10 && NICNo.length != 12) {
            $('#nic-error').html("Invalid NIC NO");
            $('#nic-error').parent().addClass('has-error');
        }else if (NICNo.length == 10 && !$.isNumeric(NICNo.substr(0, 9))) {
            $('#nic-error').html("Invalid NIC NO");
            $('#nic-error').parent().addClass('has-error');
        }else{
            // Year
            if (NICNo.length == 10) {
            year = "19" + NICNo.substr(0, 2);
            dayText = parseInt(NICNo.substr(2, 3));
            } else {
            year = NICNo.substr(0, 4);
            dayText = parseInt(NICNo.substr(4, 3));
            }

            // Gender
            if (dayText > 500) {
            gender = "Female";
            dayText = dayText - 500;
            }else{
            gender = "Male";
            }

            // Day Digit Validation
            if(dayText < 1 && dayText > 366) {
            $('#nic-error').html("Invalid NIC NO");
            $('#nic-error').parent().addClass('has-error');
            }else{
            $('#nic-error').html("");
            $('#nic-error').parent().removeClass('has-error');
            //Month
            if(dayText > 335){
                day = dayText - 335;
                month = "December";
            }else if(dayText > 305) {
                day = dayText - 305;
                month = "November";
            }else if (dayText > 274) {
                day = dayText - 274;
                month = "October";
            }else if (dayText > 244) {
                day = dayText - 244;
                month = "September";
            }else if (dayText > 213) {
                day = dayText - 213;
                month = "Auguest";
            }else if (dayText > 182) {
                day = dayText - 182;
                month = "July";
            }else if (dayText > 152) {
                day = dayText - 152;
                month = "June";
            }else if (dayText > 121) {
                day = dayText - 121;
                month = "May";
            }else if (dayText > 91) {
                day = dayText - 91;
                month = "April";
            }else if (dayText > 60) {
                day = dayText - 60;
                month = "March";
            }else if (dayText < 32) {
                month = "January";
                day = dayText;
            }else if (dayText > 31) {
                day = dayText - 31;
                month = "Febuary";
            }

            // Show Details
            $('#gender').val(gender);
            $('#date_of_birth').val(year+'-'+month+'-'+day);
            }
        }
        }else{
        $('#gender').val('');
        $('#date_of_birth').val('');
        }
    }
    $("#nic").keyup(function(){
        find();
    });

});
