$(document).ready(function () {
   
 $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        }
    });

    
    $('#passwordUpdate').on('submit', function (event) {
        event.preventDefault();
        $('#submit').prop('disabled', true);
        $('.text-danger').html('');
        $('.form-group').removeClass('has-error');
        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                
                window.location.href = response.redirectPath;
                
            },
            error:function(response) 
            {
                if( response.status === 422 ) {
                    var errors = response.responseJSON.errors;
                    $.each(errors, function (i, m) {
                        $('.'+i).html(m);
                        $('.'+i).parent().addClass('has-error');
                        $('#submit').prop('disabled', false);
                  });
                  
                }else if( response.status === 429 ){
                    var error = response.responseJSON.error;
                    $('#messages').html('<div class="alert alert-danger">'+error+'</div>').fadeIn();
                    setTimeout(function() {
                        $('#messages').fadeOut();
                        $('#submit').prop('disabled', false);
                        if(response.responseJSON.redirectPath){
                            window.location.href = response.responseJSON.redirectPath;
                        }
                        
                    }, 2500);
                    
                }
            
            }
        })
    });
});