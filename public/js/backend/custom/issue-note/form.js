$(document).ready(function () {
    $('#formIssueNote').on('submit', function (event) {

        event.preventDefault();

        $('#submit').prop('disabled', true);
        $('.text-danger').html('');
        $('.form-group').removeClass('has-error');
        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            data: formData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {

                $('.text-danger').html('');
                $('.form-group').removeClass('has-error');
                if (response.status == false) {
                    $('#submit').prop('disabled', false);
                    $('#messages').html('<div class="alert alert-danger">'+response.message+'</div>').fadeIn();
                    setTimeout(function() {
                        $('#messages').fadeOut();
                        window.location.href =response.redirectTo;
                    }, 2500);
                }else{
                    $('#submit').prop('disabled', false);
                    $('#messages').html('<div class="alert alert-success">' + response.message + '</div>').fadeIn();
                    setTimeout(function () {
                        $('#messages').fadeOut();

                        window.location.href = response.redirectTo;
                    }, 2000);
                }

            },
            error:function(response){
                $('#submit').prop('disabled', false);
                if( response.status === 401 ){

                }
                if( response.status === 422 ) {
                var errors = response.responseJSON.errors;
                $.each(errors, function (i, m) {
                    $('#' + i + '-error').html(m);
                    $('#' + i + '-error').parent().addClass('has-error');
                });
                }

            }
        })
    });

});
