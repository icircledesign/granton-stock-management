<?php

namespace App\Repositories;

use App\Models\Permission;
use App\Repositories\Contracts\PermissionInterface;
use Illuminate\Support\Facades\Auth;

use Requesttables;

class PermissionRepository implements PermissionInterface
{
    protected $permission;

    public function __construct(Permission $permission) {
        $this->permission = $permission;
    }

    
    /**Get All Permissions*/
    public function getAllPermission()
    {
            $permissions = $this->permission->get();
        return $permissions;
        
    }

    /**Store Permission*/
    public function storePermission($request)
    {   
        try {
            if (!empty($request)) {
                $currentUser =Auth::user()->id;
                foreach ($request->name as $values) {
                    $values = explode("~", $values);
                    $name = $values[0];
                    $displayName = $values[1];

                    $permission = new $this->permission;
                    $permission->name = $name;
                    $permission->display_name = $displayName;
                    $permission->created_by =  $currentUser;
                    $permission->updated_by =  $currentUser;

                    $permission->save();
                }
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Permission has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
        
    }

    
}
