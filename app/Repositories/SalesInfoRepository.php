<?php

namespace App\Repositories;

use App\Models\SalesInfo;
use App\Repositories\Contracts\SalesInfoInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class SalesInfoRepository implements SalesInfoInterface
{
    protected $salesInfo;

    protected $currentUrl;

    public function __construct(SalesInfo $salesInfo) {
        $this->salesInfo = $salesInfo;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**Get Sales Info List*/
    public function getSalesInfoList()
    {
    }


    /**Store Sales Info*/
    public function storeSalesInfo($request)
    {
        try {
            if (!empty($request)) {
                $salesInfo = $this->salesInfo;
                $salesInfo->promotion_card_id =  $request['promotion_card_id'];
                $salesInfo->customer_name =  $request['customer_name'];
                $salesInfo->phone_no =  $request['phone_no'];
                $salesInfo->nic =  $request['nic'];
                $salesInfo->email =  $request['email'];
                $salesInfo->address = $request["address"];
                $salesInfo->city_id = $request["city_id"];
                $salesInfo->distributor_id = $request["distributor_id"];
                $salesInfo->status_id = 1;
                $salesInfo->created_by =  $request['created_by'];
                $salesInfo->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Sales Info has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

}
