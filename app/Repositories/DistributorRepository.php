<?php

namespace App\Repositories;

use App\Models\Distributor;
use App\Repositories\Contracts\DistributorInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class DistributorRepository implements DistributorInterface
{
    protected $distributor;

    protected $currentUrl;

    public function __construct(Distributor $distributor) {
        $this->distributor = $distributor;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All Distributors*/
    public function getDistributors()
    {
        $distributors = [];
        $currentUserRole =Auth::user()->roles[0]->id;
        if($currentUserRole == 5){
            $distributors = $this->distributor->where('manager_id', Auth::user()->id)->get();
        }
        return $distributors;
    }


    /**Get Distributors List*/
    public function getDistributorList()
    {
        $currentUrl = $this->currentUrl;
        $currentUserRole =Auth::user()->roles[0]->id;
        $distributors = $this->distributor->get();

        return datatables($distributors)

            ->addColumn('distributor_name', function ($distributors) {
                return $distributors->name;
            })

            ->addColumn('status', function ($distributors) {
                return '<span class="badge label '.label($distributors->status->id).'">'.$distributors->status->name.'</span>';
            })
            ->addColumn('action', function ($distributors) {
                $distributorsEdit = '';
                if( (auth()->user()->can('/distributors/{id}/edit'))){
                $distributorsEdit .= '<a href="'.url($this->currentUrl."/distributors/").$distributors->id.'/edit" class="btn btn-warning edit" id="' . $distributors->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $distributorsEdit;
            })
            ->rawColumns(['action','status','distributor_name'])
            ->make(true);

    }


    /**Store Distributor*/
    public function storeDistributor($request)
    {
        try {
            if (!empty($request)) {
                $distributor = $this->distributor;
                $distributor->name =  $request['name'];
                $distributor->phone_no =  $request['phone_no'];
                $distributor->nic =  $request['nic'];
                $distributor->manager_id =  $request['manager_id'];
                $distributor->status_id =  $request['status_id'];
                $distributor->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Distributor has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    /**Edit Distributor Details*/
    public function editDistributorDetail(int $id)
    {
        return $this->distributor->findOrFail($id);
    }


    public function updateDistributor($request,int $id){
        try {
            if (!empty($request)) {
                $distributor = $this->distributor->findOrFail($id);
                $distributor->name =  $request['name'];
                $distributor->phone_no =  $request['phone_no'];
                $distributor->nic =  $request['nic'];
                $distributor->status_id =  $request['status_id'];
                $distributor->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Distributor has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }


}
