<?php

namespace App\Repositories;

use App\Models\Promotion;
use App\Repositories\Contracts\PromotionInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class PromotionRepository implements PromotionInterface
{
    protected $promotion;

    protected $currentUrl;

    public function __construct(Promotion $promotion) {
        $this->promotion = $promotion;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All Promotions*/
    public function getAllPromotion()
    {
        $promotions = $this->promotion->get();
        return $promotions;

    }

    /**Get All Active Promotions*/
    public function activeAllPromotion()
    {
        $promotions = $this->promotion->where('status_id',1)
            ->get();
        return $promotions;
    }

    public function cardPromotionList()
    {
        $promotions = $this->promotion::join('promotion_cards', 'promotions.id', '=', 'promotion_cards.promotion_id')
        ->join('promotion_card_user', 'promotion_card_user.promotion_card_id', '=', 'promotion_cards.id')
        ->where('promotion_cards.status_id', 2)
        ->where('promotion_card_user.user_id', Auth::user()->id)
        ->groupBy('promotions.name')
        ->get(['promotions.*']);
        return $promotions;
    }

    /**Get Promotions List*/
    public function getPromotionList()
    {
        $currentUrl = $this->currentUrl;
        $promotions = $this->promotion->get();

        return datatables($promotions)

            ->editColumn('end_date', function ($promotions) {
                return is_null($promotions->end_date)? '--': $promotions->end_date;
            })

            ->addColumn('remaining_data', function ($promotions) {
                $diff_in_days = '--';
                if(!is_null($promotions->end_date)){
                    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $promotions->end_date.' 00:00:00');
                    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i',$promotions->start_date.' 23:59:59');
                    $diff_in_days = $to->diffInDays($from);

                    if($diff_in_days >= 0){
                        $diff_in_days = $diff_in_days.' days ago';
                    }else{
                        $diff_in_days ='Promotion Expired ';
                    }
                }


                return $diff_in_days;

            })

            ->addColumn('lister_id', function ($promotions) {
                return '<span class="badge label">'.$promotions->lister->user->name.'</span>';
            })


            ->addColumn('status', function ($promotions) {
                return '<span class="badge label '.label($promotions->status_id).'">'.$promotions->status->name.'</span>';
            })
            ->addColumn('action', function ($promotions) {
                $promotionsEdit = '';
                if( (auth()->user()->can('/promotions/{id}/edit'))){
                $promotionsEdit .= '<a href="'.url($this->currentUrl."/promotions/").$promotions->id.'/edit" class="btn btn-warning edit" id="' . $promotions->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $promotionsEdit;
            })
            ->rawColumns(['action','status','end_date','lister_id'])
            ->make(true);

    }


    /**Store Promotion*/
    public function storePromotion($request)
    {
        try {
            if (!empty($request)) {
                $promotion = $this->promotion;
                $promotion->name =  $request['name'];
                $promotion->promotion_code =  $request['promotion_code'];
                $promotion->start_date =  $request['start_date'];
                $promotion->end_date =  $request['end_date'];
                $promotion->lister_id =  $request['lister_id'];
                $promotion->status_id = $request["status_id"];
                $promotion->created_by =  $request['created_by'];
                $promotion->updated_by =  $request['updated_by'];
                $promotion->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Promotion has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    /**Edit Promotion Details*/
    public function editPromotionDetail(int $id)
    {
        return $this->promotion->findOrFail($id);
    }


    public function updatePromotion($request,int $id){
        try {
            if (!empty($request)) {
                $promotion = $this->promotion->findOrFail($id);
                $promotion->name =  $request['name'];
                $promotion->promotion_code =  $request['promotion_code'];
                $promotion->start_date =  $request['start_date'];
                $promotion->end_date =  $request['end_date'];
                $promotion->lister_id =  $request['lister_id'];
                $promotion->status_id = $request["status_id"];
                $promotion->updated_by =  $request['updated_by'];
                $promotion->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Promotion has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }


}
