<?php

namespace App\Repositories\Contracts;

interface SalesInfoInterface
{
    public function getSalesInfoList();

    public function storeSalesInfo($request);

}
