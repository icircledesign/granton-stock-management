<?php

namespace App\Repositories\Contracts;

interface LocationInterface
{
  	
  	public function getAllLocation();
  	
  	public function activeAllLocation();

    public function getLocationList();

    public function storeLocation($request);

    public function editLocationDetail( int $id);

    public function updateLocation($request,int $id);

}
