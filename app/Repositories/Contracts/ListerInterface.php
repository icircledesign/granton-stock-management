<?php

namespace App\Repositories\Contracts;

interface ListerInterface
{
  	
  	public function getAllLister();
  	
    public function getListerList();

    public function storeLister($request);

    public function editListerDetail( int $id);

    public function updateLister($request,int $id);

}
