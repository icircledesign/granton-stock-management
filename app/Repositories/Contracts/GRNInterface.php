<?php

namespace App\Repositories\Contracts;

interface GRNInterface
{

  	public function getAllGRN();

    public function getGRNList();

    public function storeGRN($request);

}
