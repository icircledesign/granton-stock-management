<?php

namespace App\Repositories\Contracts;

interface PermissionInterface
{
  public function getAllPermission();
  
  public function storePermission($request);

}
