<?php

namespace App\Repositories\Contracts;

interface RoleInterface
{
  
  	public function getAllRole();

    public function getRoleList();

    public function storeRole($request);

    public function editRoleDetail( int $id);

    public function updateRole($request,int $id);

}
