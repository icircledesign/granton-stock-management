<?php

namespace App\Repositories\Contracts;

interface DistributorInterface
{

  	public function getDistributors();

    public function getDistributorList();

    public function storeDistributor($request);

    public function editDistributorDetail( int $id);

    public function updateDistributor($request,int $id);

}
