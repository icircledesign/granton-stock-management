<?php

namespace App\Repositories\Contracts;

interface IssueNoteInterface
{

  	public function getAllIssueNote();

    public function getIssueNoteList();

    public function storeIssueNote($request);

}
