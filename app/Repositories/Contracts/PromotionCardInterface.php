<?php

namespace App\Repositories\Contracts;

interface PromotionCardInterface
{
    public function promotion_card_id($promotion_id,$serial_no);

    public function getSerialNumbers($promotion_id);

    public function storePromotionCard($requestData);

    public function issuePromotionCard($requestData);

    public function updatePromotionCardStatus($request);

}
