<?php

namespace App\Repositories\Contracts;

interface PromotionInterface
{

  	public function getAllPromotion();

  	public function activeAllPromotion();

  	public function cardPromotionList();

    public function getPromotionList();

    public function storePromotion($request);

    public function editPromotionDetail( int $id);

    public function updatePromotion($request,int $id);

}
