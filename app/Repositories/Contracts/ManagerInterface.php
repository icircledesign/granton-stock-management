<?php

namespace App\Repositories\Contracts;

interface ManagerInterface
{
  	
  	public function getAllManager();
  	
    public function getManagerList();

    public function storeManager($request);

    public function editManagerDetail( int $id);

    public function updateManager($request,int $id);

}
