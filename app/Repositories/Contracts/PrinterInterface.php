<?php

namespace App\Repositories\Contracts;

interface PrinterInterface
{

  	public function getAllPrinter();

    public function getPrinterList();

    public function storePrinter($request);

    public function editPrinterDetail( int $id);

    public function updatePrinter($request,int $id);

}
