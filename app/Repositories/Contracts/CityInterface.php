<?php

namespace App\Repositories\Contracts;

interface CityInterface
{
  public function getAllCities();
  
  public function getCityList();

  public function storeCity($request);

  public function editCityDetail( int $id);

  public function updateCity($request,int $id);

}
