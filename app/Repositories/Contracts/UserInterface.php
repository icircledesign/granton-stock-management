<?php

namespace App\Repositories\Contracts;

interface UserInterface
{
  	
  	public function getAllUser();
  	
  	public function activeAllUser();

    public function getUserList();

    public function storeUser($request);

    public function editUserDetail( int $id);

    public function updateUser($request,int $id);

    public function updateUserPassword($request,int $id);

}
