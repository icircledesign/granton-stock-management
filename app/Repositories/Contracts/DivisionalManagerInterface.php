<?php

namespace App\Repositories\Contracts;

interface DivisionalManagerInterface
{
  	
  	public function getAllDivisionalManager();
  	
    public function getDivisionalManagerList();
    
    public function storeDivisionalManager($request);

    public function editDivisionalManagerDetail( int $id);

    public function updateDivisionalManager($request,int $id);

}
