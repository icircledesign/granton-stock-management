<?php

namespace App\Repositories;

use App\Models\Lister;
use App\Repositories\Contracts\ListerInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class ListerRepository implements ListerInterface
{
    protected $lister;

    protected $currentUrl;

    public function __construct(Lister $lister) {
        $this->lister = $lister;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    

    /**Get All Listers*/
    public function getAllLister()
    {
        $listers = $this->lister->get();
        return $listers;
        
    }

    /**Get Listers List*/
    public function getListerList()
    {
        $currentUrl = $this->currentUrl;
        $listers = $this->lister->get();
        
        return datatables($listers)
           
            ->addColumn('lister_name', function ($listers) {
                return $listers->user->name;
            })

            ->addColumn('email', function ($listers) {
                return $listers->user->email;
            })

            ->addColumn('status', function ($listers) {
                return '<span class="badge label '.label($listers->user->status->id).'">'.$listers->user->status->name.'</span>';
            })
            ->addColumn('action', function ($listers) {
                $listersEdit = '';
                if( (auth()->user()->can('/listers/{id}/edit'))){
                $listersEdit .= '<a href="'.url($this->currentUrl."/listers/").$listers->id.'/edit" class="btn btn-warning edit" id="' . $listers->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $listersEdit;
            })
            ->rawColumns(['action','status','lister_name'])
            ->make(true);
        
    }


    /**Store Lister*/
    public function storeLister($request)
    {   
        try {
            if (!empty($request)) {
                $lister = $this->lister;
                $lister->phone_no =  $request['phone_no'];
                $lister->nic =  $request['nic'];
                $lister->user_id =  $request['user_id'];
                $lister->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Lister has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
        
    }

    /**Edit Lister Details*/
    public function editListerDetail(int $id)
    {
        return $this->lister->findOrFail($id);
    }


    public function updateLister($request,int $id){
        try {
            if (!empty($request)) {
                $lister = $this->lister->findOrFail($id);
                $lister->phone_no =  $request['phone_no'];
                $lister->nic =  $request['nic'];
                $lister->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Lister has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }

    
}
