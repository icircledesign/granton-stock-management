<?php

namespace App\Repositories;

use App\Models\GRN;
use App\Repositories\Contracts\GRNInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class GRNRepository implements GRNInterface
{
    protected $grn;

    protected $currentUrl;

    public function __construct(GRN $grn) {
        $this->grn = $grn;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All GRNs*/
    public function getAllGRN()
    {
        $grns = $this->grn->get();
        return $grns;

    }

    /**Get GRNs List*/
    public function getGRNList()
    {


    }


    /**Store GRN*/
    public function storeGRN($request)
    {
        try {
            if (!empty($request)) {
                $grn = $this->grn;
                $grn->promotion_id = $request["promotion_id"];
                $grn->start_serial_no = $request["start_serial_no"];
                $grn->end_serial_no = $request["end_serial_no"];
                $grn->printer_id = $request["printer_id"];
                $grn->status_id = $request["status_id"];
                $grn->created_by =  $request['created_by'];
                $grn->updated_by =  $request['updated_by'];
                $grn->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Couldn't store GRN", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

}
