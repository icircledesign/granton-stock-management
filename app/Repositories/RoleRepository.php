<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\Contracts\RoleInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class RoleRepository implements RoleInterface
{
    protected $role;

    protected $currentUrl;

    public function __construct(Role $role) {
        $this->role = $role;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    
    /**Get All Roles*/
    public function getAllRole()
    {
        $roles = $this->role->get();
        return $roles;
        
    }

    /**Get Roles List*/
    public function getRoleList()
    {
        $currentUrl = $this->currentUrl;
        $roles = $this->role->get();
        return datatables($roles)
            ->editColumn('created_at', function ($roles) {
                return date('Y-m-d', strtotime($roles->created_at));
            })
            ->addColumn('permissionList', function ($roles) {
                $permissionList = "";
                foreach($roles->permissions as $key => $permission){
                    $permissionList .= '<span class="badge">'.$permission->display_name.'</span>';
                }

                return $permissionList;
            })

            ->addColumn('action', function ($roles) {
                $rolesEdit = '';
                if( (auth()->user()->can('/roles/{id}/edit'))){
                $rolesEdit .= '<a href="'.url($this->currentUrl."/roles/").$roles->id.'/edit" class="btn btn-warning edit" id="' . $roles->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $rolesEdit;
            })
            ->rawColumns(['action','permissionList','created_at'])
            ->make(true);
        
    }


    /**Store Role*/
    public function storeRole($request)
    {   
        try {
            if (!empty($request)) {
                $currentUser =Auth::user()->id;
                $role = $this->role;
                $role->name = $request["name"];
                $role->description = $request["description"];
                $role->created_by =  $currentUser;
                $role->updated_by =  $currentUser;
                $role->save();
                if ($request->permissionId) {
                    $role->permissions()->sync($request->permissionId,false);
                }
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Role has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
        
    }

    /**Edit Role Details*/
    public function editRoleDetail(int $id)
    {
        return $this->role->findOrFail($id);
    }


    public function updateRole($request,int $id){
        
        try {
            if (!empty($request)) {
                $currentUser =Auth::user()->id;
                $role = $this->role->findOrFail($id);
                $role->name = $request["name"];
                $role->description = $request["description"];
                $role->updated_by =  $currentUser;
                $role->save();
                if ($request->permissionId) {
                    $role->permissions()->sync($request->permissionId,true);
                }
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Role has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }

    
}
