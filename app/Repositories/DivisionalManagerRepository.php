<?php

namespace App\Repositories;

use App\Models\DivisionalManager;
use App\Repositories\Contracts\DivisionalManagerInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class DivisionalManagerRepository implements DivisionalManagerInterface
{
    protected $divisionalManager;

    protected $currentUrl;

    public function __construct(DivisionalManager $divisionalManager) {
        $this->divisionalManager = $divisionalManager;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All Divisional Managers*/
    public function getAllDivisionalManager()
    {
        $currentUserRole =Auth::user()->roles[0]->id;
        if($currentUserRole == 4){
            $divisionalManagers = $this->divisionalManager->where('user_id',Auth::user()->id)->get();
        }else{
            $divisionalManagers = $this->divisionalManager->get();
        }
        return $divisionalManagers;

    }


    /**Get Divisional Managers List*/
    public function getDivisionalManagerList()
    {
        $currentUrl = $this->currentUrl;
        $divisionalManagers = $this->divisionalManager->get();

        return datatables($divisionalManagers)

            ->addColumn('divisionalManager_name', function ($divisionalManagers) {
                return $divisionalManagers->user->name;
            })

            ->addColumn('email', function ($divisionalManagers) {
                return $divisionalManagers->user->email;
            })

            ->addColumn('location', function ($divisionalManagers) {
                return $divisionalManagers->location->location_name;
            })

            ->addColumn('status', function ($divisionalManagers) {
                return '<span class="badge label '.label($divisionalManagers->user->status->id).'">'.$divisionalManagers->user->status->name.'</span>';
            })
            ->addColumn('action', function ($divisionalManagers) {
                $divisionalManagersEdit = '';
                if( (auth()->user()->can('/divisional-managers/{id}/edit'))){
                $divisionalManagersEdit .= '<a href="'.url($this->currentUrl."/divisional-managers/").$divisionalManagers->id.'/edit" class="btn btn-warning edit" id="' . $divisionalManagers->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $divisionalManagersEdit;
            })
            ->rawColumns(['action','status','divisionalManager_name','email','location'])
            ->make(true);

    }


    /**Store Divisional Manager*/
    public function storeDivisionalManager($request)
    {
        try {
            if (!empty($request)) {
                $divisionalManager = $this->divisionalManager;
                $divisionalManager->phone_no =  $request['phone_no'];
                $divisionalManager->nic =  $request['nic'];
                $divisionalManager->user_id =  $request['user_id'];
                $divisionalManager->location_id =  $request['location_id'];
                $divisionalManager->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Divisional Manager has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    /**Edit Divisional Manager Details*/
    public function editDivisionalManagerDetail(int $id)
    {
        return $this->divisionalManager->findOrFail($id);
    }


    public function updateDivisionalManager($request,int $id){
        try {
            if (!empty($request)) {
                $divisionalManager = $this->divisionalManager->findOrFail($id);
                $divisionalManager->phone_no =  $request['phone_no'];
                $divisionalManager->nic =  $request['nic'];
                $divisionalManager->location_id =  $request['location_id'];
                $divisionalManager->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Divisional Manager has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }


}
