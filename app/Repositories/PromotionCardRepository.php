<?php

namespace App\Repositories;

use App\Models\PromotionCard;
use App\Repositories\Contracts\PromotionCardInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class PromotionCardRepository implements PromotionCardInterface
{
    protected $promotionCard;

    protected $currentUrl;

    public function __construct(PromotionCard $promotionCard) {
        $this->promotionCard = $promotionCard;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    public function promotion_card_id($promotion_id,$serial_no)
    {
        $promotionCard = $this->promotionCard::where('promotion_id',$promotion_id)->
                    where('serial_no',$serial_no)->firstOrFail();
        return $promotionCard;
    }

    public function getSerialNumbers($promotion_id)
    {
        $currentUserRole =Auth::user()->roles[0]->id;
        if($currentUserRole == 5){
            $promotionCards = $this->promotionCard->join('promotion_card_user', 'promotion_card_user.promotion_card_id', '=', 'promotion_cards.id')
            ->where('promotion_cards.status_id', 2)
            ->where('promotion_card_user.user_id', Auth::user()->id)
            ->where('promotion_id',$promotion_id)->get();
        }else{
            $promotionCards = $this->promotionCard->join('promotion_card_user', 'promotion_card_user.promotion_card_id', '=', 'promotion_cards.id')
            ->where('promotion_cards.status_id', 2)
            ->where('promotion_id',$promotion_id)->get();
        }
        return $promotionCards;

    }

    /**Store Promotion Card*/
    public function storePromotionCard($request)
    {
        try {
            if (!empty($request)) {
                for($serial_no = $request['start_serial_no']; $serial_no <= $request["end_serial_no"]; $serial_no++) {
                    $promotionCard = new $this->promotionCard;
                    $promotionCard->promotion_id = $request["promotion_id"];
                    $promotionCard->serial_no = $serial_no;
                    $promotionCard->status_id = $request["status_id"];
                    $promotionCard->created_by =  $request['created_by'];
                    $promotionCard->updated_by =  $request['updated_by'];
                    $promotionCard->save();
                }

                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Couldn't store Promotion Cards", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    public function issuePromotionCard($request)
    {
        try {
            if (!empty($request)) {
                for($serial_no = $request['start_serial_no']; $serial_no <= $request["end_serial_no"]; $serial_no++) {
                    $promotionCard = $this->promotionCard::where('promotion_id',$request["promotion_id"])->
                    where('serial_no',$serial_no)->firstOrFail();
                    $promotionCard->status_id = 2;
                    $promotionCard->updated_by =  $request['updated_by'];
                    $promotionCard->save();
                    $promotionCard->users()->sync($request['user_id'],false);
                }

                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Couldn't store Promotion Cards", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    public function updatePromotionCardStatus($request)
    {
        try {
            if (!empty($request)) {
                $promotionCard = $this->promotionCard->findOrFail($request['promotion_card_id']);
                $promotionCard->status_id = $request['status_id'];
                $promotionCard->updated_by =  $request['updated_by'];
                $promotionCard->save();

                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Couldn't store Promotion Cards", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

}
