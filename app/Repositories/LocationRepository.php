<?php

namespace App\Repositories;

use App\Models\Location;
use App\Repositories\Contracts\LocationInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

use Datatables;

class LocationRepository implements LocationInterface
{
    protected $location;

    protected $currentUrl;

    public function __construct(Location $location) {
        $this->location = $location;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    

    /**Get All Locations*/
    public function getAllLocation()
    {
        $locations = $this->location->get();
        return $locations;
        
    }

    /**Get All Active Locations*/
    public function activeAllLocation()
    {
        $locations = $this->location->where('status_id',1)
            ->get();
        return $locations;
    }

    /**Get Locations List*/
    public function getLocationList()
    {
        $currentUrl = $this->currentUrl;
        $locations = $this->location->get();
        
        return datatables($locations)
            ->editColumn('created_at', function ($locations) {
                return date('Y-m-d', strtotime($locations->created_at));
            })
            ->editColumn('address', function ($districts) {
                return Str::words($districts->address, 10, '...');
            })

            ->addColumn('status', function ($locations) {
                return '<span class="badge label '.label($locations->status_id).'">'.$locations->status->name.'</span>';
            })
            ->addColumn('action', function ($locations) {
                $locationsEdit = '';
                if( (auth()->user()->can('/locations/{id}/edit'))){
                $locationsEdit .= '<a href="'.url($this->currentUrl."/locations/").$locations->id.'/edit" class="btn btn-warning edit" id="' . $locations->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $locationsEdit;
            })
            ->rawColumns(['address','action','status','created_at'])
            ->make(true);
        
    }


    /**Store Location*/
    public function storeLocation($request)
    {   
        try {
            if (!empty($request)) {
                $location = $this->location;
                $location->location_name =  $request['location_name'];
                $location->slug =  $request['slug'];
                $location->address =  $request['address'];
                $location->status_id = $request["status_id"];
                $location->created_by =  $request['created_by'];
                $location->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Location has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
        
    }

    /**Edit Location Details*/
    public function editLocationDetail(int $id)
    {
        return $this->location->findOrFail($id);
    }


    public function updateLocation($request,int $id){
        try {
            if (!empty($request)) {
                $location = $this->location->findOrFail($id);
                $location->location_name =  $request['location_name'];
                $location->slug =  $request['slug'];
                $location->address =  $request['address'];
                $location->status_id = $request["status_id"];
                $location->updated_by = $request["updated_by"];
                $location->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Location has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }

    
}
