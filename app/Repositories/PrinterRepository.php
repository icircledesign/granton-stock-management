<?php

namespace App\Repositories;

use App\Models\Printer;
use App\Repositories\Contracts\PrinterInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class PrinterRepository implements PrinterInterface
{
    protected $printer;

    protected $currentUrl;

    public function __construct(Printer $printer) {
        $this->printer = $printer;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All Printers*/
    public function getAllPrinter()
    {
        $printers = $this->printer->get();
        return $printers;

    }

     /**Get All Active Printers*/
     public function activeAllPrinter()
     {
         $printers = $this->printer->where('status_id',1)
             ->get();
         return $printers;
     }


    /**Get Printers List*/
    public function getPrinterList()
    {
        $currentUrl = $this->currentUrl;
        $printers = $this->printer->get();

        return datatables($printers)

            ->addColumn('printer_name', function ($printers) {
                return $printers->printer_name;
            })

            ->addColumn('email', function ($printers) {
                return $printers->email;
            })

            ->addColumn('status', function ($printers) {
                return '<span class="badge label '.label($printers->status->id).'">'.$printers->status->name.'</span>';
            })
            ->addColumn('action', function ($printers) {
                $printersEdit = '';
                if( (auth()->user()->can('/printers/{id}/edit'))){
                $printersEdit .= '<a href="'.url($this->currentUrl."/printers/").$printers->id.'/edit" class="btn btn-warning edit" id="' . $printers->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $printersEdit;
            })
            ->rawColumns(['action','status','printer_name'])
            ->make(true);

    }


    /**Store Printer*/
    public function storePrinter($request)
    {
        try {
            if (!empty($request)) {
                $printer = $this->printer;
                $printer->printer_name =  $request['printer_name'];
                $printer->email =  $request['email'];
                $printer->phone_no =  $request['phone_no'];
                $printer->contact_person =  $request['contact_person'];
                $printer->status_id = $request["status_id"];
                $printer->created_by =  $request['created_by'];
                $printer->updated_by =  $request['updated_by'];
                $printer->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Printer has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    /**Edit Printer Details*/
    public function editPrinterDetail(int $id)
    {
        return $this->printer->findOrFail($id);
    }


    public function updatePrinter($request,int $id){
        try {
            if (!empty($request)) {
                $printer = $this->printer->findOrFail($id);
                $printer->printer_name =  $request['printer_name'];
                $printer->email =  $request['email'];
                $printer->phone_no =  $request['phone_no'];
                $printer->contact_person =  $request['contact_person'];
                $printer->status_id = $request["status_id"];
                $printer->updated_by =  $request['updated_by'];
                $printer->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Printer has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }


}
