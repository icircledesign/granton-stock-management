<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contracts\UserInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class UserRepository implements UserInterface
{
    protected $user;

    protected $currentUrl;

    public function __construct(User $user) {
        $this->user = $user;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All Users*/
    public function getAllUser()
    {
        $users = $this->user->get();
        return $users;

    }

    /**Get All Active Users*/
    public function activeAllUser()
    {
        $users = $this->user->where('status_id',1)
            ->get();
        return $users;
    }

    /**Get Users List*/
    public function getUserList()
    {
        $currentUrl = $this->currentUrl;
        $users = $this->user->get();

        return datatables($users)
            ->editColumn('created_at', function ($users) {
                return date('Y-m-d', strtotime($users->created_at));
            })

            ->addColumn('status', function ($users) {
                return '<span class="badge label '.label($users->status_id).'">'.$users->status->name.'</span>';
            })
            ->addColumn('action', function ($users) {
                $usersEdit = '';
                if( (auth()->user()->can('/users/{id}/edit'))){
                $usersEdit .= '<a href="'.url($this->currentUrl."/users/").$users->id.'/edit" class="btn btn-warning edit" id="' . $users->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $usersEdit;
            })
            ->rawColumns(['action','status','created_at'])
            ->make(true);

    }


    /**Store User*/
    public function storeUser($request)
    {
        try {
            if (!empty($request)) {
                $user = $this->user;
                $user->name =  $request['name'];
                $user->email =  $request['email'];
                $user->password =  $request['password'];
                $user->status_id = $request["status_id"];
                $user->email_verified_at = now();
                $user->image = $request["image"];
                $user->created_by =  $request['created_by'];
                $user->updated_by =  $request['updated_by'];
                $user->save();
                if ($request['roleId']) {
                    $user->roles()->sync($request['roleId'],false);
                }
                return array('id' => $user->id,'status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "User has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    /**Edit User Details*/
    public function editUserDetail(int $id)
    {
        return $this->user->findOrFail($id);
    }


    public function updateUser($request,int $id){
        try {
            if (!empty($request)) {
                $user = $this->user->findOrFail($id);
                $user->name =  $request['name'];
                $user->email =  $request['email'];
                $user->status_id = $request["status_id"];
                $user->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "User has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }

    public function updateUserPassword($request,int $id){
        try {
            if (!empty($request)) {
                $user = $this->user->findOrFail($id);
                if( $request['password'] !=''){
                    $user->password =  $request['password'];
                }
                $user->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Password Change failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }


}
