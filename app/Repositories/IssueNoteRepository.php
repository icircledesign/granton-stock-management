<?php

namespace App\Repositories;

use App\Models\IssueNote;
use App\Repositories\Contracts\IssueNoteInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class IssueNoteRepository implements IssueNoteInterface
{
    protected $issueNote;

    protected $currentUrl;

    public function __construct(IssueNote $issueNote) {
        $this->issueNote = $issueNote;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All IssueNotes*/
    public function getAllIssueNote()
    {
        $issueNotes = $this->issueNote->get();
        return $issueNotes;

    }

    /**Get IssueNotes List*/
    public function getIssueNoteList()
    {


    }


    /**Store IssueNote*/
    public function storeIssueNote($request)
    {
        try {
            if (!empty($request)) {
                $issueNote = $this->issueNote;
                $issueNote->promotion_id = $request["promotion_id"];
                $issueNote->start_serial_no = $request["start_serial_no"];
                $issueNote->end_serial_no = $request["end_serial_no"];
                $issueNote->user_id = $request["user_id"];
                $issueNote->status_id = $request["status_id"];
                $issueNote->created_by =  $request['created_by'];
                $issueNote->updated_by =  null;
                $issueNote->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Couldn't store Issue Note", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

}
