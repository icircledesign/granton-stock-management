<?php

namespace App\Repositories;

use App\Models\City;
use App\Repositories\Contracts\CityInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

use Datatables;

class CityRepository implements CityInterface
{
    protected $cities;

    protected $currentUrl;

    public function __construct(City $cities) {
        $this->cities = $cities;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }   

    

    /**Get All Cities*/
    public function getAllCities()
    {
        $cities = $this->cities->get();
        return $cities;
        
    }

    /**Get Cities List*/
    public function getCityList()
    {
        $currentUrl = $this->currentUrl;
        $cities = $this->cities->get();
        
        return datatables($cities)
        
        
        ->addColumn('action', function ($city) {
            $cityEdit = '';
            if( (auth()->user()->can('/cities/{id}/edit'))){
                $cityEdit .= '<a href="'.url($this->currentUrl."/cities/").$city->id.'/edit" class="btn btn-warning edit" id="' . $city->id . '"><i class="fa fa-edit"></i> Edit</a> ';
            }    
            return $cityEdit;
        })
        ->rawColumns(['action'])
        ->make(true);
        
    }

    /**Store City*/
    public function storeCity($request)
    {   
        try {
            if (!empty($request)) {
                $cities = $this->cities;
                $cities->name =  $request['name'];
                $cities->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "City has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
        
    }

    /**Edit City Details*/
    public function editCityDetail(int $id)
    {
        return $this->cities->findOrFail($id);
    }

    public function updateCity($request,int $id){
        try {
            if (!empty($request)) {
                $cities = $this->cities->findOrFail($id);
                $cities->name =  $request['name'];
                $cities->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "City has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }

    
}
