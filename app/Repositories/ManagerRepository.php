<?php

namespace App\Repositories;

use App\Models\Manager;
use App\Repositories\Contracts\ManagerInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use Datatables;

class ManagerRepository implements ManagerInterface
{
    protected $manager;

    protected $currentUrl;

    public function __construct(Manager $manager) {
        $this->manager = $manager;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }



    /**Get All Managers*/
    public function getAllManager()
    {
        $managers = $this->manager->get();
        return $managers;

    }


    /**Get Managers List*/
    public function getManagerList()
    {
        $currentUrl = $this->currentUrl;
        $currentUserRole =Auth::user()->roles[0]->id;
        if($currentUserRole == 4){
           $managers = $this->manager::join('divisional_managers', 'managers.divisional_manager_id', '=', 'divisional_managers.id')
            ->where('divisional_managers.user_id', Auth::user()->id)
            ->get(['managers.*']);

        }else{
            $managers = $this->manager->get();
        }



        return datatables($managers)

            ->addColumn('manager_name', function ($managers) {
                return $managers->user->name;
            })

            ->addColumn('email', function ($managers) {
                return $managers->user->email;
            })

            ->addColumn('location', function ($divisionalManagers) {
                return $divisionalManagers->location->location_name;
            })

            ->addColumn('status', function ($managers) {
                return '<span class="badge label '.label($managers->user->status->id).'">'.$managers->user->status->name.'</span>';
            })
            ->addColumn('action', function ($managers) {
                $managersEdit = '';
                if( (auth()->user()->can('/managers/{id}/edit'))){
                $managersEdit .= '<a href="'.url($this->currentUrl."/managers/").$managers->id.'/edit" class="btn btn-warning edit" id="' . $managers->id . '"><i class="fa fa-edit"></i> </a> ';
                }
                return $managersEdit;
            })
            ->rawColumns(['action','status','manager_name','location','email'])
            ->make(true);

    }


    /**Store Manager*/
    public function storeManager($request)
    {
        try {
            if (!empty($request)) {
                $manager = $this->manager;
                $manager->phone_no =  $request['phone_no'];
                $manager->nic =  $request['nic'];
                $manager->user_id =  $request['user_id'];
                $manager->location_id =  $request['location_id'];
                $manager->divisional_manager_id =  $request['divisional_manager_id'];
                $manager->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Manager has been store failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }

    }

    /**Edit Manager Details*/
    public function editManagerDetail(int $id)
    {
        return $this->manager->findOrFail($id);
    }


    public function updateManager($request,int $id){
        try {
            if (!empty($request)) {
                $manager = $this->manager->findOrFail($id);
                $manager->phone_no =  $request['phone_no'];
                $manager->nic =  $request['nic'];
                $manager->location_id =  $request['location_id'];
                $manager->divisional_manager_id =  $request['divisional_manager_id'];
                $manager->save();
                return array('status' => true);
            }
        } catch (Exception $e) {
            $exceptionError = array('msg' => "Manager has been update failed", 'error' => $e->getMessage());
            \Log::error($exceptionError);
        }
    }


}
