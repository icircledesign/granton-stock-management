<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lister extends Model
{
    protected $fillable = ['id','phone_no','nic','status_id','created_by','updated_by'];
    
    protected $table = 'listers';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

}
