<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $fillable = ['id','phone_no','nic','location_id','divisional_manager_id','status_id','created_by','updated_by'];
    
    protected $table = 'managers';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function location() {
        
        return $this->belongsTo(Location::class);
    }

    public function divisional_manager() {
        
        return $this->belongsTo(DivisionalManager::class);
    }
    

}
