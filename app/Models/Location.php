<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['id','location_name','slug','address','status_id','created_by','updated_by'];
    
    protected $table = 'locations';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    // public function divisional_managers() {
    //     return $this->belongsToMany(DivisionalManager::class,'location_divisional_manager')
    //         ->withTimestamps();
    // }

    // public function managers() {
    //     return $this->belongsToMany(Manager::class,'divisional_manager_manager')
    //         ->withTimestamps();
    // }

}
