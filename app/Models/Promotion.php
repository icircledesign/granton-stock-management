<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ['id','name','promotion_code','lister_id','start_date','end_date','status_id','created_by','updated_by'];
    
    protected $table = 'promotions';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function lister()
    {
        return $this->belongsTo(Lister::class);
    }
}
