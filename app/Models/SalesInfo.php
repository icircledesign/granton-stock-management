<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesInfo extends Model
{
    protected $fillable = ['id','promotion_card_id','customer_name','phone_no','nic','email','address','city_id','distributor_id','status_id','created_by','updated_by'];

    protected $table = 'sales_info';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function promotionCard()
    {
        return $this->belongsTo(PromotionCard::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function distributor()
    {
        return $this->belongsTo(Distributor::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
