<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DivisionalManager extends Model
{
    protected $fillable = ['id','phone_no','nic','location_id','status_id','created_by','updated_by'];
    
    protected $table = 'divisional_managers';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function location() {
        
        return $this->belongsTo(Location::class);
    }

}
