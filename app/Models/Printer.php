<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Printer extends Model
{
    protected $fillable = ['id','phone_no','contact_person','status_id','created_by','updated_by'];

    protected $table = 'printers';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
