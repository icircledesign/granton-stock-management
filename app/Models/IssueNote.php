<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IssueNote extends Model
{
    protected $fillable = ['id','promotion_id','start_serial_no','end_serial_no','user_id','status_id','created_by','updated_by'];

    protected $table = 'issue_notes';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
