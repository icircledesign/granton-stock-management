<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GRN extends Model
{
    protected $fillable = ['id','promotion_id','start_serial_no','end_serial_no','printer_id','status_id','created_by','updated_by'];

    protected $table = 'grns';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
