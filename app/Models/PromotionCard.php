<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionCard extends Model
{
    protected $fillable = ['id','promotion_id','serial_no','status_id','created_by','updated_by'];

    protected $table = 'promotion_cards';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
