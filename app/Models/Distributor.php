<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    protected $fillable = ['id','phone_no','nic','name','manager_id','status_id','created_by','updated_by'];

    protected $table = 'distributors';

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function manager() {

        return $this->belongsTo(Manager::class);
    }


}
