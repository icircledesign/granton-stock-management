<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapFrontendRoutes();

        $this->mapBackEndRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        // Route::middleware('web')
        //      ->namespace($this->namespace)
        //      ->group(base_path('routes/web.php'));
    }


    protected function mapFrontendRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace.'\Frontend',
        ], function ($router) {
            require base_path('routes/frontend/web.php');
        });
    }


    protected function mapBackEndRoutes()
    {

        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace.'\BackEnd',
            'prefix' => 'admin',
        ], function ($router) {
            require base_path('routes/backend/auth.php');
            require base_path('routes/backend/dashboard.php');
            require base_path('routes/backend/permission.php');
            require base_path('routes/backend/role.php');
            require base_path('routes/backend/location.php');
            require base_path('routes/backend/lister.php');
            require base_path('routes/backend/printer.php');
            require base_path('routes/backend/promotion.php');
            require base_path('routes/backend/divisional-manager.php');
            require base_path('routes/backend/manager.php');
            require base_path('routes/backend/distributor.php');
            require base_path('routes/backend/grn.php');
            require base_path('routes/backend/issue-note.php');
            require base_path('routes/backend/sales-info.php');
            require base_path('routes/backend/update-profile.php');
            require base_path('routes/backend/error.php');

        });
    }
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
