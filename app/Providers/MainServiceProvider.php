<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\ServiceProvider;
use App\Models\User;
use App\Models\Permission;
use \Blade;


class MainServiceProvider extends ServiceProvider
{

    public function boot(GateContract $gate)
    {

        try {
            //Laravel Permissions and Roles with Gate/Can
            $permissions = Permission::with('roles')->get();
            foreach ($permissions as $permission)
            {
                $gate->define($permission->name, function(User $user) use ($permission) {
                    return $user->hasPermission($permission);
                });
            }

            // Allow Super Admin All Blade permissions
            $gate->before(function(User $user, $ability){
                if ( $user->hasAnyRoles(1) ) {
                    return true;
                }
            });


            //Convert @can to @permissions
            Blade::directive('permissions', function ($arguments) {
                list($permissions, $guard) = explode(',', $arguments.',');

                $permissions = explode('|', str_replace('\'', '', $permissions));

                $expression = "<?php if(auth({$guard})->check() && ( false";
                foreach ($permissions as $permission) {
                    $expression .= " || auth({$guard})->user()->can('{$permission}')";
                }

                return $expression . ")): ?>";
            });

            Blade::directive('endpermissions', function () {
                return '<?php endif; ?>';
            });
        } catch (\Exception $e) {
            return [];
        }
    }


    public function register()
    {

        $this->app->bind(
            'App\Repositories\Contracts\UserInterface',
            'App\Repositories\UserRepository'
        );


        $this->app->bind(
            'App\Repositories\Contracts\RoleInterface',
            'App\Repositories\RoleRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\PermissionInterface',
            'App\Repositories\PermissionRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ListerInterface',
            'App\Repositories\ListerRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\PromotionInterface',
            'App\Repositories\PromotionRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\LocationInterface',
            'App\Repositories\LocationRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\DivisionalManagerInterface',
            'App\Repositories\DivisionalManagerRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ManagerInterface',
            'App\Repositories\ManagerRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\DistributorInterface',
            'App\Repositories\DistributorRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\PrinterInterface',
            'App\Repositories\PrinterRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\GRNInterface',
            'App\Repositories\GRNRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\PromotionCardInterface',
            'App\Repositories\PromotionCardRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\CityInterface',
            'App\Repositories\CityRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\IssueNoteInterface',
            'App\Repositories\IssueNoteRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\SalesInfoInterface',
            'App\Repositories\SalesInfoRepository'
        );
    }
}
