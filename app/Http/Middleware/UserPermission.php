<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
class UserPermission
{
    public function handle(Request $request, Closure $next)
    {
        $roles = Auth::user()->roles;

        foreach ($roles as $role) {
            //Check User Role is not Super Admin
            if ($role->id != 1) {
                foreach ($role->permissions as $permission) {

                    if (Route::getFacadeRoot()->current()->getName() == $permission->display_name) {
                        return $next($request);
                    }
                }

                abort('403');

            }else{
                return $next($request);
            }


        }

    }
}
