<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Crypt;

class UpdatePromotion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $id = Crypt::decrypt($this->request->get('id'));
        return [
            'name' => 'required|string|max:255|unique:promotions,name,'.$id,
            'promotion_code' => 'required|string|max:100|unique:promotions,promotion_code,'.$id,
            'lister_id' => 'required|string|max:100',
            'start_date' => 'required|string',
        ];

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is Required',
            'name.unique' => 'Name Already Exist',
            'promotion_code.required' => 'Promotion Code is Required',
            'promotion_code.unique' => 'Promotion Code Already Exist',
            'start_date.required' => 'Select Start Date',
            'lister_id.required' => 'Select Lister Name',
        ];
    }
}
