<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Crypt;

class UpdateDistributor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = Crypt::decrypt($this->request->get('id'));
        return [
            'distributor_name' => 'required|string|max:255',
            'nic' => ['required','string','max:100','unique:distributors,nic,'.$id,'regex:/^[0-9]{1,9}(x|X|v|V)+|^[0-9]{12}+$/'],
            'phone_no' => 'required|string|max:20',
        ];

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'distributor_name.required' => 'Distributor Name is Required',
            'nic.required' => 'NIC Number is Required',
            'nic.unique' => 'NIC Number Already Exist',
            'nic.regex' => 'NIC Number Format is Invalid ',
            'phone_no.max' => 'Phone No Format is Invalid ',
            'phone_no.required' => 'Phone No is Required',
        ];
    }
}
