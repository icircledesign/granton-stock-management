<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Crypt;

class UpdatePrinter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = Crypt::decrypt($this->request->get('id'));
        return [
            'printer_name' => 'required|string|max:255|unique:printers,printer_name,'.$id,
            'email' => 'required|string|email|max:100',
            'contact_person' => 'required|string|max:100',
            'phone_no' => 'required|string|max:20',
        ];

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'printer_name.required' => 'Printer Name is Required',
            'printer_name.unique' => 'Printer Name Already Exist',
            'email.required' => 'Email is Required',
            'email.email' => 'Invalid Email Address',
            'contact_person.required' => 'Contact Person is Required',
            'phone_no.max' => 'Phone No Format is Invalid ',
            'phone_no.required' => 'Phone No is Required',
        ];
    }
}
