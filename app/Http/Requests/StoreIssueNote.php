<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CanIssue;
use App\Rules\Notexists;

class StoreIssueNote extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promotion_code' => 'required',
            'start_serial_no' =>[
                'required',
                'lt:end_serial_no',
                new CanIssue(request('promotion_code')),
                new Notexists(request('promotion_code'))
            ],
            'end_serial_no' => [
                'required',
                'gt:start_serial_no',
                new CanIssue(request('promotion_code')),
                new Notexists(request('promotion_code'))
            ],
            'manager_id' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'promotion_code.required' => 'Select Promotion Code',
            'start_serial_no.required' => 'Enter Start Serial No',
            'start_serial_no.lt' => 'Invalides Serial No (Must be Less than)',
            'end_serial_no.required' => 'Enter End Serial No',
            'end_serial_no.gt' => 'Invalides Serial No (Must be Greater than)',
            'manager_id.required' => 'Select Manager Name',
        ];
    }
}
