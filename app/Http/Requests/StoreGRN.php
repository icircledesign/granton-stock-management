<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreGRN extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promotion_code' => 'required',
            'start_serial_no' =>'required|lt:end_serial_no|unique:promotion_cards,serial_no,NULL,id,promotion_id,'.request('promotion_code'),
            'end_serial_no' => 'required|gt:start_serial_no|unique:promotion_cards,serial_no,NULL,id,promotion_id,'.request('promotion_code'),
            'printer_id' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'promotion_code.required' => 'Select Promotion Code',
            'start_serial_no.required' => 'Enter Start Serial No',
            'start_serial_no.lt' => 'Invalides Serial No (Must be Less than)',
            'start_serial_no.unique' => ' Serial No Already Exist',
            'end_serial_no.required' => 'Enter End Serial No',
            'end_serial_no.gt' => 'Invalides Serial No (Must be Greater than)',
            'end_serial_no.unique' => ' Serial No Already Exist',
            'printer_id.required' => 'Select Printers Name',
        ];
    }
}
