<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDivisionalManager extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'divisionalManager_name' => 'required|string|max:100',
            'email' => 'required|string|email|max:100|unique:users',
            'nic' => ['required','string','max:100','unique:divisional_managers','regex:/^[0-9]{1,9}(x|X|v|V)+|^[0-9]{12}+$/'],
            'phone_no' => 'required|string|max:20',
            'location_id' => 'required|numeric|max:10',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'divisionalManager_name.required' => 'Divisional Manager Name is Required',
            'email.required' => 'Email is Required',
            'email.email' => 'Invalid Email Address',
            'email.unique' => 'Email Address Already Exist',
            'nic.required' => 'NIC Number is Required',
            'nic.unique' => 'NIC Number Already Exist',
            'nic.regex' => 'NIC Number Format is Invalid ',
            'phone_no.max' => 'Phone No Format is Invalid ',
            'phone_no.required' => 'Phone No is Required',
            'location_id.required' => 'Select Location',
        ];
    }
}
