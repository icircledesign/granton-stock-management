<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CanIssue;
use App\Rules\Notexists;

class StoreSalesInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promotion_id' => 'required',
            'serial_no_id' => 'required',
            'customer_name' => 'required|max:100',
            'phone_no' => 'required|string|max:20',
            'nic' => ['required','string','max:100','regex:/^[0-9]{1,9}(x|X|v|V)+|^[0-9]{12}+$/'],
            'email' => 'email|max:100',
            'address' => 'required|string',
            'city_id' => 'required|string',
            'distributor_id' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'promotion_id.required' => 'Select Promotion Code',
            'serial_no_id.required' => 'Enter Serial No',
            'customer_name.required' => 'Enter Customer Name',
            'phone_no.max' => 'Phone No Format is Invalid ',
            'phone_no.required' => 'Phone No is Required',
            'nic.required' => 'NIC Number is Required',
            'nic.regex' => 'NIC Number Format is Invalid ',
            'email.email' => 'Invalid Email Address',
            'address.required' => 'Address is Required',
            'city_id.required' => 'Select City',
            'distributor_id.required' => 'Select Distributor Name',
        ];
    }
}
