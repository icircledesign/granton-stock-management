<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePromotion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100|unique:promotions',
            'lister_id' => 'required|string|max:100',
            'promotion_code' => 'required|string|max:100|unique:promotions',
            'start_date' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is Required',
            'name.unique' => 'Name Already Exist',
            'promotion_code.required' => 'Promotion Code is Required',
            'promotion_code.unique' => 'Promotion Code Already Exist',
            'start_date.required' => 'Select Start Date',
            'lister_id.required' => 'Select Lister Name',
        ];
    }
}
