<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',            
            'password' => 'required|min:6|different:current_password',
            'password_confirm' => 'required|same:password',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'current_password.required' => 'Current Password is Required',
            'password.required' => 'New Password is Required',
            'password.different' => 'New Password and Current Password must be different',
            'password_confirm.required' => 'Confirm New Password is Required',
            'password_confirm.same' => 'Confirm New Password is not Match',
            'password.min' => 'New Password Minimunm Length 6',
        ];
    }
}
