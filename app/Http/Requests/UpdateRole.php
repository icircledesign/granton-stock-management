<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Crypt;

class UpdateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        
        $id = Crypt::decrypt($this->request->get('id'));
        return [
            'name' => 'required|string|max:255|unique:roles,name,'.$id,
            'permissionId' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Role Name is Required',
            'name.unique' => 'Role Name Already Exist',
            'permissionId.required'=> 'Select Permission(s)',
        ];
    }
}
