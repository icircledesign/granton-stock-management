<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Crypt;

class UpdateLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $id = Crypt::decrypt($this->request->get('id'));
        return [
            'location_name' => 'required|string|max:255|unique:locations,location_name,'.$id,
            'address' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'location_name.required' => 'Name is Required',
            'address.required' => 'Address is Required',
        ];
    }
}
