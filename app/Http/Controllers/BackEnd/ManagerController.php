<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreManager;
use App\Http\Requests\UpdateManager;
use App\Repositories\Contracts\ManagerInterface;
use App\Repositories\Contracts\UserInterface;
use App\Repositories\Contracts\LocationInterface;
use App\Repositories\Contracts\DivisionalManagerInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;
use Mail;

class ManagerController extends Controller
{
    protected $currentUrl;

    protected $manager;

    protected $location;

    protected $divisionalManager;

    protected $user;

    public function __construct(ManagerInterface $manager,UserInterface $user,
        LocationInterface $location,DivisionalManagerInterface $divisionalManager) {

        $this->manager = $manager;
        $this->user = $user;
        $this->location = $location;
        $this->divisionalManager = $divisionalManager;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Manager List
    */

    public function index()
    {
        return view('backend.manager.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Manager List from Database.
    */
    public function getManagerList()
    {
       return $this->manager->getManagerList();
    }

    /**
    * Show the form for Creating a new Manager.
    */
    public function create()
    {
        $locations = $this->location->activeAllLocation();
        $divisionalManagers = $this->divisionalManager->getAllDivisionalManager();
        return view('backend.manager.form',['currentUrl' => $this->currentUrl,'locations' => $locations,
            'divisionalManagers'=>$divisionalManagers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreManager $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $length = 10;
        $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        $password = $str;

        $requestUserData = [
            'name'=> $request->manager_name,
            'email'=> $request->email,
            'password' => bcrypt('password'),
            'roleId' => 5,
            'image' => 'avatar.jpg',
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $userResult = $this->user->storeUser($requestUserData);

        if ($userResult['status']) {

            $requestData = [
                'phone_no'=> $request->country_code.$request->phone_no,
                'nic'=> $request->nic,
                'location_id' => $request->location_id,
                'divisional_manager_id' => $request->divisional_manager_id,
                'user_id'=> $userResult['id'],
            ];

            $result = $this->manager->storeManager($requestData);

            if ($result['status']) {


                $managerData = [
                    'name'=> $request->manager_name,
                    'email'=> $request->email,
                    'password'=> $password,
                    'url'=> url(route('login'))
                ];

                // Mail::send('backend.user.mail.login_details', $managerData, function( $message ) use ($managerData){
                //     $message->to($managerData['email'])
                //             ->subject('Login Details');
                // });

                return response()->json(['status' => true, 'message' => 'Manager has been successfully created', 'redirectTo' => url($this->currentUrl.'/managers')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Manager has been store failed', 'redirectTo' => url($this->currentUrl.'/managers')]);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'Manager has been store failed', 'redirectTo' => url($this->currentUrl.'/managers')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manager = $this->manager->editManagerDetail($id);

        $locations = $this->location->activeAllLocation();

        $divisionalManagers = $this->divisionalManager->getAllDivisionalManager();

        $id = Crypt::encrypt($manager->id);
        return view('backend.manager.form',['currentUrl' => $this->currentUrl,'manager' => $manager,
            'locations' => $locations, 'divisionalManagers'=>$divisionalManagers,'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateManager $request)
    {


        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));
        $managerData = $this->manager->editManagerDetail($id);

        $currentUser =Auth::user()->id;
        $userId = Crypt::decrypt($request->input('user_id'));

        $requestData = [
            'name'=> $request->manager_name,
            'email'=> $request->email,
            'status_id'=> $request->status_id,
            'updated_by'=> $currentUser,
        ];
        $result = $this->user->updateUser($requestData,$userId);

        if ($result['status']) {

            $requestData = [
                'phone_no'=> $request->country_code.$request->phone_no,
                'nic'=> $request->nic,
                'location_id' => $request->location_id,
                'divisional_manager_id' => $request->divisional_manager_id,


            ];
            $result = $this->manager->updateManager($requestData,$id);

            if ($result['status']) {

                return response()->json(['status' => true, 'message' => 'Manager has been successfully updated', 'redirectTo' => url($this->currentUrl.'/managers')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Manager has been update failed', 'redirectTo' => url($this->currentUrl.'/managers')]);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'Manager has been update failed', 'redirectTo' => url($this->currentUrl.'/managers')]);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
