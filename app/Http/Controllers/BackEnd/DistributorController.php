<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreDistributor;
use App\Http\Requests\UpdateDistributor;
use App\Repositories\Contracts\DistributorInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;

class DistributorController extends Controller
{
    protected $currentUrl;

    protected $distributor;

    public function __construct(DistributorInterface $distributor) {

        $this->distributor = $distributor;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Distributor List
    */

    public function index()
    {
        return view('backend.distributor.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Distributor List from Database.
    */
    public function getDistributorList()
    {
       return $this->distributor->getDistributorList();
    }

    /**
    * Show the form for Creating a new Distributor.
    */
    public function create()
    {
        return view('backend.distributor.form',['currentUrl' => $this->currentUrl]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDistributor $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $requestData = [
            'name'=> $request->distributor_name,
            'phone_no'=> $request->country_code.$request->phone_no,
            'nic'=> $request->nic,
            'manager_id' => $currentUser->id,
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $result = $this->distributor->storeDistributor($requestData);

        if ($result['status']) {
            return response()->json(['status' => true, 'message' => 'Distributor has been successfully created', 'redirectTo' => url($this->currentUrl.'/distributors')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Distributor has been store failed', 'redirectTo' => url($this->currentUrl.'/distributors')]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distributor = $this->distributor->editDistributorDetail($id);
        $id = Crypt::encrypt($distributor->id);
        return view('backend.distributor.form',['currentUrl' => $this->currentUrl,'distributor' => $distributor,'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDistributor $request)
    {

        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));

        $currentUser =Auth::user()->id;

        $requestData = [
            'name'=> $request->distributor_name,
            'phone_no'=> $request->country_code.$request->phone_no,
            'nic'=> $request->nic,
            'status_id'=> $request->status_id,
            'updated_by'=> $currentUser,
        ];

        $result = $this->distributor->updateDistributor($requestData,$id);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'Distributor has been successfully updated', 'redirectTo' => url($this->currentUrl.'/distributors')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Distributor has been update failed', 'redirectTo' => url($this->currentUrl.'/distributors')]);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
