<?php

namespace App\Http\Controllers\BackEnd\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Mail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be resent if the user did not receive the original email message.
    |
    */

    use VerifiesEmails;


    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }


    public function show(Request $request)
    {
        return $request->user()->hasVerifiedEmail()
                        ? redirect($this->redirectPath())
                        : view('backend.auth.verify');
    }

    public function verify(Request $request)
    {
        if ($request->route('id') == $request->user()->getKey()) {
            $request->user()->markEmailAsVerified();
        }

        return redirect($this->redirectPath());
    }


    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        $requestUserData = [
            'name'=> $request->user()->name,
            'email'=> $request->user()->email,
            'url'=>  $this->verificationUrl($request->user())
        ];


        Mail::send('backend.user.mail.email_confirmation', $requestUserData, function( $message ) use ($requestUserData){
            $message->to($requestUserData['email'])
                    ->subject('Email Confirmation');
        });
        return back()->with('resent', true);
    }

    protected function verificationUrl($user)
    {
        return URL::temporarySignedRoute(
            'verification.verify', Carbon::now()->addMinutes(60), ['id' => $user->getKey()]
        );
    }


}
