<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreLocation;
use App\Http\Requests\UpdateLocation;
use App\Repositories\Contracts\LocationInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;

class LocationController extends Controller
{
    protected $currentUrl;

    protected $location;

    public function __construct(LocationInterface $location) {
        $this->location = $location;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Location List
    */

    public function index()
    {
        return view('backend.location.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Location List from Database.
    */
    public function getLocationList()
    {
       return $this->location->getLocationList();
    }

    /**
    * Show the form for Creating a new Location.
    */
    public function create()
    {
        return view('backend.location.form',['currentUrl' => $this->currentUrl]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLocation $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();
        $requestData = [
            'location_name'=> $request->location_name,
            'slug'=> str_slug($request->location_name, '-'),
            'address'=> $request->address,
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $result = $this->location->storeLocation($requestData);

        if ($result['status']) {
            return response()->json(['status' => true, 'message' => 'Location has been successfully created', 'redirectTo' => url($this->currentUrl.'/locations')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Location has been store failed', 'redirectTo' => url($this->currentUrl.'/locations')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = $this->location->editLocationDetail($id);
        $id = Crypt::encrypt($location->id);
        return view('backend.location.form',['currentUrl' => $this->currentUrl,'location' => $location, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLocation $request)
    {


        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));
        $locationData = $this->location->editLocationDetail($id);

        $currentUser =Auth::user()->id;
        $requestData = [
            'location_name'=> $request->location_name,
            'slug'=> str_slug($request->location_name, '-'),
            'address'=> $request->address,
            'status_id'=> $request->status_id,
            'updated_by'=> $currentUser,
        ];

        $result = $this->location->updateLocation($requestData,$id);

        if ($result['status']) {
            return response()->json(['status' => true, 'message' => 'Location has been successfully updated', 'redirectTo' => url($this->currentUrl.'/locations')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Location has been updated failed', 'redirectTo' => url($this->currentUrl.'/locations')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
