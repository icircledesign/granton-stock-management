<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreIssueNote;
use App\Repositories\Contracts\IssueNoteInterface;
use App\Repositories\Contracts\PromotionInterface;
use App\Repositories\Contracts\ManagerInterface;
use App\Repositories\Contracts\PromotionCardInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Crypt;

class IssueNoteController extends Controller
{
    protected $currentUrl;

    protected $issueNote;

    protected $promotion;

    protected $manager;

    protected $promotionCard;

    public function __construct(PromotionInterface $promotion,ManagerInterface $manager, IssueNoteInterface $issueNote, PromotionCardInterface $promotionCard) {
        $this->promotion = $promotion;
        $this->manager = $manager;
        $this->issueNote = $issueNote;
        $this->promotionCard = $promotionCard;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Issue Note List
    */

    public function index()
    {
        //return view('backend.issue-note.list',['currentUrl' => $this->currentUrl]);
        $promotions = $this->promotion->activeAllPromotion();
        $managers = $this->manager->getAllManager();
        return view('backend.issue-note.form',['currentUrl' => $this->currentUrl,'promotions'=>$promotions,'managers'=>$managers]);

    }

    /**
    * Retrieve Issue Note List from Database.
    */
    public function getIssueNoteList()
    {
       return $this->issueNote->getIssueNoteList();
    }

    /**
    * Show the form for Creating a new Issue Note.
    */
    public function create()
    {
        $promotions = $this->promotion->activeAllPromotion();
        $managers = $this->manager->getAllManager();
        return view('backend.issue-note.form',['currentUrl' => $this->currentUrl,'promotions'=>$promotions,'managers'=>$managers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIssueNote $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $requestData = [
            'promotion_id'=> $request->promotion_code,
            'start_serial_no'=> $request->start_serial_no,
            'end_serial_no'=> $request->end_serial_no,
            'user_id'=> $request->manager_id,
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> $currentUser->id,
        ];

        $result = $this->issueNote->storeIssueNote($requestData);

        $result = $this->promotionCard->issuePromotionCard($requestData);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'Issue Note has been successfully created', 'redirectTo' => url($this->currentUrl.'/issue-notes/new')]);
        }else{
            return response()->json(['status' => false, 'message' => "Couldn't store Issue Note", 'redirectTo' => url($this->currentUrl.'/issue-notes/new')]);
        }

    }

}
