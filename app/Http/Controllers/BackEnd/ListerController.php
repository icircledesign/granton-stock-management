<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreLister;
use App\Http\Requests\UpdateLister;
use App\Repositories\Contracts\ListerInterface;
use App\Repositories\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;
use Mail;

class ListerController extends Controller
{
    protected $currentUrl;

    protected $lister;

    protected $user;

    public function __construct(ListerInterface $lister,UserInterface $user) {
        $this->lister = $lister;
        $this->user = $user;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Lister List
    */

    public function index()
    {
        return view('backend.lister.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Lister List from Database.
    */
    public function getListerList()
    {
       return $this->lister->getListerList();
    }

    /**
    * Show the form for Creating a new Lister.
    */
    public function create()
    {
        return view('backend.lister.form',['currentUrl' => $this->currentUrl]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLister $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $length = 10;
        $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        $password = $str;

        $requestUserData = [
            'name'=> $request->lister_name,
            'email'=> $request->email,
            'password' => bcrypt('password'),
            'roleId' => 3,
            'image' => 'avatar.jpg',
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $userResult = $this->user->storeUser($requestUserData);

        if ($userResult['status']) {

            $requestData = [
                'phone_no'=> $request->country_code.$request->phone_no,
                'nic'=> $request->nic,
                'user_id'=> $userResult['id'],
            ];

            $result = $this->lister->storeLister($requestData);

            if ($result['status']) {


                $listerData = [
                    'name'=> $request->lister_name,
                    'email'=> $request->email,
                    'password'=> $password,
                    'url'=> url(route('login'))
                ];

                // Mail::send('backend.user.mail.login_details', $listerData, function( $message ) use ($listerData){
                //     $message->to($listerData['email'])
                //             ->subject('Login Details');
                // });

                return response()->json(['status' => true, 'message' => 'Lister has been successfully created', 'redirectTo' => url($this->currentUrl.'/listers')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Lister has been store failed', 'redirectTo' => url($this->currentUrl.'/listers')]);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'Lister has been store failed', 'redirectTo' => url($this->currentUrl.'/listers')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lister = $this->lister->editListerDetail($id);
        $id = Crypt::encrypt($lister->id);
        return view('backend.lister.form',['currentUrl' => $this->currentUrl,'lister' => $lister, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLister $request)
    {


        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));
        $listerData = $this->lister->editListerDetail($id);

        $currentUser =Auth::user()->id;
        $userId = Crypt::decrypt($request->input('user_id'));

        $requestData = [
            'name'=> $request->lister_name,
            'email'=> $request->email,
            'status_id'=> $request->status_id,
            'updated_by'=> $currentUser,
        ];
        $result = $this->user->updateUser($requestData,$userId);

        if ($result['status']) {

            $requestData = [
                'phone_no'=> $request->country_code.$request->phone_no,
                'nic'=> $request->nic,

            ];
            $result = $this->lister->updateLister($requestData,$id);

            if ($result['status']) {

                return response()->json(['status' => true, 'message' => 'Lister has been successfully updated', 'redirectTo' => url($this->currentUrl.'/listers')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Lister has been update failed', 'redirectTo' => url($this->currentUrl.'/listers')]);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'Lister has been update failed', 'redirectTo' => url($this->currentUrl.'/listers')]);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
