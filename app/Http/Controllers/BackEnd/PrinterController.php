<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StorePrinter;
use App\Http\Requests\UpdatePrinter;
use App\Repositories\Contracts\PrinterInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Crypt;

class PrinterController extends Controller
{
    protected $currentUrl;

    protected $printer;

    public function __construct(PrinterInterface $printer) {
        $this->printer = $printer;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Printer List
    */

    public function index()
    {
        return view('backend.printer.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Printer List from Database.
    */
    public function getPrinterList()
    {
       return $this->printer->getPrinterList();
    }

    /**
    * Show the form for Creating a new Printer.
    */
    public function create()
    {
        return view('backend.printer.form',['currentUrl' => $this->currentUrl]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePrinter $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $requestData = [
            'printer_name'=> $request->printer_name,
            'email'=> $request->email,
            'phone_no'=> $request->country_code.$request->phone_no,
            'contact_person'=> $request->contact_person,
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $result = $this->printer->storePrinter($requestData);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'Printer has been successfully created', 'redirectTo' => url($this->currentUrl.'/printers')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Printer has been store failed', 'redirectTo' => url($this->currentUrl.'/printers')]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $printer = $this->printer->editPrinterDetail($id);
        $id = Crypt::encrypt($printer->id);
        return view('backend.printer.form',['currentUrl' => $this->currentUrl,'printer' => $printer, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePrinter $request)
    {
        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));
        $printerData = $this->printer->editPrinterDetail($id);

        $currentUser =Auth::user()->id;

        $requestData = [
            'printer_name'=> $request->printer_name,
            'email'=> $request->email,
            'phone_no'=> $request->country_code.$request->phone_no,
            'contact_person'=> $request->contact_person,
            'status_id'=> $request->status_id,
            'updated_by'=> $currentUser,
        ];

        $result = $this->printer->updatePrinter($requestData,$id);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'Printer has been successfully updated', 'redirectTo' => url($this->currentUrl.'/printers')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Printer has been update failed', 'redirectTo' => url($this->currentUrl.'/printers')]);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
