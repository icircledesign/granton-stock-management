<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreGRN;
use App\Repositories\Contracts\GRNInterface;
use App\Repositories\Contracts\PromotionInterface;
use App\Repositories\Contracts\PrinterInterface;
use App\Repositories\Contracts\PromotionCardInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Crypt;

class GRNController extends Controller
{
    protected $currentUrl;

    protected $grn;

    protected $promotion;

    protected $printer;

    protected $promotionCard;

    public function __construct(PromotionInterface $promotion,PrinterInterface $printer, GRNInterface $grn, PromotionCardInterface $promotionCard) {
        $this->promotion = $promotion;
        $this->printer = $printer;
        $this->grn = $grn;
        $this->promotionCard = $promotionCard;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the GRN List
    */

    public function index()
    {
        //return view('backend.grn.list',['currentUrl' => $this->currentUrl]);
        $promotions = $this->promotion->activeAllPromotion();
        $printers = $this->printer->activeAllPrinter();
        return view('backend.grn.form',['currentUrl' => $this->currentUrl,'promotions'=>$promotions,'printers'=>$printers]);

    }

    /**
    * Retrieve GRN List from Database.
    */
    public function getGRNList()
    {
       return $this->grn->getGRNList();
    }

    /**
    * Show the form for Creating a new GRN.
    */
    public function create()
    {
        $promotions = $this->promotion->activeAllPromotion();
        $printers = $this->printer->activeAllPrinter();
        return view('backend.grn.form',['currentUrl' => $this->currentUrl,'promotions'=>$promotions,'printers'=>$printers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGRN $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $requestData = [
            'promotion_id'=> $request->promotion_code,
            'start_serial_no'=> $request->start_serial_no,
            'end_serial_no'=> $request->end_serial_no,
            'printer_id'=> $request->printer_id,
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $result = $this->grn->storeGRN($requestData);

        $result = $this->promotionCard->storePromotionCard($requestData);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'GRN has been successfully created', 'redirectTo' => url($this->currentUrl.'/grns/create')]);
        }else{
            return response()->json(['status' => false, 'message' => "Couldn't store GRN", 'redirectTo' => url($this->currentUrl.'/grns/create')]);
        }

    }

}
