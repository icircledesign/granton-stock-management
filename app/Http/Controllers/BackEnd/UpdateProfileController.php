<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\UpdateUserPassword;
use App\Repositories\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;
use Illuminate\Support\Facades\Hash;

class UpdateProfileController extends Controller
{
    protected $currentUrl;
    
    protected $user;
    
    public function __construct(UserInterface $user) {
        $this->user = $user;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }
    
    /**Show the Users List  */
    
    public function index()
    {  
        return view('backend.update-profile.form',['currentUrl' => $this->currentUrl]);
    }
       
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateUserPassword $request)
    {
        $validated = $request->validated();
        
        $user = $this->user->editUserDetail(auth()->user()->id);
        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json(['errors' => ['current_password'=> ['Current Password does not Match']]], 422);
        }

        $userId = Crypt::decrypt($request->input('user_id'));
        if(($request->password != '') && ($request->has('password'))){
            $password = $request->password;
            $requestData = [
                'password' => bcrypt($password),
            ];
            $result = $this->user->updateUserPassword($requestData,$userId);
        }

        if(($request->password != '') && ($request->has('password'))){
            if ($result['status']) {
                return response()->json(['status' => true, 'message' => 'Password Change successfully', 'redirectTo' => url($this->currentUrl.'/logout')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Password Change failed', 'redirectTo' => url($this->currentUrl.'/update-profile')]);
            }
        }

        
    }

}
