<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreDivisionalManager;
use App\Http\Requests\UpdateDivisionalManager;
use App\Repositories\Contracts\DivisionalManagerInterface;
use App\Repositories\Contracts\UserInterface;
use App\Repositories\Contracts\LocationInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;
use Mail;

class DivisionalManagerController extends Controller
{
    protected $currentUrl;

    protected $divisionalManager;

    protected $location;

    protected $user;

    public function __construct(DivisionalManagerInterface $divisionalManager,UserInterface $user,
        LocationInterface $location) {

        $this->divisionalManager = $divisionalManager;
        $this->user = $user;
        $this->location = $location;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Divisional Manager List
    */

    public function index()
    {
        return view('backend.divisional-manager.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Divisional Manager List from Database.
    */
    public function getDivisionalManagerList()
    {
       return $this->divisionalManager->getDivisionalManagerList();
    }

    /**
    * Show the form for Creating a new Divisional Manager.
    */
    public function create()
    {
        $locations = $this->location->activeAllLocation();
        return view('backend.divisional-manager.form',['currentUrl' => $this->currentUrl,'locations' => $locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDivisionalManager $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $length = 10;
        $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        $password = $str;

        $requestUserData = [
            'name'=> $request->divisionalManager_name,
            'email'=> $request->email,
            'password' => bcrypt('password'),
            'roleId' => 4,
            'image' => 'avatar.jpg',
            'status_id'=> 1,
            'created_by'=> $currentUser->id,
            'updated_by'=> null,
        ];

        $userResult = $this->user->storeUser($requestUserData);

        if ($userResult['status']) {

            $requestData = [
                'phone_no'=> $request->country_code.$request->phone_no,
                'nic'=> $request->nic,
                'location_id' => $request->location_id,
                'user_id'=> $userResult['id'],
            ];

            $result = $this->divisionalManager->storeDivisionalManager($requestData);

            if ($result['status']) {


                $divisionalManagerData = [
                    'name'=> $request->divisionalManager_name,
                    'email'=> $request->email,
                    'password'=> $password,
                    'url'=> url(route('login'))
                ];

                // Mail::send('backend.user.mail.login_details', $divisionalManagerData, function( $message ) use ($divisionalManagerData){
                //     $message->to($divisionalManagerData['email'])
                //             ->subject('Login Details');
                // });

                return response()->json(['status' => true, 'message' => 'Divisional Manager has been successfully created', 'redirectTo' => url($this->currentUrl.'/divisional-managers')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Divisional Manager has been store failed', 'redirectTo' => url($this->currentUrl.'/divisional-managers')]);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'Divisional Manager has been store failed', 'redirectTo' => url($this->currentUrl.'/divisional-managers')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $divisionalManager = $this->divisionalManager->editDivisionalManagerDetail($id);
        $locations = $this->location->activeAllLocation();
        $id = Crypt::encrypt($divisionalManager->id);
        return view('backend.divisional-manager.form',['currentUrl' => $this->currentUrl,'divisionalManager' => $divisionalManager,'locations' => $locations, 'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDivisionalManager $request)
    {


        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));
        $divisionalManagerData = $this->divisionalManager->editDivisionalManagerDetail($id);

        $currentUser =Auth::user()->id;
        $userId = Crypt::decrypt($request->input('user_id'));

        $requestData = [
            'name'=> $request->divisionalManager_name,
            'email'=> $request->email,
            'status_id'=> $request->status_id,
            'updated_by'=> $currentUser,
        ];
        $result = $this->user->updateUser($requestData,$userId);

        if ($result['status']) {

            $requestData = [
                'phone_no'=> $request->country_code.$request->phone_no,
                'nic'=> $request->nic,
                'location_id' => $request->location_id,
            ];

            $result = $this->divisionalManager->updateDivisionalManager($requestData,$id);

            if ($result['status']) {

                return response()->json(['status' => true, 'message' => 'Divisional Manager has been successfully updated', 'redirectTo' => url($this->currentUrl.'/divisional-managers')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Divisional Manager has been update failed', 'redirectTo' => url($this->currentUrl.'/divisional-managers')]);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'Divisional Manager has been update failed', 'redirectTo' => url($this->currentUrl.'/divisional-managers')]);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
