<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreSalesInfo;
use App\Repositories\Contracts\PromotionInterface;
use App\Repositories\Contracts\SalesInfoInterface;
use App\Repositories\Contracts\ManagerInterface;
use App\Repositories\Contracts\PromotionCardInterface;
use App\Repositories\Contracts\DistributorInterface;
use App\Repositories\Contracts\CityInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Crypt;

class SalesInfoController extends Controller
{
    protected $currentUrl;

    protected $promotion;

    protected $salesInfo;

    protected $manager;

    protected $promotionCard;

    protected $city;

    protected $distributor;

    public function __construct(PromotionInterface $promotion,ManagerInterface $manager,PromotionCardInterface $promotionCard,
    CityInterface $city, DistributorInterface $distributor,SalesInfoInterface $salesInfo) {
        $this->promotion = $promotion;
        $this->salesInfo = $salesInfo;
        $this->manager = $manager;
        $this->promotionCard = $promotionCard;
        $this->city = $city;
        $this->distributor = $distributor;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the form for Creating a new sales info
    */

    public function index()
    {
        $currentUserRole =Auth::user()->roles[0]->id;
        if($currentUserRole == 5){
            $promotions = $this->promotion->cardPromotionList();
        }else{
            $promotions = $this->promotion->activeAllPromotion();
        }
        $distributors = $this->distributor->getDistributors();
        $cities = $this->city->getAllCities();
        return view('backend.sales-info.form',['currentUrl' => $this->currentUrl,'promotions'=>$promotions,'cities' => $cities, 'distributors' =>$distributors]);

    }

    /**
    * Retrieve Issue Note List from Database.
    */
    public function getSalesInfoList()
    {
       return $this->salesInfo->getSalesInfoList();
    }

    /**
    * Show the form for Creating a new sales info.
    */
    public function create()
    {
        $currentUserRole =Auth::user()->roles[0]->id;
        if($currentUserRole == 5){
            $promotions = $this->promotion->cardPromotionList();
        }else{
            $promotions = $this->promotion->activeAllPromotion();
        }
        $distributors = $this->distributor->getDistributors();
        $cities = $this->city->getAllCities();
        return view('backend.sales-info.form',['currentUrl' => $this->currentUrl,'promotions'=>$promotions,'cities' => $cities, 'distributors' =>$distributors]);
    }


    public function getSerialNumbers(Request $request)
    {
        $promotionCards = $this->promotionCard->getSerialNumbers($request->promotion_id);
        $data = view('backend.sales-info.ajax-options',['promotionCards' => $promotionCards])->render();
        return response()->json(['options'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSalesInfo $request)
    {
        $currentUser =Auth::user();
        $validated = $request->validated();

        $promotion_card = $this->promotionCard->promotion_card_id($request->promotion_id,$request->serial_no_id);
        if($promotion_card){
            $requestData = [
                'promotion_card_id'=> $promotion_card->id,
                'customer_name'=> $request->customer_name,
                'phone_no'=> $request->phone_no,
                'nic'=> $request->nic,
                'email'=> $request->email,
                'address'=> $request->address,
                'city_id'=> $request->city_id,
                'distributor_id'=> $request->distributor_id,
                'status_id'=> 3,
                'created_by'=> $currentUser->id,
                'updated_by'=> null,
            ];
            $result = $this->salesInfo->storeSalesInfo($requestData);

            $this->promotionCard->updatePromotionCardStatus($requestData);

            if ($result['status']) {

                return response()->json(['status' => true, 'message' => 'Sales Info has been successfully created', 'redirectTo' => url($this->currentUrl.'/sales-info/new')]);
            }else{
                return response()->json(['status' => false, 'message' => "Couldn't store Sales Info", 'redirectTo' => url($this->currentUrl.'/sales-info/new')]);
            }
        }


    }

}
