<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StorePermission;
use App\Repositories\Contracts\PermissionInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Crypt;

class PermissionController extends Controller
{
    protected $currentUrl;

    protected $permission;

    public function __construct(PermissionInterface $permission) {
        $this->permission = $permission;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Permission List
    */

    public function index()
    {
        $routes = [];

        foreach (Route::getRoutes()->getRoutes() as $route){
            if(is_array($route->action['middleware'])){
                if(in_array('UserPermission',$route->action['middleware'])){
                    $routeUrl = url($route->uri());
                    $routeUrl = str_replace($this->currentUrl,'',$routeUrl);
                    $routeName = $route->getName();
                    $item = array(
                        'routeUrl' => $routeUrl,
                        'routeName' => $routeName
                    );
                    $routes[] = $item;
                }
            };
        }

        //Remove Duplicate
        $newArray = array();
        $usedFruits = array();
        foreach ( $routes AS $key => $line ) {
            if ( !in_array($line['routeName'], $usedFruits) ) {
                $usedFruits[] = $line['routeName'];
                $newArray[$key] = $line;
            }
        }
        $routeList = $newArray;


        $permissions = $this->permission->getAllPermission();
        $userPermission = $permissions->pluck('display_name')->toArray();



       return view('backend.permission.form',['routes' =>$routeList,'userPermission'=>$userPermission,'currentUrl' => $this->currentUrl]);
    }



    /**
    * Store a Newly Created Permission in Storage.
    */
    public function store(StorePermission $request)
    {
            $validated = $request->validated();

            $result = $this->permission->storePermission($request);

            if ($result['status']) {
                return response()->json(['status' => true, 'message' => 'Permission has been successfully created', 'redirectTo' => url($this->currentUrl.'/permissions')]);
            }else{
                return response()->json(['status' => false, 'message' => 'Permission has been store failed', 'redirectTo' => url($this->currentUrl.'/permissions')]);
            }


    }

}
