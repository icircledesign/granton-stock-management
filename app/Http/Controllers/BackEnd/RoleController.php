<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StoreRole;
use App\Http\Requests\UpdateRole;
use App\Repositories\Contracts\PermissionInterface;
use App\Repositories\Contracts\RoleInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use \Crypt;

class RoleController extends Controller
{
    protected $currentUrl;

    protected $permission;
    protected $role;

    public function __construct(PermissionInterface $permission,RoleInterface $role) {
        $this->permission = $permission;
        $this->role = $role;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Role List
    */

    public function index()
    {
        return view('backend.role.list',['currentUrl' => $this->currentUrl]);
    }

    /**
    * Retrieve Role List from Database.
    */
    public function getRoleList()
    {
        return $this->role->getRoleList();
    }

    /**
    * Show the form for Creating a new Role.
    */
    public function create()
    {
        $permissions = $this->permission->getAllPermission();
        return view('backend.role.form',['permissions' => $permissions,'currentUrl' => $this->currentUrl]);
    }

    /**
    * Store a newly Created Role in Storage.
    */
    public function store(StoreRole $request)
    {
        $validated = $request->validated();

        $result = $this->role->storeRole($request);

        if ($result['status']) {
            return response()->json(['status' => true, 'message' => 'Role has been successfully created', 'redirectTo' => url($this->currentUrl.'/roles')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Role has been store failed', 'redirectTo' => url($this->currentUrl.'/roles')]);
        }
    }

    /**
    * Show the form for Editing the Specified Role.
    */
    public function edit($id)
    {
        $role = $this->role->editRoleDetail($id);
        $permissions = $this->permission->getAllPermission();
        $userPermission = $role->permissions->pluck('id','id')->toArray();
        $id = Crypt::encrypt($role->id);
        return view('backend.role.form',['currentUrl' => $this->currentUrl,'role' => $role, 'permissions' => $permissions,'userPermission' =>$userPermission, 'id' => $id]);
    }

    /**
    * Update the Specified Role in Storage.
    */
    public function update(UpdateRole $request)
    {
        $validated = $request->validated();

        $id = Crypt::decrypt($request->input('id'));

        $currentUserRole =Auth::user()->roles[0]->id;

        //check select user role isn't Super Administrator
        if($id != 1){
            $result = $this->role->updateRole($request,$id);

        }elseif($id == 1 && $currentUserRole == 1){
            $result = $this->role->updateRole($request,$id);
        }else{
            return response()->json(['status' => false, 'message' => "Sorry You Can't Update Super administrator Role", 'redirectTo' => url($this->currentUrl.'/roles')]);
        }

        if ($result['status']) {
            return response()->json(['status' => true, 'message' => 'Role has been successfully updated', 'redirectTo' => url($this->currentUrl.'/roles')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Role has been updated failed', 'redirectTo' => url($this->currentUrl.'/roles')]);
        }


    }


}
