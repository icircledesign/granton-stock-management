<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\StorePromotion;
use App\Http\Requests\UpdatePromotion;
use App\Repositories\Contracts\PromotionInterface;
use App\Repositories\Contracts\ListerInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \Crypt;
use Mail;

class PromotionController extends Controller
{
    protected $currentUrl;

    protected $promotion;

    protected $lister;

    public function __construct(PromotionInterface $promotion,ListerInterface $lister) {
        $this->promotion = $promotion;
        $this->lister = $lister;
        $this->currentUrl = url(Route::getFacadeRoot()->current()->getPrefix());
    }

    /**
    * Show the Promotion List
    */

    public function index()
    {
        return view('backend.promotion.list',['currentUrl' => $this->currentUrl]);

    }

    /**
    * Retrieve Promotion List from Database.
    */
    public function getPromotionList()
    {
       return $this->promotion->getPromotionList();
    }

    /**
    * Show the form for Creating a new Promotion.
    */
    public function create()
    {
        $listers = $this->lister->getAllLister();
        return view('backend.promotion.form',['currentUrl' => $this->currentUrl,'listers'=>$listers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePromotion $request)
    {
        $currentUser = Auth::user();


        $requestData = [
            'name' => $request->name,
            'promotion_code' => $request->promotion_code,
            'start_date' => $request->start_date,
            'end_date' => (!empty($request->end_date))?$request->end_date:null,
            'lister_id' => $request->lister_id,
            'status_id' => 1,
            'created_by' => $currentUser->id,
            'updated_by' => null
        ];

        $result = $this->promotion->storePromotion($requestData);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'Promotion has been successfully created', 'redirectTo' => url($this->currentUrl.'/promotions')]);

        }else{
            return response()->json(['status' => false, 'message' => 'Promotion has been store failed', 'redirectTo' => url($this->currentUrl.'/promotions')]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotion = $this->promotion->editPromotionDetail($id);
        $listers = $this->lister->getAllLister();

        $id = Crypt::encrypt($promotion->id);
        return view('backend.promotion.form',['currentUrl' => $this->currentUrl,
            'promotion' => $promotion,'listers'=>$listers,'id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePromotion $request)
    {


        $validated = $request->validated();
        $id = Crypt::decrypt($request->input('id'));
        $promotionData = $this->promotion->editPromotionDetail($id);

        $currentUser =Auth::user();

        $requestData = [
            'name' => $request->name,
            'promotion_code' => $request->promotion_code,
            'start_date' => $request->start_date,
            'end_date' => (!empty($request->end_date))?$request->end_date:null,
            'lister_id' => $request->lister_id,
            'status_id'=> $request->status_id,
            'updated_by' => $currentUser->id,
        ];

        $result = $this->promotion->updatePromotion($requestData,$id);

        if ($result['status']) {

            return response()->json(['status' => true, 'message' => 'Promotion has been successfully updated', 'redirectTo' => url($this->currentUrl.'/promotions')]);
        }else{
            return response()->json(['status' => false, 'message' => 'Promotion has been update failed', 'redirectTo' => url($this->currentUrl.'/promotions')]);
        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
