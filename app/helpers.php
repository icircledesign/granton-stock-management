<?php

function isActive($path, $active = 'active'){
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

function isCurrentMenu($path, $active = 'current-menu-item'){
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}
function label($value){

    switch($value){
        case 2:
            return 'label-warning';
        case 3:
            return 'label-info';
        case 4:
            return 'label-danger';
        default:
            return 'label-primary';
    }

}

