<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\PromotionCard;

class Notexists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $promotion_id;

    public function __construct($promotion_id)
    {
        $this->promotion_id = $promotion_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return PromotionCard::where('serial_no', '=', $value)
        ->where('promotion_id', '=', $this->promotion_id)
        ->count()==1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This Card is Not Exists';
    }
}
