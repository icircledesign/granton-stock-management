<?php
namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class PermissionsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $routes = [];
        foreach (\Route::getRoutes()->getRoutes() as $route){
            if(is_array($route->action['middleware'])){
                if(in_array('UserPermission',$route->action['middleware'])){
                    $routeUrl = url($route->uri());
                    $routeUrl = str_replace(url('admin'),'',$routeUrl);
                    $routeName = $route->getName();
                    $item = array(
                        'name' => $routeUrl,
                        'display_name' => $routeName,
                        'created_by'=> 1,
                        'updated_by'=> 1
                    );
                    $routes[] = $item;
                }
            };
        }

        //Remove Duplicate
        $routeList = array();
        $usedFruits = array();
        foreach ( $routes AS $key => $line ) {
            if ( !in_array($line['display_name'], $usedFruits) ) {
                $usedFruits[] = $line['display_name'];
                $routeList[$key] = $line;
            }
        }
        
        DB::table('permissions')->insert($routeList);
        DB::table('permissions')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')

        ]);
    }
}
