<?php
namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class RolesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Superadministrator',
                'description' => 'Superadministrator',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by'=> 1,
                'updated_by'=> 1
            ],
            [
                'name' => 'HQ Administrator',
                'description' => 'HQ Administrator',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by'=> 1,
                'updated_by'=> 1
            ],
            [
                'name' => 'Lister',
                'description' => 'Lister',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by'=> 1,
                'updated_by'=> 1
            ],
            [
                'name' => 'Divisional Managers',
                'description' => 'Divisional Managers',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by'=> 1,
                'updated_by'=> 1
            ],
            [
                'name' => 'Managers',
                'description' => 'Managers',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by'=> 1,
                'updated_by'=> 1
            ],
        ];
        DB::table('roles')->insert($data);
    }
}
