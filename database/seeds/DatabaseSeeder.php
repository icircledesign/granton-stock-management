<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StatusesTableDataSeeder::class);
        $this->call(UsersTableDataSeeder::class);
        $this->call(RolesTableDataSeeder::class);
        $this->call(RoleUserTableDataSeeder::class);
        $this->call(PermissionsTableDataSeeder::class);
        $this->call(PermissionRoleTableDataSeeder::class);
        $this->call(CitiesTableDataSeeder::class);

    }
}
