<?php
namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class CitiesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
              "id"=> 1,
              "name"=> "Akarawita"
            ],
            [
              "id"=> 2,
              "name"=> "Akuregoda"
            ],
            [
              "id"=> 3,
              "name"=> "Angoda"
            ],
            [
              "id"=> 4,
              "name"=> "Athurugiriya"
            ],
            [
              "id"=> 5,
              "name"=> "Avissawella"
            ],
            [
              "id"=> 6,
              "name"=> "Batawala"
            ],
            [
              "id"=> 7,
              "name"=> "Battaramulla"
            ],
            [
              "id"=> 8,
              "name"=> "Batugampola"
            ],
            [
              "id"=> 9,
              "name"=> "Bellanwila"
            ],
            [
              "id"=> 10,
              "name"=> "Bope"
            ],
            [
              "id"=> 11,
              "name"=> "Boralesgamuwa"
            ],
            [
              "id"=> 12,
              "name"=> "Colombo 1 Fort"
            ],
            [
              "id"=> 13,
              "name"=> "Colombo 10 Panchikawatte/Maradana"
            ],
            [
              "id"=> 14,
              "name"=> "Colombo 11 Pettah"
            ],
            [
              "id"=> 15,
              "name"=> "Colombo 12 Hulfts Dorp"
            ],
            [
              "id"=> 16,
              "name"=> "Colombo 13 Kotahena/Kochchikade"
            ],
            [
              "id"=> 17,
              "name"=> "Colombo 14 Grandpass"
            ],
            [
              "id"=> 18,
              "name"=> "Colombo 15 Mutwal/Modara/Mattakuliya/Madampitiya"
            ],
            [
              "id"=> 19,
              "name"=> "Colombo 2 Slave lsland"
            ],
            [
              "id"=> 20,
              "name"=> "Colombo 3 Kollupitiya"
            ],
            [
              "id"=> 21,
              "name"=> "Colombo 4 Bambalapitiya"
            ],
            [
              "id"=> 22,
              "name"=> "Colombo 5 Havelock town/Kirulapane South"
            ],
            [
              "id"=> 23,
              "name"=> "Colombo 6 Wellawatta/Pamankada/Kirulapane North"
            ],
            [
              "id"=> 24,
              "name"=> "Colombo 7 Cinnamon Gardens"
            ],
            [
              "id"=> 25,
              "name"=> "Colombo 8 Borella"
            ],
            [
              "id"=> 26,
              "name"=> "Colombo 9 Dematagoda"
            ],
            [
              "id"=> 27,
              "name"=> "Dedigamuwa"
            ],
            [
              "id"=> 28,
              "name"=> "Dehiwala"
            ],
            [
              "id"=> 29,
              "name"=> "Deltara"
            ],
            [
              "id"=> 30,
              "name"=> "Ethul Kotte"
            ],
            [
              "id"=> 31,
              "name"=> "Godagama"
            ],
            [
              "id"=> 32,
              "name"=> "Gothatuwa"
            ],
            [
              "id"=> 33,
              "name"=> "Habarakada"
            ],
            [
              "id"=> 34,
              "name"=> "Handapangoda"
            ],
            [
              "id"=> 35,
              "name"=> "Hanwella"
            ],
            [
              "id"=> 36,
              "name"=> "Hewainna"
            ],
            [
              "id"=> 37,
              "name"=> "Hiripitya"
            ],
            [
              "id"=> 38,
              "name"=> "Hokandara"
            ],
            [
              "id"=> 39,
              "name"=> "Homagama"
            ],
            [
              "id"=> 40,
              "name"=> "Horagala"
            ],
            [
              "id"=> 41,
              "name"=> "Ingiriya"
            ],
            [
              "id"=> 42,
              "name"=> "Kaduwela"
            ],
            [
              "id"=> 43,
              "name"=> "Kahathuduwa"
            ],
            [
              "id"=> 44,
              "name"=> "Kahawala"
            ],
            [
              "id"=> 45,
              "name"=> "Kalatuwawa"
            ],
            [
              "id"=> 46,
              "name"=> "Kaluaggala"
            ],
            [
              "id"=> 47,
              "name"=> "Kalubowila"
            ],
            [
              "id"=> 48,
              "name"=> "Kesbewa"
            ],
            [
              "id"=> 49,
              "name"=> "Kiriwattuduwa"
            ],
            [
              "id"=> 50,
              "name"=> "Kohuwala"
            ],
            [
              "id"=> 51,
              "name"=> "Kolonnawa"
            ],
            [
              "id"=> 52,
              "name"=> "Kosgama"
            ],
            [
              "id"=> 53,
              "name"=> "Kotikawatta"
            ],
            [
              "id"=> 54,
              "name"=> "Kottawa"
            ],
            [
              "id"=> 55,
              "name"=> "Kotte"
            ],
            [
              "id"=> 56,
              "name"=> "Madapatha"
            ],
            [
              "id"=> 57,
              "name"=> "Madiwela"
            ],
            [
              "id"=> 58,
              "name"=> "Maharagama"
            ],
            [
              "id"=> 59,
              "name"=> "Malabe"
            ],
            [
              "id"=> 60,
              "name"=> "Mattegoda"
            ],
            [
              "id"=> 61,
              "name"=> "Meegoda"
            ],
            [
              "id"=> 62,
              "name"=> "Meepe"
            ],
            [
              "id"=> 63,
              "name"=> "Moragahahena"
            ],
            [
              "id"=> 64,
              "name"=> "Moratuwa"
            ],
            [
              "id"=> 65,
              "name"=> "Mount Lavinia"
            ],
            [
              "id"=> 66,
              "name"=> "Mullegama"
            ],
            [
              "id"=> 67,
              "name"=> "Mulleriyawa New Town"
            ],
            [
              "id"=> 68,
              "name"=> "Napawela"
            ],
            [
              "id"=> 69,
              "name"=> "Navagamuwa"
            ],
            [
              "id"=> 70,
              "name"=> "Nawala"
            ],
            [
              "id"=> 71,
              "name"=> "Nugegoda"
            ],
            [
              "id"=> 72,
              "name"=> "Padukka"
            ],
            [
              "id"=> 73,
              "name"=> "Pannipitiya"
            ],
            [
              "id"=> 74,
              "name"=> "Pelawatta"
            ],
            [
              "id"=> 75,
              "name"=> "Peliyagoda"
            ],
            [
              "id"=> 76,
              "name"=> "Pepiliyana"
            ],
            [
              "id"=> 77,
              "name"=> "Piliyandala"
            ],
            [
              "id"=> 78,
              "name"=> "Pita Kotte"
            ],
            [
              "id"=> 79,
              "name"=> "Pitipana Homagama"
            ],
            [
              "id"=> 80,
              "name"=> "Polgasowita"
            ],
            [
              "id"=> 81,
              "name"=> "Puwakpitiya"
            ],
            [
              "id"=> 82,
              "name"=> "Rajagiriya"
            ],
            [
              "id"=> 83,
              "name"=> "Ranala"
            ],
            [
              "id"=> 84,
              "name"=> "Ratmalana"
            ],
            [
              "id"=> 85,
              "name"=> "Siddamulla"
            ],
            [
              "id"=> 86,
              "name"=> "Sri Jayawardenepura Kotte"
            ],
            [
              "id"=> 87,
              "name"=> "Talawatugoda"
            ],
            [
              "id"=> 88,
              "name"=> "Thalapathpitiya"
            ],
            [
              "id"=> 89,
              "name"=> "Tummodara"
            ],
            [
              "id"=> 90,
              "name"=> "Waga"
            ],
            [
              "id"=> 91,
              "name"=> "Watareka"
            ],
            [
              "id"=> 92,
              "name"=> "Wellampitiya"
            ],
            [
              "id"=> 93,
              "name"=> "Akaragama"
            ],
            [
              "id"=> 94,
              "name"=> "Alawala"
            ],
            [
              "id"=> 95,
              "name"=> "Ambagaspitiya"
            ],
            [
              "id"=> 96,
              "name"=> "Ambepussa"
            ],
            [
              "id"=> 97,
              "name"=> "Andiambalama"
            ],
            [
              "id"=> 98,
              "name"=> "Attanagalla"
            ],
            [
              "id"=> 99,
              "name"=> "Badalgama"
            ],
            [
              "id"=> 100,
              "name"=> "Balabowa"
            ],
            [
              "id"=> 101,
              "name"=> "Banduragoda"
            ],
            [
              "id"=> 102,
              "name"=> "Batuwatta"
            ],
            [
              "id"=> 103,
              "name"=> "Bemmulla"
            ],
            [
              "id"=> 104,
              "name"=> "Biyagama"
            ],
            [
              "id"=> 105,
              "name"=> "Biyagama IPZ"
            ],
            [
              "id"=> 106,
              "name"=> "Bokalagama"
            ],
            [
              "id"=> 107,
              "name"=> "Bollete WP"
            ],
            [
              "id"=> 108,
              "name"=> "Bopagama"
            ],
            [
              "id"=> 109,
              "name"=> "Buthpitiya"
            ],
            [
              "id"=> 110,
              "name"=> "Dagonna"
            ],
            [
              "id"=> 111,
              "name"=> "Dalugama"
            ],
            [
              "id"=> 112,
              "name"=> "Danowita"
            ],
            [
              "id"=> 113,
              "name"=> "Debahera"
            ],
            [
              "id"=> 114,
              "name"=> "Dekatana"
            ],
            [
              "id"=> 115,
              "name"=> "Delgoda"
            ],
            [
              "id"=> 116,
              "name"=> "Delwagura"
            ],
            [
              "id"=> 117,
              "name"=> "Demalagama"
            ],
            [
              "id"=> 118,
              "name"=> "Demanhandiya"
            ],
            [
              "id"=> 119,
              "name"=> "Dewalapola"
            ],
            [
              "id"=> 120,
              "name"=> "Divulapitiya"
            ],
            [
              "id"=> 121,
              "name"=> "Divuldeniya"
            ],
            [
              "id"=> 122,
              "name"=> "Dompe"
            ],
            [
              "id"=> 123,
              "name"=> "Dunagaha"
            ],
            [
              "id"=> 124,
              "name"=> "Ekala"
            ],
            [
              "id"=> 125,
              "name"=> "Ellakkala"
            ],
            [
              "id"=> 126,
              "name"=> "Essella"
            ],
            [
              "id"=> 127,
              "name"=> "Gampaha"
            ],
            [
              "id"=> 128,
              "name"=> "Ganemulla"
            ],
            [
              "id"=> 129,
              "name"=> "GonawalaWP"
            ],
            [
              "id"=> 130,
              "name"=> "Heiyanthuduwa"
            ],
            [
              "id"=> 131,
              "name"=> "Hendala"
            ],
            [
              "id"=> 132,
              "name"=> "Henegama"
            ],
            [
              "id"=> 133,
              "name"=> "Hinatiyana Madawala"
            ],
            [
              "id"=> 134,
              "name"=> "Hiswella"
            ],
            [
              "id"=> 135,
              "name"=> "Horampella"
            ],
            [
              "id"=> 136,
              "name"=> "Hunumulla"
            ],
            [
              "id"=> 137,
              "name"=> "Ihala Madampella"
            ],
            [
              "id"=> 138,
              "name"=> "Imbulgoda"
            ],
            [
              "id"=> 139,
              "name"=> "Ja-Ela"
            ],
            [
              "id"=> 140,
              "name"=> "Kadawatha"
            ],
            [
              "id"=> 141,
              "name"=> "Kadirana"
            ],
            [
              "id"=> 142,
              "name"=> "Kahatowita"
            ],
            [
              "id"=> 143,
              "name"=> "Kalagedihena"
            ],
            [
              "id"=> 144,
              "name"=> "Kaleliya"
            ],
            [
              "id"=> 145,
              "name"=> "Kandana"
            ],
            [
              "id"=> 146,
              "name"=> "Kapugoda"
            ],
            [
              "id"=> 147,
              "name"=> "Katana"
            ],
            [
              "id"=> 148,
              "name"=> "Kattuwa"
            ],
            [
              "id"=> 149,
              "name"=> "Katunayake"
            ],
            [
              "id"=> 150,
              "name"=> "Katunayake Air Force Camp"
            ],
            [
              "id"=> 151,
              "name"=> "KatunayakeFTZ"
            ],
            [
              "id"=> 152,
              "name"=> "Katuwellegama"
            ],
            [
              "id"=> 153,
              "name"=> "Kelaniya"
            ],
            [
              "id"=> 154,
              "name"=> "Kimbulapitiya"
            ],
            [
              "id"=> 155,
              "name"=> "Kiribathgoda"
            ],
            [
              "id"=> 156,
              "name"=> "Kirillawala"
            ],
            [
              "id"=> 157,
              "name"=> "Kirindiwela"
            ],
            [
              "id"=> 158,
              "name"=> "Kitalawalana"
            ],
            [
              "id"=> 159,
              "name"=> "Kitulwala"
            ],
            [
              "id"=> 160,
              "name"=> "Kochchikade"
            ],
            [
              "id"=> 161,
              "name"=> "Kotadeniyawa"
            ],
            [
              "id"=> 162,
              "name"=> "Kotugoda"
            ],
            [
              "id"=> 163,
              "name"=> "Kumbaloluwa"
            ],
            [
              "id"=> 164,
              "name"=> "Loluwagoda"
            ],
            [
              "id"=> 165,
              "name"=> "Lunugama"
            ],
            [
              "id"=> 166,
              "name"=> "Mabodale"
            ],
            [
              "id"=> 167,
              "name"=> "Madelgamuwa"
            ],
            [
              "id"=> 168,
              "name"=> "Mahabage"
            ],
            [
              "id"=> 169,
              "name"=> "Makewita"
            ],
            [
              "id"=> 170,
              "name"=> "Makola"
            ],
            [
              "id"=> 171,
              "name"=> "Malwana"
            ],
            [
              "id"=> 172,
              "name"=> "Mandawala"
            ],
            [
              "id"=> 173,
              "name"=> "Marandagahamula"
            ],
            [
              "id"=> 174,
              "name"=> "Mellawagedara"
            ],
            [
              "id"=> 175,
              "name"=> "Minuwangoda"
            ],
            [
              "id"=> 176,
              "name"=> "Mirigama"
            ],
            [
              "id"=> 177,
              "name"=> "Mithirigala"
            ],
            [
              "id"=> 178,
              "name"=> "Muddaragama"
            ],
            [
              "id"=> 179,
              "name"=> "Mudungoda"
            ],
            [
              "id"=> 180,
              "name"=> "Naranwala"
            ],
            [
              "id"=> 181,
              "name"=> "Nawana"
            ],
            [
              "id"=> 182,
              "name"=> "Nedungamuwa"
            ],
            [
              "id"=> 183,
              "name"=> "Negombo"
            ],
            [
              "id"=> 184,
              "name"=> "Nikahetikanda"
            ],
            [
              "id"=> 185,
              "name"=> "Nittambuwa"
            ],
            [
              "id"=> 186,
              "name"=> "Niwandama"
            ],
            [
              "id"=> 187,
              "name"=> "Pallewela"
            ],
            [
              "id"=> 188,
              "name"=> "Pamunugama"
            ],
            [
              "id"=> 189,
              "name"=> "Pamunuwatta"
            ],
            [
              "id"=> 190,
              "name"=> "Pasyala"
            ],
            [
              "id"=> 191,
              "name"=> "Peliyagoda"
            ],
            [
              "id"=> 192,
              "name"=> "Pepiliyawala"
            ],
            [
              "id"=> 193,
              "name"=> "Pethiyagoda"
            ],
            [
              "id"=> 194,
              "name"=> "Polpithimukulana"
            ],
            [
              "id"=> 195,
              "name"=> "Pugoda"
            ],
            [
              "id"=> 196,
              "name"=> "Radawadunna"
            ],
            [
              "id"=> 197,
              "name"=> "Radawana"
            ],
            [
              "id"=> 198,
              "name"=> "Raddolugama"
            ],
            [
              "id"=> 199,
              "name"=> "Ragama"
            ],
            [
              "id"=> 200,
              "name"=> "Ruggahawila"
            ],
            [
              "id"=> 201,
              "name"=> "Rukmale"
            ],
            [
              "id"=> 202,
              "name"=> "Seeduwa"
            ],
            [
              "id"=> 203,
              "name"=> "Siyambalape"
            ],
            [
              "id"=> 204,
              "name"=> "Talahena"
            ],
            [
              "id"=> 205,
              "name"=> "Thihariya"
            ],
            [
              "id"=> 206,
              "name"=> "Thimbirigaskatuwa"
            ],
            [
              "id"=> 207,
              "name"=> "Tittapattara"
            ],
            [
              "id"=> 208,
              "name"=> "Udathuthiripitiya"
            ],
            [
              "id"=> 209,
              "name"=> "Udugampola"
            ],
            [
              "id"=> 210,
              "name"=> "Uggalboda"
            ],
            [
              "id"=> 211,
              "name"=> "Urapola"
            ],
            [
              "id"=> 212,
              "name"=> "Uswetakeiyawa"
            ],
            [
              "id"=> 213,
              "name"=> "Veyangoda"
            ],
            [
              "id"=> 214,
              "name"=> "Walgammulla"
            ],
            [
              "id"=> 215,
              "name"=> "Walpita"
            ],
            [
              "id"=> 216,
              "name"=> "Walpola WP"
            ],
            [
              "id"=> 217,
              "name"=> "Wanaluwewa"
            ],
            [
              "id"=> 218,
              "name"=> "Wathurugama"
            ],
            [
              "id"=> 219,
              "name"=> "Watinapaha"
            ],
            [
              "id"=> 220,
              "name"=> "Wattala"
            ],
            [
              "id"=> 221,
              "name"=> "Weboda"
            ],
            [
              "id"=> 222,
              "name"=> "Wegowwa"
            ],
            [
              "id"=> 223,
              "name"=> "Welisara"
            ],
            [
              "id"=> 224,
              "name"=> "Weliveriya"
            ],
            [
              "id"=> 225,
              "name"=> "Weweldeniya"
            ],
            [
              "id"=> 226,
              "name"=> "Yakkala"
            ],
            [
              "id"=> 227,
              "name"=> "YatiyanaWP"
            ],
            [
              "id"=> 228,
              "name"=> "Agalawatta"
            ],
            [
              "id"=> 229,
              "name"=> "Alubomulla"
            ],
            [
              "id"=> 230,
              "name"=> "Alutgama"
            ],
            [
              "id"=> 231,
              "name"=> "Anguruwatota"
            ],
            [
              "id"=> 232,
              "name"=> "Baduraliya"
            ],
            [
              "id"=> 233,
              "name"=> "Bandaragama"
            ],
            [
              "id"=> 234,
              "name"=> "Bellana"
            ],
            [
              "id"=> 235,
              "name"=> "Benthara"
            ],
            [
              "id"=> 236,
              "name"=> "Beruwala"
            ],
            [
              "id"=> 237,
              "name"=> "Bolossagama"
            ],
            [
              "id"=> 238,
              "name"=> "Bombuwala"
            ],
            [
              "id"=> 239,
              "name"=> "Boralugoda"
            ],
            [
              "id"=> 240,
              "name"=> "Bulathsinhala"
            ],
            [
              "id"=> 241,
              "name"=> "Danawala Thiniyawala"
            ],
            [
              "id"=> 242,
              "name"=> "Delmella"
            ],
            [
              "id"=> 243,
              "name"=> "Dharga Town"
            ],
            [
              "id"=> 244,
              "name"=> "Diwalakada"
            ],
            [
              "id"=> 245,
              "name"=> "Dodangoda"
            ],
            [
              "id"=> 246,
              "name"=> "Dombagoda"
            ],
            [
              "id"=> 247,
              "name"=> "Galpatha"
            ],
            [
              "id"=> 248,
              "name"=> "Gamagoda"
            ],
            [
              "id"=> 249,
              "name"=> "Gonapola Junction"
            ],
            [
              "id"=> 250,
              "name"=> "Govinna"
            ],
            [
              "id"=> 251,
              "name"=> "Gurulubadda"
            ],
            [
              "id"=> 252,
              "name"=> "Halkandawila"
            ],
            [
              "id"=> 253,
              "name"=> "Haltota"
            ],
            [
              "id"=> 254,
              "name"=> "Halwala"
            ],
            [
              "id"=> 255,
              "name"=> "Halwatura"
            ],
            [
              "id"=> 256,
              "name"=> "Hedigalla Colony"
            ],
            [
              "id"=> 257,
              "name"=> "Horana"
            ],
            [
              "id"=> 258,
              "name"=> "Ittapana"
            ],
            [
              "id"=> 259,
              "name"=> "Kalawila Kiranthidiya"
            ],
            [
              "id"=> 260,
              "name"=> "Kalutara"
            ],
            [
              "id"=> 261,
              "name"=> "Kananwila"
            ],
            [
              "id"=> 262,
              "name"=> "Kandanagama"
            ],
            [
              "id"=> 263,
              "name"=> "Kehelwatta"
            ],
            [
              "id"=> 264,
              "name"=> "Kelinkanda"
            ],
            [
              "id"=> 265,
              "name"=> "Kitulgoda"
            ],
            [
              "id"=> 266,
              "name"=> "Koholana"
            ],
            [
              "id"=> 267,
              "name"=> "Kuda Uduwa"
            ],
            [
              "id"=> 268,
              "name"=> "Kumbuka"
            ],
            [
              "id"=> 269,
              "name"=> "lngiriya"
            ],
            [
              "id"=> 270,
              "name"=> "Maggona"
            ],
            [
              "id"=> 271,
              "name"=> "Mahagama"
            ],
            [
              "id"=> 272,
              "name"=> "Mahakalupahana"
            ],
            [
              "id"=> 273,
              "name"=> "Matugama"
            ],
            [
              "id"=> 274,
              "name"=> "Meegahatenna"
            ],
            [
              "id"=> 275,
              "name"=> "Meegama"
            ],
            [
              "id"=> 276,
              "name"=> "Millaniya"
            ],
            [
              "id"=> 277,
              "name"=> "Millewa"
            ],
            [
              "id"=> 278,
              "name"=> "Miwanapalana"
            ],
            [
              "id"=> 279,
              "name"=> "Molkawa"
            ],
            [
              "id"=> 280,
              "name"=> "Morapitiya"
            ],
            [
              "id"=> 281,
              "name"=> "Morontuduwa"
            ],
            [
              "id"=> 282,
              "name"=> "Nawattuduwa"
            ],
            [
              "id"=> 283,
              "name"=> "Neboda"
            ],
            [
              "id"=> 284,
              "name"=> "Padagoda"
            ],
            [
              "id"=> 285,
              "name"=> "Pahalahewessa"
            ],
            [
              "id"=> 286,
              "name"=> "Paiyagala"
            ],
            [
              "id"=> 287,
              "name"=> "Panadura"
            ],
            [
              "id"=> 288,
              "name"=> "Pannila"
            ],
            [
              "id"=> 289,
              "name"=> "Paragastota"
            ],
            [
              "id"=> 290,
              "name"=> "Paragoda"
            ],
            [
              "id"=> 291,
              "name"=> "Paraigama"
            ],
            [
              "id"=> 292,
              "name"=> "Pelanda"
            ],
            [
              "id"=> 293,
              "name"=> "Pelawatta"
            ],
            [
              "id"=> 294,
              "name"=> "Pokunuwita"
            ],
            [
              "id"=> 295,
              "name"=> "Polgampola"
            ],
            [
              "id"=> 296,
              "name"=> "Poruwedanda"
            ],
            [
              "id"=> 297,
              "name"=> "Remunagoda"
            ],
            [
              "id"=> 298,
              "name"=> "Tebuwana"
            ],
            [
              "id"=> 299,
              "name"=> "Uduwara"
            ],
            [
              "id"=> 300,
              "name"=> "Utumgama"
            ],
            [
              "id"=> 301,
              "name"=> "Veyangalla"
            ],
            [
              "id"=> 302,
              "name"=> "Wadduwa"
            ],
            [
              "id"=> 303,
              "name"=> "Walagedara"
            ],
            [
              "id"=> 304,
              "name"=> "Walallawita"
            ],
            [
              "id"=> 305,
              "name"=> "Waskaduwa"
            ],
            [
              "id"=> 306,
              "name"=> "Welipenna"
            ],
            [
              "id"=> 307,
              "name"=> "Welmilla Junction"
            ],
            [
              "id"=> 308,
              "name"=> "Yagirala"
            ],
            [
              "id"=> 309,
              "name"=> "Yatadolawatta"
            ],
            [
              "id"=> 310,
              "name"=> "Yatawara Junction"
            ],
            [
              "id"=> 311,
              "name"=> "Akurana"
            ],
            [
              "id"=> 312,
              "name"=> "Alawatugoda"
            ],
            [
              "id"=> 313,
              "name"=> "Aludeniya"
            ],
            [
              "id"=> 314,
              "name"=> "Ambagahapelessa"
            ],
            [
              "id"=> 315,
              "name"=> "Ambatenna"
            ],
            [
              "id"=> 316,
              "name"=> "Ampitiya"
            ],
            [
              "id"=> 317,
              "name"=> "Aniwatta"
            ],
            [
              "id"=> 318,
              "name"=> "Ankumbura"
            ],
            [
              "id"=> 319,
              "name"=> "Asgiriya"
            ],
            [
              "id"=> 320,
              "name"=> "Atabage"
            ],
            [
              "id"=> 321,
              "name"=> "Balagolla"
            ],
            [
              "id"=> 322,
              "name"=> "Balana"
            ],
            [
              "id"=> 323,
              "name"=> "Bambaragahaela"
            ],
            [
              "id"=> 324,
              "name"=> "Barawardhana Oya"
            ],
            [
              "id"=> 325,
              "name"=> "Batagolladeniya"
            ],
            [
              "id"=> 326,
              "name"=> "Batugoda"
            ],
            [
              "id"=> 327,
              "name"=> "Batumulla"
            ],
            [
              "id"=> 328,
              "name"=> "Bawlana"
            ],
            [
              "id"=> 329,
              "name"=> "Bopana"
            ],
            [
              "id"=> 330,
              "name"=> "Dangolla"
            ],
            [
              "id"=> 331,
              "name"=> "Danture"
            ],
            [
              "id"=> 332,
              "name"=> "Dedunupitiya"
            ],
            [
              "id"=> 333,
              "name"=> "Dekinda"
            ],
            [
              "id"=> 334,
              "name"=> "Deltota"
            ],
            [
              "id"=> 335,
              "name"=> "Digana"
            ],
            [
              "id"=> 336,
              "name"=> "Dolapihilla"
            ],
            [
              "id"=> 337,
              "name"=> "Dolosbage"
            ],
            [
              "id"=> 338,
              "name"=> "Doluwa"
            ],
            [
              "id"=> 339,
              "name"=> "Doragamuwa"
            ],
            [
              "id"=> 340,
              "name"=> "Dunuwila"
            ],
            [
              "id"=> 341,
              "name"=> "Ekiriya"
            ],
            [
              "id"=> 342,
              "name"=> "Elamulla"
            ],
            [
              "id"=> 343,
              "name"=> "Elkaduwa"
            ],
            [
              "id"=> 344,
              "name"=> "Etulgama"
            ],
            [
              "id"=> 345,
              "name"=> "Galaboda"
            ],
            [
              "id"=> 346,
              "name"=> "Galagedara"
            ],
            [
              "id"=> 347,
              "name"=> "Galaha"
            ],
            [
              "id"=> 348,
              "name"=> "Galhinna"
            ],
            [
              "id"=> 349,
              "name"=> "Gallellagama"
            ],
            [
              "id"=> 350,
              "name"=> "Gampola"
            ],
            [
              "id"=> 351,
              "name"=> "Gelioya"
            ],
            [
              "id"=> 352,
              "name"=> "Godamunna"
            ],
            [
              "id"=> 353,
              "name"=> "Gomagoda"
            ],
            [
              "id"=> 354,
              "name"=> "Gonagantenna"
            ],
            [
              "id"=> 355,
              "name"=> "Gonawalapatana"
            ],
            [
              "id"=> 356,
              "name"=> "Gunnepana"
            ],
            [
              "id"=> 357,
              "name"=> "Gurudeniya"
            ],
            [
              "id"=> 358,
              "name"=> "Halloluwa"
            ],
            [
              "id"=> 359,
              "name"=> "Handaganawa"
            ],
            [
              "id"=> 360,
              "name"=> "Handawalapitiya"
            ],
            [
              "id"=> 361,
              "name"=> "Handessa"
            ],
            [
              "id"=> 362,
              "name"=> "Hanguranketha"
            ],
            [
              "id"=> 363,
              "name"=> "Hantane"
            ],
            [
              "id"=> 364,
              "name"=> "Haragama"
            ],
            [
              "id"=> 365,
              "name"=> "Harankahawa"
            ],
            [
              "id"=> 366,
              "name"=> "Hasalaka"
            ],
            [
              "id"=> 367,
              "name"=> "Hataraliyadda"
            ],
            [
              "id"=> 368,
              "name"=> "Heerassagala"
            ],
            [
              "id"=> 369,
              "name"=> "Hewaheta"
            ],
            [
              "id"=> 370,
              "name"=> "Hindagala"
            ],
            [
              "id"=> 371,
              "name"=> "Hondiyadeniya"
            ],
            [
              "id"=> 372,
              "name"=> "Hunnasgiriya"
            ],
            [
              "id"=> 373,
              "name"=> "Jambugahapitiya"
            ],
            [
              "id"=> 374,
              "name"=> "Kadugannawa"
            ],
            [
              "id"=> 375,
              "name"=> "Kahataliyadda"
            ],
            [
              "id"=> 376,
              "name"=> "Kalugala"
            ],
            [
              "id"=> 377,
              "name"=> "Kandy"
            ],
            [
              "id"=> 378,
              "name"=> "Kapuliyadde"
            ],
            [
              "id"=> 379,
              "name"=> "Karandagolla"
            ],
            [
              "id"=> 380,
              "name"=> "Katugastota"
            ],
            [
              "id"=> 381,
              "name"=> "Kengalla"
            ],
            [
              "id"=> 382,
              "name"=> "Ketakumbura"
            ],
            [
              "id"=> 383,
              "name"=> "Ketawala Leula"
            ],
            [
              "id"=> 384,
              "name"=> "Kiribathkumbura"
            ],
            [
              "id"=> 385,
              "name"=> "Kobonila"
            ],
            [
              "id"=> 386,
              "name"=> "Kolabissa"
            ],
            [
              "id"=> 387,
              "name"=> "Kolongoda"
            ],
            [
              "id"=> 388,
              "name"=> "Kulugammana"
            ],
            [
              "id"=> 389,
              "name"=> "Kumbukkandura"
            ],
            [
              "id"=> 390,
              "name"=> "Kumburegama"
            ],
            [
              "id"=> 391,
              "name"=> "Kundasale"
            ],
            [
              "id"=> 392,
              "name"=> "Leemagahakotuwa"
            ],
            [
              "id"=> 393,
              "name"=> "Lewella"
            ],
            [
              "id"=> 394,
              "name"=> "lhala Kobbekaduwa"
            ],
            [
              "id"=> 395,
              "name"=> "lllagolla"
            ],
            [
              "id"=> 396,
              "name"=> "Lunuketiya Maditta"
            ],
            [
              "id"=> 397,
              "name"=> "Madawala Bazaar"
            ],
            [
              "id"=> 398,
              "name"=> "Madugalla"
            ],
            [
              "id"=> 399,
              "name"=> "Madulkele"
            ],
            [
              "id"=> 400,
              "name"=> "Mahadoraliyadda"
            ],
            [
              "id"=> 401,
              "name"=> "Mahamedagama"
            ],
            [
              "id"=> 402,
              "name"=> "Mailapitiya"
            ],
            [
              "id"=> 403,
              "name"=> "Makkanigama"
            ],
            [
              "id"=> 404,
              "name"=> "Makuldeniya"
            ],
            [
              "id"=> 405,
              "name"=> "Mandaram Nuwara"
            ],
            [
              "id"=> 406,
              "name"=> "Mapakanda"
            ],
            [
              "id"=> 407,
              "name"=> "Marassana"
            ],
            [
              "id"=> 408,
              "name"=> "Marymount Colony"
            ],
            [
              "id"=> 409,
              "name"=> "Maturata"
            ],
            [
              "id"=> 410,
              "name"=> "Mawatura"
            ],
            [
              "id"=> 411,
              "name"=> "Mawilmada"
            ],
            [
              "id"=> 412,
              "name"=> "Medamahanuwara"
            ],
            [
              "id"=> 413,
              "name"=> "Medawala Harispattuwa"
            ],
            [
              "id"=> 414,
              "name"=> "Meetalawa"
            ],
            [
              "id"=> 415,
              "name"=> "Megoda Kalugamuwa"
            ],
            [
              "id"=> 416,
              "name"=> "Menikdiwela"
            ],
            [
              "id"=> 417,
              "name"=> "Menikhinna"
            ],
            [
              "id"=> 418,
              "name"=> "Mimure"
            ],
            [
              "id"=> 419,
              "name"=> "Minigamuwa"
            ],
            [
              "id"=> 420,
              "name"=> "Minipe"
            ],
            [
              "id"=> 421,
              "name"=> "Murutalawa"
            ],
            [
              "id"=> 422,
              "name"=> "Muruthagahamulla"
            ],
            [
              "id"=> 423,
              "name"=> "Naranpanawa"
            ],
            [
              "id"=> 424,
              "name"=> "Nattarampotha"
            ],
            [
              "id"=> 425,
              "name"=> "Nawalapitiya"
            ],
            [
              "id"=> 426,
              "name"=> "Nillambe"
            ],
            [
              "id"=> 427,
              "name"=> "Nugaliyadda"
            ],
            [
              "id"=> 428,
              "name"=> "Nugawela"
            ],
            [
              "id"=> 429,
              "name"=> "Pallebowala"
            ],
            [
              "id"=> 430,
              "name"=> "Pallekelle"
            ],
            [
              "id"=> 431,
              "name"=> "Pallekotuwa"
            ],
            [
              "id"=> 432,
              "name"=> "Panvila"
            ],
            [
              "id"=> 433,
              "name"=> "Panwilatenna"
            ],
            [
              "id"=> 434,
              "name"=> "Paradeka"
            ],
            [
              "id"=> 435,
              "name"=> "Pasbage"
            ],
            [
              "id"=> 436,
              "name"=> "Pattitalawa"
            ],
            [
              "id"=> 437,
              "name"=> "Pattiya Watta"
            ],
            [
              "id"=> 438,
              "name"=> "Peradeniya"
            ],
            [
              "id"=> 439,
              "name"=> "Pilawala"
            ],
            [
              "id"=> 440,
              "name"=> "Pilimatalawa"
            ],
            [
              "id"=> 441,
              "name"=> "Poholiyadda"
            ],
            [
              "id"=> 442,
              "name"=> "Polgolla"
            ],
            [
              "id"=> 443,
              "name"=> "Pujapitiya"
            ],
            [
              "id"=> 444,
              "name"=> "Pupuressa"
            ],
            [
              "id"=> 445,
              "name"=> "Pussellawa"
            ],
            [
              "id"=> 446,
              "name"=> "Putuhapuwa"
            ],
            [
              "id"=> 447,
              "name"=> "Rajawella"
            ],
            [
              "id"=> 448,
              "name"=> "Rambukpitiya"
            ],
            [
              "id"=> 449,
              "name"=> "Rambukwella"
            ],
            [
              "id"=> 450,
              "name"=> "Rangala"
            ],
            [
              "id"=> 451,
              "name"=> "Rantembe"
            ],
            [
              "id"=> 452,
              "name"=> "Rathukohodigala"
            ],
            [
              "id"=> 453,
              "name"=> "Rikillagaskada"
            ],
            [
              "id"=> 454,
              "name"=> "Sangarajapura"
            ],
            [
              "id"=> 455,
              "name"=> "Senarathwela"
            ],
            [
              "id"=> 456,
              "name"=> "Talatuoya"
            ],
            [
              "id"=> 457,
              "name"=> "Tawalantenna"
            ],
            [
              "id"=> 458,
              "name"=> "Teldeniya"
            ],
            [
              "id"=> 459,
              "name"=> "Tennekumbura"
            ],
            [
              "id"=> 460,
              "name"=> "Uda Peradeniya"
            ],
            [
              "id"=> 461,
              "name"=> "Udahentenna"
            ],
            [
              "id"=> 462,
              "name"=> "Udahingulwala"
            ],
            [
              "id"=> 463,
              "name"=> "Udatalawinna"
            ],
            [
              "id"=> 464,
              "name"=> "Udawatta"
            ],
            [
              "id"=> 465,
              "name"=> "Udispattuwa"
            ],
            [
              "id"=> 466,
              "name"=> "Ududumbara"
            ],
            [
              "id"=> 467,
              "name"=> "Uduwa"
            ],
            [
              "id"=> 468,
              "name"=> "Uduwahinna"
            ],
            [
              "id"=> 469,
              "name"=> "Uduwela"
            ],
            [
              "id"=> 470,
              "name"=> "Ulapane"
            ],
            [
              "id"=> 471,
              "name"=> "Ulpothagama"
            ],
            [
              "id"=> 472,
              "name"=> "Unuwinna"
            ],
            [
              "id"=> 473,
              "name"=> "Velamboda"
            ],
            [
              "id"=> 474,
              "name"=> "Watagoda Harispattuwa"
            ],
            [
              "id"=> 475,
              "name"=> "Watapuluwa"
            ],
            [
              "id"=> 476,
              "name"=> "Wattappola"
            ],
            [
              "id"=> 477,
              "name"=> "Wattegama"
            ],
            [
              "id"=> 478,
              "name"=> "Weligalla"
            ],
            [
              "id"=> 479,
              "name"=> "Weligampola"
            ],
            [
              "id"=> 480,
              "name"=> "Wendaruwa"
            ],
            [
              "id"=> 481,
              "name"=> "Weragantota"
            ],
            [
              "id"=> 482,
              "name"=> "Werapitya"
            ],
            [
              "id"=> 483,
              "name"=> "Werellagama"
            ],
            [
              "id"=> 484,
              "name"=> "Wettawa"
            ],
            [
              "id"=> 485,
              "name"=> "Wilanagama"
            ],
            [
              "id"=> 486,
              "name"=> "Yahalatenna"
            ],
            [
              "id"=> 487,
              "name"=> "Yatihalagala"
            ],
            [
              "id"=> 488,
              "name"=> "Akuramboda"
            ],
            [
              "id"=> 489,
              "name"=> "Alwatta"
            ],
            [
              "id"=> 490,
              "name"=> "Ambana"
            ],
            [
              "id"=> 491,
              "name"=> "Ataragallewa"
            ],
            [
              "id"=> 492,
              "name"=> "Bambaragaswewa"
            ],
            [
              "id"=> 493,
              "name"=> "Beligamuwa"
            ],
            [
              "id"=> 494,
              "name"=> "Dambulla"
            ],
            [
              "id"=> 495,
              "name"=> "Dankanda"
            ],
            [
              "id"=> 496,
              "name"=> "Devagiriya"
            ],
            [
              "id"=> 497,
              "name"=> "Dewahuwa"
            ],
            [
              "id"=> 498,
              "name"=> "Dullewa"
            ],
            [
              "id"=> 499,
              "name"=> "Dunkolawatta"
            ],
            [
              "id"=> 500,
              "name"=> "Dunuwilapitiya"
            ],
            [
              "id"=> 501,
              "name"=> "Elkaduwa"
            ],
            [
              "id"=> 502,
              "name"=> "Erawula Junction"
            ],
            [
              "id"=> 503,
              "name"=> "Etanawala"
            ],
            [
              "id"=> 504,
              "name"=> "Galewela"
            ],
            [
              "id"=> 505,
              "name"=> "Gammaduwa"
            ],
            [
              "id"=> 506,
              "name"=> "Gangala Puwakpitiya"
            ],
            [
              "id"=> 507,
              "name"=> "Handungamuwa"
            ],
            [
              "id"=> 508,
              "name"=> "Hattota Amuna"
            ],
            [
              "id"=> 509,
              "name"=> "Imbulgolla"
            ],
            [
              "id"=> 510,
              "name"=> "Inamaluwa"
            ],
            [
              "id"=> 511,
              "name"=> "Kaikawala"
            ],
            [
              "id"=> 512,
              "name"=> "Kalundawa"
            ],
            [
              "id"=> 513,
              "name"=> "Kandalama"
            ],
            [
              "id"=> 514,
              "name"=> "Karagahinna"
            ],
            [
              "id"=> 515,
              "name"=> "Katudeniya"
            ],
            [
              "id"=> 516,
              "name"=> "Kavudupelella"
            ],
            [
              "id"=> 517,
              "name"=> "Kibissa"
            ],
            [
              "id"=> 518,
              "name"=> "Kiwula"
            ],
            [
              "id"=> 519,
              "name"=> "Kongahawela"
            ],
            [
              "id"=> 520,
              "name"=> "Laggala Pallegama"
            ],
            [
              "id"=> 521,
              "name"=> "Leliambe"
            ],
            [
              "id"=> 522,
              "name"=> "Lenadora"
            ],
            [
              "id"=> 523,
              "name"=> "lllukkumbura"
            ],
            [
              "id"=> 524,
              "name"=> "Madawala Ulpotha"
            ],
            [
              "id"=> 525,
              "name"=> "Madipola"
            ],
            [
              "id"=> 526,
              "name"=> "Mahawela"
            ],
            [
              "id"=> 527,
              "name"=> "Mananwatta"
            ],
            [
              "id"=> 528,
              "name"=> "Maraka"
            ],
            [
              "id"=> 529,
              "name"=> "Matale"
            ],
            [
              "id"=> 530,
              "name"=> "Melipitiya"
            ],
            [
              "id"=> 531,
              "name"=> "Metihakka"
            ],
            [
              "id"=> 532,
              "name"=> "Millawana"
            ],
            [
              "id"=> 533,
              "name"=> "Muwandeniya"
            ],
            [
              "id"=> 534,
              "name"=> "Nalanda"
            ],
            [
              "id"=> 535,
              "name"=> "Naula"
            ],
            [
              "id"=> 536,
              "name"=> "Nugagolla"
            ],
            [
              "id"=> 537,
              "name"=> "Opalgala"
            ],
            [
              "id"=> 538,
              "name"=> "Ovilikanda"
            ],
            [
              "id"=> 539,
              "name"=> "Palapathwela"
            ],
            [
              "id"=> 540,
              "name"=> "Pallepola"
            ],
            [
              "id"=> 541,
              "name"=> "Perakanatta"
            ],
            [
              "id"=> 542,
              "name"=> "Pubbiliya"
            ],
            [
              "id"=> 543,
              "name"=> "Ranamuregama"
            ],
            [
              "id"=> 544,
              "name"=> "Rattota"
            ],
            [
              "id"=> 545,
              "name"=> "Selagama"
            ],
            [
              "id"=> 546,
              "name"=> "Sigiriya"
            ],
            [
              "id"=> 547,
              "name"=> "Talagoda Junction"
            ],
            [
              "id"=> 548,
              "name"=> "Talakiriyagama"
            ],
            [
              "id"=> 549,
              "name"=> "Udasgiriya"
            ],
            [
              "id"=> 550,
              "name"=> "Udatenna"
            ],
            [
              "id"=> 551,
              "name"=> "Ukuwela"
            ],
            [
              "id"=> 552,
              "name"=> "Wahacotte"
            ],
            [
              "id"=> 553,
              "name"=> "Walawela"
            ],
            [
              "id"=> 554,
              "name"=> "Wehigala"
            ],
            [
              "id"=> 555,
              "name"=> "Welangahawatte"
            ],
            [
              "id"=> 556,
              "name"=> "Wewalawewa"
            ],
            [
              "id"=> 557,
              "name"=> "Wilgamuwa"
            ],
            [
              "id"=> 558,
              "name"=> "Yatawatta"
            ],
            [
              "id"=> 559,
              "name"=> "Agarapathana"
            ],
            [
              "id"=> 560,
              "name"=> "Ambagamuwa Udabulathgama"
            ],
            [
              "id"=> 561,
              "name"=> "Ambatalawa"
            ],
            [
              "id"=> 562,
              "name"=> "Ambewela"
            ],
            [
              "id"=> 563,
              "name"=> "Bogawantalawa"
            ],
            [
              "id"=> 564,
              "name"=> "Bopattalawa"
            ],
            [
              "id"=> 565,
              "name"=> "Dagampitiya"
            ],
            [
              "id"=> 566,
              "name"=> "Dayagama Bazaar"
            ],
            [
              "id"=> 567,
              "name"=> "Dikoya"
            ],
            [
              "id"=> 568,
              "name"=> "Doragala"
            ],
            [
              "id"=> 569,
              "name"=> "Dunukedeniya"
            ],
            [
              "id"=> 570,
              "name"=> "Ginigathena"
            ],
            [
              "id"=> 571,
              "name"=> "Gonakele"
            ],
            [
              "id"=> 572,
              "name"=> "Haggala"
            ],
            [
              "id"=> 573,
              "name"=> "Halgranoya"
            ],
            [
              "id"=> 574,
              "name"=> "Hangarapitiya"
            ],
            [
              "id"=> 575,
              "name"=> "Hapugastalawa"
            ],
            [
              "id"=> 576,
              "name"=> "Harangalagama"
            ],
            [
              "id"=> 577,
              "name"=> "Harasbedda"
            ],
            [
              "id"=> 578,
              "name"=> "Hatton"
            ],
            [
              "id"=> 579,
              "name"=> "Hedunuwewa"
            ],
            [
              "id"=> 580,
              "name"=> "Hitigegama"
            ],
            [
              "id"=> 581,
              "name"=> "Kalaganwatta"
            ],
            [
              "id"=> 582,
              "name"=> "Kandapola"
            ],
            [
              "id"=> 583,
              "name"=> "Katukitula"
            ],
            [
              "id"=> 584,
              "name"=> "Keerthi Bandarapura"
            ],
            [
              "id"=> 585,
              "name"=> "Kelanigama"
            ],
            [
              "id"=> 586,
              "name"=> "Ketaboola"
            ],
            [
              "id"=> 587,
              "name"=> "Kotagala"
            ],
            [
              "id"=> 588,
              "name"=> "Kotmale"
            ],
            [
              "id"=> 589,
              "name"=> "Kottellena"
            ],
            [
              "id"=> 590,
              "name"=> "Kumbalgamuwa"
            ],
            [
              "id"=> 591,
              "name"=> "Kumbukwela"
            ],
            [
              "id"=> 592,
              "name"=> "Kurupanawela"
            ],
            [
              "id"=> 593,
              "name"=> "Labukele"
            ],
            [
              "id"=> 594,
              "name"=> "Laxapana"
            ],
            [
              "id"=> 595,
              "name"=> "Lindula"
            ],
            [
              "id"=> 596,
              "name"=> "Madulla"
            ],
            [
              "id"=> 597,
              "name"=> "Maldeniya"
            ],
            [
              "id"=> 598,
              "name"=> "Maskeliya"
            ],
            [
              "id"=> 599,
              "name"=> "Maswela"
            ],
            [
              "id"=> 600,
              "name"=> "Mipanawa"
            ],
            [
              "id"=> 601,
              "name"=> "Mipilimana"
            ],
            [
              "id"=> 602,
              "name"=> "Morahenagama"
            ],
            [
              "id"=> 603,
              "name"=> "Munwatta"
            ],
            [
              "id"=> 604,
              "name"=> "Nanuoya"
            ],
            [
              "id"=> 605,
              "name"=> "Nawathispane"
            ],
            [
              "id"=> 606,
              "name"=> "Nayapana Janapadaya"
            ],
            [
              "id"=> 607,
              "name"=> "Nildandahinna"
            ],
            [
              "id"=> 608,
              "name"=> "Nissanka Uyana"
            ],
            [
              "id"=> 609,
              "name"=> "Norwood"
            ],
            [
              "id"=> 610,
              "name"=> "Nuwara Eliya"
            ],
            [
              "id"=> 611,
              "name"=> "Padiyapelella"
            ],
            [
              "id"=> 612,
              "name"=> "Patana"
            ],
            [
              "id"=> 613,
              "name"=> "Pattipola"
            ],
            [
              "id"=> 614,
              "name"=> "Pitawala"
            ],
            [
              "id"=> 615,
              "name"=> "Pundaluoya"
            ],
            [
              "id"=> 616,
              "name"=> "Ramboda"
            ],
            [
              "id"=> 617,
              "name"=> "Rozella"
            ],
            [
              "id"=> 618,
              "name"=> "Rupaha"
            ],
            [
              "id"=> 619,
              "name"=> "Ruwaneliya"
            ],
            [
              "id"=> 620,
              "name"=> "Santhipura"
            ],
            [
              "id"=> 621,
              "name"=> "Talawakele"
            ],
            [
              "id"=> 622,
              "name"=> "Teripeha"
            ],
            [
              "id"=> 623,
              "name"=> "Udamadura"
            ],
            [
              "id"=> 624,
              "name"=> "Udapussallawa"
            ],
            [
              "id"=> 625,
              "name"=> "Walapane"
            ],
            [
              "id"=> 626,
              "name"=> "Watagoda"
            ],
            [
              "id"=> 627,
              "name"=> "Watawala"
            ],
            [
              "id"=> 628,
              "name"=> "Widulipura"
            ],
            [
              "id"=> 629,
              "name"=> "Wijebahukanda"
            ],
            [
              "id"=> 630,
              "name"=> "Ampilanthurai"
            ],
            [
              "id"=> 631,
              "name"=> "Araipattai"
            ],
            [
              "id"=> 632,
              "name"=> "Ayithiyamalai"
            ],
            [
              "id"=> 633,
              "name"=> "Bakiella"
            ],
            [
              "id"=> 634,
              "name"=> "Batticaloa"
            ],
            [
              "id"=> 635,
              "name"=> "Cheddipalayam"
            ],
            [
              "id"=> 636,
              "name"=> "Chenkaladi"
            ],
            [
              "id"=> 637,
              "name"=> "Eravur"
            ],
            [
              "id"=> 638,
              "name"=> "Kalkudah"
            ],
            [
              "id"=> 639,
              "name"=> "Kallar"
            ],
            [
              "id"=> 640,
              "name"=> "Kaluwanchikudi"
            ],
            [
              "id"=> 641,
              "name"=> "Kaluwankemy"
            ],
            [
              "id"=> 642,
              "name"=> "Kannankudah"
            ],
            [
              "id"=> 643,
              "name"=> "Karadiyanaru"
            ],
            [
              "id"=> 644,
              "name"=> "Kathiraveli"
            ],
            [
              "id"=> 645,
              "name"=> "Kattankudi"
            ],
            [
              "id"=> 646,
              "name"=> "Kiran"
            ],
            [
              "id"=> 647,
              "name"=> "Kirankulam"
            ],
            [
              "id"=> 648,
              "name"=> "Koddaikallar"
            ],
            [
              "id"=> 649,
              "name"=> "Kokkaddichcholai"
            ],
            [
              "id"=> 650,
              "name"=> "Kurukkalmadam"
            ],
            [
              "id"=> 651,
              "name"=> "Mandur"
            ],
            [
              "id"=> 652,
              "name"=> "Mankemi"
            ],
            [
              "id"=> 653,
              "name"=> "Miravodai"
            ],
            [
              "id"=> 654,
              "name"=> "Murakottanchanai"
            ],
            [
              "id"=> 655,
              "name"=> "Navagirinagar"
            ],
            [
              "id"=> 656,
              "name"=> "Navatkadu"
            ],
            [
              "id"=> 657,
              "name"=> "Oddamavadi"
            ],
            [
              "id"=> 658,
              "name"=> "Panichankemi"
            ],
            [
              "id"=> 659,
              "name"=> "Pankudavely"
            ],
            [
              "id"=> 660,
              "name"=> "Periyaporativu"
            ],
            [
              "id"=> 661,
              "name"=> "Periyapullumalai"
            ],
            [
              "id"=> 662,
              "name"=> "Pillaiyaradi"
            ],
            [
              "id"=> 663,
              "name"=> "Punanai"
            ],
            [
              "id"=> 664,
              "name"=> "Puthukudiyiruppu"
            ],
            [
              "id"=> 665,
              "name"=> "Thannamunai"
            ],
            [
              "id"=> 666,
              "name"=> "Thettativu"
            ],
            [
              "id"=> 667,
              "name"=> "Thikkodai"
            ],
            [
              "id"=> 668,
              "name"=> "Thirupalugamam"
            ],
            [
              "id"=> 669,
              "name"=> "Thuraineelavanai"
            ],
            [
              "id"=> 670,
              "name"=> "Unnichchai"
            ],
            [
              "id"=> 671,
              "name"=> "Vakaneri"
            ],
            [
              "id"=> 672,
              "name"=> "Vakarai"
            ],
            [
              "id"=> 673,
              "name"=> "Valaichenai"
            ],
            [
              "id"=> 674,
              "name"=> "Vantharumoolai"
            ],
            [
              "id"=> 675,
              "name"=> "Vellavely"
            ],
            [
              "id"=> 676,
              "name"=> "Agbopura"
            ],
            [
              "id"=> 677,
              "name"=> "Buckmigama"
            ],
            [
              "id"=> 678,
              "name"=> "Chinabay"
            ],
            [
              "id"=> 679,
              "name"=> "Dehiwatte"
            ],
            [
              "id"=> 680,
              "name"=> "Echchilampattai"
            ],
            [
              "id"=> 681,
              "name"=> "Galmetiyawa"
            ],
            [
              "id"=> 682,
              "name"=> "Gomarankadawala"
            ],
            [
              "id"=> 683,
              "name"=> "Kaddaiparichchan"
            ],
            [
              "id"=> 684,
              "name"=> "Kanniya"
            ],
            [
              "id"=> 685,
              "name"=> "Kantalai"
            ],
            [
              "id"=> 686,
              "name"=> "Kantalai Sugar Factory"
            ],
            [
              "id"=> 687,
              "name"=> "Kiliveddy"
            ],
            [
              "id"=> 688,
              "name"=> "Kinniya"
            ],
            [
              "id"=> 689,
              "name"=> "Kuchchaveli"
            ],
            [
              "id"=> 690,
              "name"=> "Kumburupiddy"
            ],
            [
              "id"=> 691,
              "name"=> "Kurinchakemy"
            ],
            [
              "id"=> 692,
              "name"=> "Lankapatuna"
            ],
            [
              "id"=> 693,
              "name"=> "Mahadivulwewa"
            ],
            [
              "id"=> 694,
              "name"=> "Maharugiramam"
            ],
            [
              "id"=> 695,
              "name"=> "Mallikativu"
            ],
            [
              "id"=> 696,
              "name"=> "Mawadichenai"
            ],
            [
              "id"=> 697,
              "name"=> "Mullipothana"
            ],
            [
              "id"=> 698,
              "name"=> "Mutur"
            ],
            [
              "id"=> 699,
              "name"=> "Neelapola"
            ],
            [
              "id"=> 700,
              "name"=> "Nilaveli"
            ],
            [
              "id"=> 701,
              "name"=> "Pankulam"
            ],
            [
              "id"=> 702,
              "name"=> "Rottawewa"
            ],
            [
              "id"=> 703,
              "name"=> "Sampaltivu"
            ],
            [
              "id"=> 704,
              "name"=> "Sampur"
            ],
            [
              "id"=> 705,
              "name"=> "Serunuwara"
            ],
            [
              "id"=> 706,
              "name"=> "Seruwila"
            ],
            [
              "id"=> 707,
              "name"=> "Sirajnagar"
            ],
            [
              "id"=> 708,
              "name"=> "Somapura"
            ],
            [
              "id"=> 709,
              "name"=> "Tampalakamam"
            ],
            [
              "id"=> 710,
              "name"=> "Tiriyayi"
            ],
            [
              "id"=> 711,
              "name"=> "Toppur"
            ],
            [
              "id"=> 712,
              "name"=> "Trincomalee"
            ],
            [
              "id"=> 713,
              "name"=> "Vellamanal"
            ],
            [
              "id"=> 714,
              "name"=> "Wanela"
            ],
            [
              "id"=> 715,
              "name"=> "Addalaichenai"
            ],
            [
              "id"=> 716,
              "name"=> "Akkaraipattu"
            ],
            [
              "id"=> 717,
              "name"=> "Ampara"
            ],
            [
              "id"=> 718,
              "name"=> "Bakmitiyawa"
            ],
            [
              "id"=> 719,
              "name"=> "Central Camp"
            ],
            [
              "id"=> 720,
              "name"=> "Dadayamtalawa"
            ],
            [
              "id"=> 721,
              "name"=> "Damana"
            ],
            [
              "id"=> 722,
              "name"=> "Damanewela"
            ],
            [
              "id"=> 723,
              "name"=> "Deegawapiya"
            ],
            [
              "id"=> 724,
              "name"=> "Dehiattakandiya"
            ],
            [
              "id"=> 725,
              "name"=> "Devalahinda"
            ],
            [
              "id"=> 726,
              "name"=> "Digamadulla Weeragoda"
            ],
            [
              "id"=> 727,
              "name"=> "Dorakumbura"
            ],
            [
              "id"=> 728,
              "name"=> "Galapitagala"
            ],
            [
              "id"=> 729,
              "name"=> "Gonagolla"
            ],
            [
              "id"=> 730,
              "name"=> "Hingurana"
            ],
            [
              "id"=> 731,
              "name"=> "Hulannuge"
            ],
            [
              "id"=> 732,
              "name"=> "Kalmunai"
            ],
            [
              "id"=> 733,
              "name"=> "Kannakipuram"
            ],
            [
              "id"=> 734,
              "name"=> "Karativu"
            ],
            [
              "id"=> 735,
              "name"=> "Kekirihena"
            ],
            [
              "id"=> 736,
              "name"=> "Koknahara"
            ],
            [
              "id"=> 737,
              "name"=> "Kolamanthalawa"
            ],
            [
              "id"=> 738,
              "name"=> "Komari"
            ],
            [
              "id"=> 739,
              "name"=> "Lahugala"
            ],
            [
              "id"=> 740,
              "name"=> "lmkkamam"
            ],
            [
              "id"=> 741,
              "name"=> "Madawalalanda"
            ],
            [
              "id"=> 742,
              "name"=> "Mahanagapura"
            ],
            [
              "id"=> 743,
              "name"=> "Mahaoya"
            ],
            [
              "id"=> 744,
              "name"=> "Malwatta"
            ],
            [
              "id"=> 745,
              "name"=> "Mangalagama"
            ],
            [
              "id"=> 746,
              "name"=> "Marathamune"
            ],
            [
              "id"=> 747,
              "name"=> "Mawanagama"
            ],
            [
              "id"=> 748,
              "name"=> "Moragahapallama"
            ],
            [
              "id"=> 749,
              "name"=> "Namaloya"
            ],
            [
              "id"=> 750,
              "name"=> "Navithanveli"
            ],
            [
              "id"=> 751,
              "name"=> "Nawamedagama"
            ],
            [
              "id"=> 752,
              "name"=> "Nintavur"
            ],
            [
              "id"=> 753,
              "name"=> "Oluvil"
            ],
            [
              "id"=> 754,
              "name"=> "Padiyatalawa"
            ],
            [
              "id"=> 755,
              "name"=> "Pahalalanda"
            ],
            [
              "id"=> 756,
              "name"=> "Palamunai"
            ],
            [
              "id"=> 757,
              "name"=> "Panama"
            ],
            [
              "id"=> 758,
              "name"=> "Pannalagama"
            ],
            [
              "id"=> 759,
              "name"=> "Paragahakele"
            ],
            [
              "id"=> 760,
              "name"=> "Periyaneelavanai"
            ],
            [
              "id"=> 761,
              "name"=> "Polwaga Janapadaya"
            ],
            [
              "id"=> 762,
              "name"=> "Pottuvil"
            ],
            [
              "id"=> 763,
              "name"=> "Rajagalatenna"
            ],
            [
              "id"=> 764,
              "name"=> "Sainthamaruthu"
            ],
            [
              "id"=> 765,
              "name"=> "Samanthurai"
            ],
            [
              "id"=> 766,
              "name"=> "Serankada"
            ],
            [
              "id"=> 767,
              "name"=> "Siripura"
            ],
            [
              "id"=> 768,
              "name"=> "Siyambalawewa"
            ],
            [
              "id"=> 769,
              "name"=> "Tempitiya"
            ],
            [
              "id"=> 770,
              "name"=> "Thambiluvil"
            ],
            [
              "id"=> 771,
              "name"=> "Tirukovil"
            ],
            [
              "id"=> 772,
              "name"=> "Uhana"
            ],
            [
              "id"=> 773,
              "name"=> "Wadinagala"
            ],
            [
              "id"=> 774,
              "name"=> "Wanagamuwa"
            ],
            [
              "id"=> 775,
              "name"=> "Werunketagoda"
            ],
            [
              "id"=> 776,
              "name"=> "Allaipiddi"
            ],
            [
              "id"=> 777,
              "name"=> "Allaveddi"
            ],
            [
              "id"=> 778,
              "name"=> "Alvai"
            ],
            [
              "id"=> 779,
              "name"=> "Anaicoddai"
            ],
            [
              "id"=> 780,
              "name"=> "Analaitivu"
            ],
            [
              "id"=> 781,
              "name"=> "Atchuveli"
            ],
            [
              "id"=> 782,
              "name"=> "Chankanai"
            ],
            [
              "id"=> 783,
              "name"=> "Chavakachcheri"
            ],
            [
              "id"=> 784,
              "name"=> "Chullipuram"
            ],
            [
              "id"=> 785,
              "name"=> "Chundikuli"
            ],
            [
              "id"=> 786,
              "name"=> "Chunnakam"
            ],
            [
              "id"=> 787,
              "name"=> "Delft"
            ],
            [
              "id"=> 788,
              "name"=> "DelftWest"
            ],
            [
              "id"=> 789,
              "name"=> "Eluvaitivu"
            ],
            [
              "id"=> 790,
              "name"=> "Erlalai"
            ],
            [
              "id"=> 791,
              "name"=> "Jaffna"
            ],
            [
              "id"=> 792,
              "name"=> "Kaitadi"
            ],
            [
              "id"=> 793,
              "name"=> "Kankesanthurai"
            ],
            [
              "id"=> 794,
              "name"=> "Karainagar"
            ],
            [
              "id"=> 795,
              "name"=> "Karaveddi"
            ],
            [
              "id"=> 796,
              "name"=> "Kayts"
            ],
            [
              "id"=> 797,
              "name"=> "Kodikamam"
            ],
            [
              "id"=> 798,
              "name"=> "Kokuvil"
            ],
            [
              "id"=> 799,
              "name"=> "Kondavil"
            ],
            [
              "id"=> 800,
              "name"=> "Kopay"
            ],
            [
              "id"=> 801,
              "name"=> "Kudatanai"
            ],
            [
              "id"=> 802,
              "name"=> "llavalai"
            ],
            [
              "id"=> 803,
              "name"=> "Mallakam"
            ],
            [
              "id"=> 804,
              "name"=> "Manipay"
            ],
            [
              "id"=> 805,
              "name"=> "Mathagal"
            ],
            [
              "id"=> 806,
              "name"=> "Meesalai"
            ],
            [
              "id"=> 807,
              "name"=> "Mirusuvil"
            ],
            [
              "id"=> 808,
              "name"=> "Nagar Kovil"
            ],
            [
              "id"=> 809,
              "name"=> "Nainathivu"
            ],
            [
              "id"=> 810,
              "name"=> "Neervely"
            ],
            [
              "id"=> 811,
              "name"=> "Pandaterippu"
            ],
            [
              "id"=> 812,
              "name"=> "Point Pedro"
            ],
            [
              "id"=> 813,
              "name"=> "Pungudutivu"
            ],
            [
              "id"=> 814,
              "name"=> "Putur"
            ],
            [
              "id"=> 815,
              "name"=> "Sandilipay"
            ],
            [
              "id"=> 816,
              "name"=> "Sithankemy"
            ],
            [
              "id"=> 817,
              "name"=> "Tellippallai"
            ],
            [
              "id"=> 818,
              "name"=> "Thondamanaru"
            ],
            [
              "id"=> 819,
              "name"=> "Urumpirai"
            ],
            [
              "id"=> 820,
              "name"=> "Vaddukoddai"
            ],
            [
              "id"=> 821,
              "name"=> "Valvettithurai"
            ],
            [
              "id"=> 822,
              "name"=> "Vannarponnai"
            ],
            [
              "id"=> 823,
              "name"=> "Varany"
            ],
            [
              "id"=> 824,
              "name"=> "Vasavilan"
            ],
            [
              "id"=> 825,
              "name"=> "Velanai"
            ],
            [
              "id"=> 853,
              "name"=> "Alampil"
            ],
            [
              "id"=> 827,
              "name"=> "Adampan"
            ],
            [
              "id"=> 828,
              "name"=> "Arippu"
            ],
            [
              "id"=> 829,
              "name"=> "Athimottai"
            ],
            [
              "id"=> 830,
              "name"=> "Chilavathurai"
            ],
            [
              "id"=> 831,
              "name"=> "Erukkalampiddy"
            ],
            [
              "id"=> 832,
              "name"=> "llluppaikadavai"
            ],
            [
              "id"=> 833,
              "name"=> "Madhu Church"
            ],
            [
              "id"=> 834,
              "name"=> "Madhu Road"
            ],
            [
              "id"=> 835,
              "name"=> "Mannar"
            ],
            [
              "id"=> 836,
              "name"=> "Marichchi Kaddi"
            ],
            [
              "id"=> 837,
              "name"=> "Murunkan"
            ],
            [
              "id"=> 838,
              "name"=> "Nanattan"
            ],
            [
              "id"=> 839,
              "name"=> "P.P.Potkemy"
            ],
            [
              "id"=> 840,
              "name"=> "Palampiddy"
            ],
            [
              "id"=> 841,
              "name"=> "Periyakunchikulam"
            ],
            [
              "id"=> 842,
              "name"=> "Periyamadhu"
            ],
            [
              "id"=> 843,
              "name"=> "Pesalai"
            ],
            [
              "id"=> 844,
              "name"=> "Talaimannar"
            ],
            [
              "id"=> 845,
              "name"=> "Temple"
            ],
            [
              "id"=> 846,
              "name"=> "Tharapuram"
            ],
            [
              "id"=> 847,
              "name"=> "Thiruketheeswaram Temple"
            ],
            [
              "id"=> 848,
              "name"=> "Uyilankulam"
            ],
            [
              "id"=> 849,
              "name"=> "Vaddakandal"
            ],
            [
              "id"=> 850,
              "name"=> "Vankalai"
            ],
            [
              "id"=> 851,
              "name"=> "Vellan Kulam"
            ],
            [
              "id"=> 852,
              "name"=> "Vidataltivu"
            ],
            [
              "id"=> 854,
              "name"=> "Karuppaddamurippu"
            ],
            [
              "id"=> 855,
              "name"=> "Kokkilai"
            ],
            [
              "id"=> 856,
              "name"=> "Kokkuthuoduvai"
            ],
            [
              "id"=> 857,
              "name"=> "Mankulam"
            ],
            [
              "id"=> 858,
              "name"=> "Mullativu"
            ],
            [
              "id"=> 859,
              "name"=> "Mullivaikkal"
            ],
            [
              "id"=> 860,
              "name"=> "Mulliyawalai"
            ],
            [
              "id"=> 861,
              "name"=> "Muthauyan Kaddakulam"
            ],
            [
              "id"=> 862,
              "name"=> "Naddan Kandal"
            ],
            [
              "id"=> 863,
              "name"=> "Odduchudan"
            ],
            [
              "id"=> 864,
              "name"=> "Puthuvedduvan"
            ],
            [
              "id"=> 865,
              "name"=> "Thunukkai"
            ],
            [
              "id"=> 866,
              "name"=> "Udayarkaddu"
            ],
            [
              "id"=> 867,
              "name"=> "Vavunakkulam"
            ],
            [
              "id"=> 868,
              "name"=> "Visvamadukulam"
            ],
            [
              "id"=> 869,
              "name"=> "Yogapuram"
            ],
            [
              "id"=> 870,
              "name"=> "Akkarayankulam"
            ],
            [
              "id"=> 871,
              "name"=> "Cheddikulam"
            ],
            [
              "id"=> 872,
              "name"=> "Chemamadukkulam"
            ],
            [
              "id"=> 873,
              "name"=> "Elephant Pass"
            ],
            [
              "id"=> 874,
              "name"=> "Eluthumadduval"
            ],
            [
              "id"=> 875,
              "name"=> "Iranai lluppaikulam"
            ],
            [
              "id"=> 876,
              "name"=> "Iranaitiv"
            ],
            [
              "id"=> 877,
              "name"=> "Kanagarayankulam"
            ],
            [
              "id"=> 878,
              "name"=> "Kanavil"
            ],
            [
              "id"=> 879,
              "name"=> "Kandavalai"
            ],
            [
              "id"=> 880,
              "name"=> "Kavutharimunai"
            ],
            [
              "id"=> 881,
              "name"=> "Kilinochchi"
            ],
            [
              "id"=> 882,
              "name"=> "lyakachchi"
            ],
            [
              "id"=> 883,
              "name"=> "Mahakachchakodiya"
            ],
            [
              "id"=> 884,
              "name"=> "Mamaduwa"
            ],
            [
              "id"=> 885,
              "name"=> "Maraiyadithakulam"
            ],
            [
              "id"=> 886,
              "name"=> "Mulliyan"
            ],
            [
              "id"=> 887,
              "name"=> "Murasumoddai"
            ],
            [
              "id"=> 888,
              "name"=> "Nedunkemy"
            ],
            [
              "id"=> 889,
              "name"=> "Neriyakulam"
            ],
            [
              "id"=> 890,
              "name"=> "Omanthai"
            ],
            [
              "id"=> 891,
              "name"=> "Palamoddai"
            ],
            [
              "id"=> 892,
              "name"=> "Pallai"
            ],
            [
              "id"=> 893,
              "name"=> "Pallavarayankaddu"
            ],
            [
              "id"=> 894,
              "name"=> "Pampaimadu"
            ],
            [
              "id"=> 895,
              "name"=> "Paranthan"
            ],
            [
              "id"=> 896,
              "name"=> "Pavaikulam"
            ],
            [
              "id"=> 897,
              "name"=> "Periyathambanai"
            ],
            [
              "id"=> 898,
              "name"=> "Periyaulukkulam"
            ],
            [
              "id"=> 899,
              "name"=> "Purakari Nallur"
            ],
            [
              "id"=> 900,
              "name"=> "Ramanathapuram"
            ],
            [
              "id"=> 901,
              "name"=> "Sasthrikulankulam"
            ],
            [
              "id"=> 902,
              "name"=> "Sivapuram"
            ],
            [
              "id"=> 903,
              "name"=> "Skanthapuram"
            ],
            [
              "id"=> 904,
              "name"=> "Thalayadi"
            ],
            [
              "id"=> 905,
              "name"=> "Tharmapuram"
            ],
            [
              "id"=> 906,
              "name"=> "Uruthirapuram"
            ],
            [
              "id"=> 907,
              "name"=> "Vaddakachchi"
            ],
            [
              "id"=> 908,
              "name"=> "Vannerikkulam"
            ],
            [
              "id"=> 909,
              "name"=> "Varikkuttiyoor"
            ],
            [
              "id"=> 910,
              "name"=> "Vavuniya"
            ],
            [
              "id"=> 911,
              "name"=> "Veravil"
            ],
            [
              "id"=> 912,
              "name"=> "Vinayagapuram"
            ],
            [
              "id"=> 989,
              "name"=> "Kahatagasdigiliya"
            ],
            [
              "id"=> 988,
              "name"=> "Kagama"
            ],
            [
              "id"=> 987,
              "name"=> "Hurulunikawewa"
            ],
            [
              "id"=> 986,
              "name"=> "Hurigaswewa"
            ],
            [
              "id"=> 985,
              "name"=> "Horiwila"
            ],
            [
              "id"=> 984,
              "name"=> "Horawpatana"
            ],
            [
              "id"=> 983,
              "name"=> "Hidogama"
            ],
            [
              "id"=> 982,
              "name"=> "Halmillawetiya"
            ],
            [
              "id"=> 981,
              "name"=> "Halmillawa Dambulla"
            ],
            [
              "id"=> 980,
              "name"=> "Habarana"
            ],
            [
              "id"=> 979,
              "name"=> "Gonahaddenawa"
            ],
            [
              "id"=> 978,
              "name"=> "Gnanikulama"
            ],
            [
              "id"=> 977,
              "name"=> "Getalawa"
            ],
            [
              "id"=> 976,
              "name"=> "Gemunupura"
            ],
            [
              "id"=> 975,
              "name"=> "Ganewalpola"
            ],
            [
              "id"=> 974,
              "name"=> "Gambirigaswewa"
            ],
            [
              "id"=> 973,
              "name"=> "Galnewa"
            ],
            [
              "id"=> 972,
              "name"=> "Galkulama"
            ],
            [
              "id"=> 971,
              "name"=> "Galkiriyagama"
            ],
            [
              "id"=> 970,
              "name"=> "Galkadawala"
            ],
            [
              "id"=> 969,
              "name"=> "Galenbindunuwewa"
            ],
            [
              "id"=> 968,
              "name"=> "Galadivulwewa"
            ],
            [
              "id"=> 967,
              "name"=> "Etaweeragollewa"
            ],
            [
              "id"=> 966,
              "name"=> "Etawatunuwewa"
            ],
            [
              "id"=> 965,
              "name"=> "Eppawala"
            ],
            [
              "id"=> 964,
              "name"=> "Elayapattuwa"
            ],
            [
              "id"=> 963,
              "name"=> "Dutuwewa"
            ],
            [
              "id"=> 962,
              "name"=> "Dunumadalawa"
            ],
            [
              "id"=> 961,
              "name"=> "Dematawewa"
            ],
            [
              "id"=> 960,
              "name"=> "Bogahawewa"
            ],
            [
              "id"=> 959,
              "name"=> "Awukana"
            ],
            [
              "id"=> 958,
              "name"=> "Anuradhapura"
            ],
            [
              "id"=> 957,
              "name"=> "Angamuwa"
            ],
            [
              "id"=> 956,
              "name"=> "Andiyagala"
            ],
            [
              "id"=> 990,
              "name"=> "Kahatagollewa"
            ],
            [
              "id"=> 991,
              "name"=> "Kalakarambewa"
            ],
            [
              "id"=> 992,
              "name"=> "Kalankuttiya"
            ],
            [
              "id"=> 993,
              "name"=> "Kalaoya"
            ],
            [
              "id"=> 994,
              "name"=> "Kalawedi Ulpotha"
            ],
            [
              "id"=> 995,
              "name"=> "Kallanchiya"
            ],
            [
              "id"=> 996,
              "name"=> "Kapugallawa"
            ],
            [
              "id"=> 997,
              "name"=> "Karagahawewa"
            ],
            [
              "id"=> 998,
              "name"=> "Katiyawa"
            ],
            [
              "id"=> 999,
              "name"=> "Kebithigollewa"
            ],
            [
              "id"=> 1000,
              "name"=> "Kekirawa"
            ],
            [
              "id"=> 1001,
              "name"=> "Kendewa"
            ],
            [
              "id"=> 1002,
              "name"=> "Kiralogama"
            ],
            [
              "id"=> 1003,
              "name"=> "Kirigalwewa"
            ],
            [
              "id"=> 1004,
              "name"=> "Kitulhitiyawa"
            ],
            [
              "id"=> 1005,
              "name"=> "Kurundankulama"
            ],
            [
              "id"=> 1006,
              "name"=> "Labunoruwa"
            ],
            [
              "id"=> 1007,
              "name"=> "lhala Halmillewa"
            ],
            [
              "id"=> 1008,
              "name"=> "lhalagama"
            ],
            [
              "id"=> 1009,
              "name"=> "lpologama"
            ],
            [
              "id"=> 1010,
              "name"=> "Madatugama"
            ],
            [
              "id"=> 1011,
              "name"=> "Maha Elagamuwa"
            ],
            [
              "id"=> 1012,
              "name"=> "Mahabulankulama"
            ],
            [
              "id"=> 1013,
              "name"=> "Mahailluppallama"
            ],
            [
              "id"=> 1014,
              "name"=> "Mahakanadarawa"
            ],
            [
              "id"=> 1015,
              "name"=> "Mahapothana"
            ],
            [
              "id"=> 1016,
              "name"=> "Mahasenpura"
            ],
            [
              "id"=> 1017,
              "name"=> "Mahawilachchiya"
            ],
            [
              "id"=> 1018,
              "name"=> "Mailagaswewa"
            ],
            [
              "id"=> 1019,
              "name"=> "Malwanagama"
            ],
            [
              "id"=> 1020,
              "name"=> "Maneruwa"
            ],
            [
              "id"=> 1021,
              "name"=> "Maradankadawala"
            ],
            [
              "id"=> 1022,
              "name"=> "Maradankalla"
            ],
            [
              "id"=> 1023,
              "name"=> "Medawachchiya"
            ],
            [
              "id"=> 1024,
              "name"=> "Megodawewa"
            ],
            [
              "id"=> 1025,
              "name"=> "Mihintale"
            ],
            [
              "id"=> 1026,
              "name"=> "Morakewa"
            ],
            [
              "id"=> 1027,
              "name"=> "Mulkiriyawa"
            ],
            [
              "id"=> 1028,
              "name"=> "Muriyakadawala"
            ],
            [
              "id"=> 1029,
              "name"=> "Nachchaduwa"
            ],
            [
              "id"=> 1030,
              "name"=> "Namalpura"
            ],
            [
              "id"=> 1031,
              "name"=> "Negampaha"
            ],
            [
              "id"=> 1032,
              "name"=> "Nochchiyagama"
            ],
            [
              "id"=> 1033,
              "name"=> "Padavi Maithripura"
            ],
            [
              "id"=> 1034,
              "name"=> "Padavi Parakramapura"
            ],
            [
              "id"=> 1035,
              "name"=> "Padavi Sripura"
            ],
            [
              "id"=> 1036,
              "name"=> "Padavi Sritissapura"
            ],
            [
              "id"=> 1037,
              "name"=> "Padaviya"
            ],
            [
              "id"=> 1038,
              "name"=> "Padikaramaduwa"
            ],
            [
              "id"=> 1039,
              "name"=> "Pahala Halmillewa"
            ],
            [
              "id"=> 1040,
              "name"=> "Pahala Maragahawe"
            ],
            [
              "id"=> 1041,
              "name"=> "Pahalagama"
            ],
            [
              "id"=> 1042,
              "name"=> "Palagala"
            ],
            [
              "id"=> 1043,
              "name"=> "Palugaswewa"
            ],
            [
              "id"=> 1044,
              "name"=> "Pandukabayapura"
            ],
            [
              "id"=> 1045,
              "name"=> "Pandulagama"
            ],
            [
              "id"=> 1046,
              "name"=> "Parakumpura"
            ],
            [
              "id"=> 1047,
              "name"=> "Parangiyawadiya"
            ],
            [
              "id"=> 1048,
              "name"=> "Parasangahawewa"
            ],
            [
              "id"=> 1049,
              "name"=> "Pemaduwa"
            ],
            [
              "id"=> 1050,
              "name"=> "Perimiyankulama"
            ],
            [
              "id"=> 1051,
              "name"=> "Pihimbiyagolewa"
            ],
            [
              "id"=> 1052,
              "name"=> "Pubbogama"
            ],
            [
              "id"=> 1053,
              "name"=> "Puliyankulama"
            ],
            [
              "id"=> 1054,
              "name"=> "Pulmoddai"
            ],
            [
              "id"=> 1055,
              "name"=> "Punewa"
            ],
            [
              "id"=> 1056,
              "name"=> "Rajanganaya"
            ],
            [
              "id"=> 1057,
              "name"=> "Rambewa"
            ],
            [
              "id"=> 1058,
              "name"=> "Rampathwila"
            ],
            [
              "id"=> 1059,
              "name"=> "Ranorawa"
            ],
            [
              "id"=> 1060,
              "name"=> "Rathmalgahawewa"
            ],
            [
              "id"=> 1061,
              "name"=> "Saliyapura"
            ],
            [
              "id"=> 1062,
              "name"=> "Seeppukulama"
            ],
            [
              "id"=> 1063,
              "name"=> "Senapura"
            ],
            [
              "id"=> 1064,
              "name"=> "Sivalakulama"
            ],
            [
              "id"=> 1065,
              "name"=> "Siyambalewa"
            ],
            [
              "id"=> 1066,
              "name"=> "Sravasthipura"
            ],
            [
              "id"=> 1067,
              "name"=> "Talawa"
            ],
            [
              "id"=> 1068,
              "name"=> "Tambuttegama"
            ],
            [
              "id"=> 1069,
              "name"=> "Tammennawa"
            ],
            [
              "id"=> 1070,
              "name"=> "Tantirimale"
            ],
            [
              "id"=> 1071,
              "name"=> "Telhiriyawa"
            ],
            [
              "id"=> 1072,
              "name"=> "Tennamarawadiya"
            ],
            [
              "id"=> 1073,
              "name"=> "Tirappane"
            ],
            [
              "id"=> 1074,
              "name"=> "Tittagonewa"
            ],
            [
              "id"=> 1075,
              "name"=> "Udunuwara Colony"
            ],
            [
              "id"=> 1076,
              "name"=> "Upuldeniya"
            ],
            [
              "id"=> 1077,
              "name"=> "Uttimaduwa"
            ],
            [
              "id"=> 1078,
              "name"=> "Viharapalugama"
            ],
            [
              "id"=> 1079,
              "name"=> "Vijithapura"
            ],
            [
              "id"=> 1080,
              "name"=> "Wahalkada"
            ],
            [
              "id"=> 1081,
              "name"=> "Wahamalgollewa"
            ],
            [
              "id"=> 1082,
              "name"=> "Walagambahuwa"
            ],
            [
              "id"=> 1083,
              "name"=> "Walahaviddawewa"
            ],
            [
              "id"=> 1084,
              "name"=> "Welimuwapotana"
            ],
            [
              "id"=> 1085,
              "name"=> "Welioya Project"
            ],
            [
              "id"=> 1086,
              "name"=> "Alutwewa"
            ],
            [
              "id"=> 1087,
              "name"=> "Aralaganwila"
            ],
            [
              "id"=> 1088,
              "name"=> "Aselapura"
            ],
            [
              "id"=> 1089,
              "name"=> "Attanakadawala"
            ],
            [
              "id"=> 1090,
              "name"=> "Bakamuna"
            ],
            [
              "id"=> 1091,
              "name"=> "Dalukana"
            ],
            [
              "id"=> 1092,
              "name"=> "Damminna"
            ],
            [
              "id"=> 1093,
              "name"=> "Dewagala"
            ],
            [
              "id"=> 1094,
              "name"=> "Dimbulagala"
            ],
            [
              "id"=> 1095,
              "name"=> "Divulankadawala"
            ],
            [
              "id"=> 1096,
              "name"=> "Divuldamana"
            ],
            [
              "id"=> 1097,
              "name"=> "Diyabeduma"
            ],
            [
              "id"=> 1098,
              "name"=> "Diyasenpura"
            ],
            [
              "id"=> 1099,
              "name"=> "Elahera"
            ],
            [
              "id"=> 1100,
              "name"=> "Ellewewa"
            ],
            [
              "id"=> 1101,
              "name"=> "Galamuna"
            ],
            [
              "id"=> 1102,
              "name"=> "Galoya Junction"
            ],
            [
              "id"=> 1103,
              "name"=> "Giritale"
            ],
            [
              "id"=> 1104,
              "name"=> "Hansayapalama"
            ],
            [
              "id"=> 1105,
              "name"=> "Hingurakdamana"
            ],
            [
              "id"=> 1106,
              "name"=> "Hingurakgoda"
            ],
            [
              "id"=> 1107,
              "name"=> "Jayanthipura"
            ],
            [
              "id"=> 1108,
              "name"=> "Jayasiripura"
            ],
            [
              "id"=> 1109,
              "name"=> "Kaduruwela"
            ],
            [
              "id"=> 1110,
              "name"=> "Kalingaela"
            ],
            [
              "id"=> 1111,
              "name"=> "Kalukele Badanagala"
            ],
            [
              "id"=> 1112,
              "name"=> "Kashyapapura"
            ],
            [
              "id"=> 1113,
              "name"=> "Kawudulla"
            ],
            [
              "id"=> 1114,
              "name"=> "Kawuduluwewa Stagell"
            ],
            [
              "id"=> 1115,
              "name"=> "Kottapitiya"
            ],
            [
              "id"=> 1116,
              "name"=> "Kumaragama"
            ],
            [
              "id"=> 1117,
              "name"=> "Lakshauyana"
            ],
            [
              "id"=> 1118,
              "name"=> "Maduruoya"
            ],
            [
              "id"=> 1119,
              "name"=> "Maha Ambagaswewa"
            ],
            [
              "id"=> 1120,
              "name"=> "Mahatalakolawewa"
            ],
            [
              "id"=> 1121,
              "name"=> "Mahawela Sinhapura"
            ],
            [
              "id"=> 1122,
              "name"=> "Mampitiya"
            ],
            [
              "id"=> 1123,
              "name"=> "Medirigiriya"
            ],
            [
              "id"=> 1124,
              "name"=> "Meegaswewa"
            ],
            [
              "id"=> 1125,
              "name"=> "Minneriya"
            ],
            [
              "id"=> 1126,
              "name"=> "Mutugala"
            ],
            [
              "id"=> 1127,
              "name"=> "Nawasenapura"
            ],
            [
              "id"=> 1128,
              "name"=> "Nelumwewa"
            ],
            [
              "id"=> 1129,
              "name"=> "Nuwaragala"
            ],
            [
              "id"=> 1130,
              "name"=> "Onegama"
            ],
            [
              "id"=> 1131,
              "name"=> "Orubendi Siyambalawa"
            ],
            [
              "id"=> 1132,
              "name"=> "Palugasdamana"
            ],
            [
              "id"=> 1133,
              "name"=> "Parakramasamudraya"
            ],
            [
              "id"=> 1134,
              "name"=> "Pelatiyawa"
            ],
            [
              "id"=> 1135,
              "name"=> "Pimburattewa"
            ],
            [
              "id"=> 1136,
              "name"=> "Polonnaruwa"
            ],
            [
              "id"=> 1137,
              "name"=> "Pulastigama"
            ],
            [
              "id"=> 1138,
              "name"=> "Sevanapitiya"
            ],
            [
              "id"=> 1139,
              "name"=> "Sinhagama"
            ],
            [
              "id"=> 1140,
              "name"=> "Sungavila"
            ],
            [
              "id"=> 1141,
              "name"=> "Talpotha"
            ],
            [
              "id"=> 1142,
              "name"=> "Tamankaduwa"
            ],
            [
              "id"=> 1143,
              "name"=> "Tambala"
            ],
            [
              "id"=> 1144,
              "name"=> "Unagalavehera"
            ],
            [
              "id"=> 1145,
              "name"=> "Welikanda"
            ],
            [
              "id"=> 1146,
              "name"=> "Wijayabapura"
            ],
            [
              "id"=> 1147,
              "name"=> "Yodaela"
            ],
            [
              "id"=> 1148,
              "name"=> "Alahengama"
            ],
            [
              "id"=> 1149,
              "name"=> "Alahitiyawa"
            ],
            [
              "id"=> 1150,
              "name"=> "Alawatuwala"
            ],
            [
              "id"=> 1151,
              "name"=> "Alawwa"
            ],
            [
              "id"=> 1152,
              "name"=> "Ambakote"
            ],
            [
              "id"=> 1153,
              "name"=> "Ambanpola"
            ],
            [
              "id"=> 1154,
              "name"=> "Anhandiya"
            ],
            [
              "id"=> 1155,
              "name"=> "Anukkane"
            ],
            [
              "id"=> 1156,
              "name"=> "Aragoda"
            ],
            [
              "id"=> 1157,
              "name"=> "Ataragalla"
            ],
            [
              "id"=> 1158,
              "name"=> "Awulegama"
            ],
            [
              "id"=> 1159,
              "name"=> "Balalla"
            ],
            [
              "id"=> 1160,
              "name"=> "Bamunukotuwa"
            ],
            [
              "id"=> 1161,
              "name"=> "Bandara Koswatta"
            ],
            [
              "id"=> 1162,
              "name"=> "Bingiriya"
            ],
            [
              "id"=> 1163,
              "name"=> "Bogamulla"
            ],
            [
              "id"=> 1164,
              "name"=> "Bopitiya"
            ],
            [
              "id"=> 1165,
              "name"=> "Boraluwewa"
            ],
            [
              "id"=> 1166,
              "name"=> "Boyagane"
            ],
            [
              "id"=> 1167,
              "name"=> "Bujjomuwa"
            ],
            [
              "id"=> 1168,
              "name"=> "Buluwala"
            ],
            [
              "id"=> 1169,
              "name"=> "Dambadeniya"
            ],
            [
              "id"=> 1170,
              "name"=> "Daraluwa"
            ],
            [
              "id"=> 1171,
              "name"=> "Deegalla"
            ],
            [
              "id"=> 1172,
              "name"=> "Delwite"
            ],
            [
              "id"=> 1173,
              "name"=> "Demataluwa"
            ],
            [
              "id"=> 1174,
              "name"=> "Diddeniya"
            ],
            [
              "id"=> 1175,
              "name"=> "Digannewa"
            ],
            [
              "id"=> 1176,
              "name"=> "Divullegoda"
            ],
            [
              "id"=> 1177,
              "name"=> "Dodangaslanda"
            ],
            [
              "id"=> 1178,
              "name"=> "Doratiyawa"
            ],
            [
              "id"=> 1179,
              "name"=> "Dummalasuriya"
            ],
            [
              "id"=> 1180,
              "name"=> "Ehetuwewa"
            ],
            [
              "id"=> 1181,
              "name"=> "Elibichchiya"
            ],
            [
              "id"=> 1182,
              "name"=> "Embogama"
            ],
            [
              "id"=> 1183,
              "name"=> "Etungahakotuwa"
            ],
            [
              "id"=> 1184,
              "name"=> "Galgamuwa"
            ],
            [
              "id"=> 1185,
              "name"=> "Gallewa"
            ],
            [
              "id"=> 1186,
              "name"=> "Girathalana"
            ],
            [
              "id"=> 1187,
              "name"=> "Giriulla"
            ],
            [
              "id"=> 1188,
              "name"=> "Gokaralla"
            ],
            [
              "id"=> 1189,
              "name"=> "Gonawila"
            ],
            [
              "id"=> 1190,
              "name"=> "Halmillawewa"
            ],
            [
              "id"=> 1191,
              "name"=> "Hengamuwa"
            ],
            [
              "id"=> 1192,
              "name"=> "Hettipola"
            ],
            [
              "id"=> 1193,
              "name"=> "Hilogama"
            ],
            [
              "id"=> 1194,
              "name"=> "Hindagolla"
            ],
            [
              "id"=> 1195,
              "name"=> "Hiriyala Lenawa"
            ],
            [
              "id"=> 1196,
              "name"=> "Hiruwalpola"
            ],
            [
              "id"=> 1197,
              "name"=> "Horambawa"
            ],
            [
              "id"=> 1198,
              "name"=> "Hulogedara"
            ],
            [
              "id"=> 1199,
              "name"=> "Hulugalla"
            ],
            [
              "id"=> 1200,
              "name"=> "Hunupola"
            ],
            [
              "id"=> 1201,
              "name"=> "Ihala Gomugomuwa"
            ],
            [
              "id"=> 1202,
              "name"=> "Ihala Katugampala"
            ],
            [
              "id"=> 1203,
              "name"=> "Indulgodakanda"
            ],
            [
              "id"=> 1204,
              "name"=> "Inguruwatta"
            ],
            [
              "id"=> 1205,
              "name"=> "Iriyagolla"
            ],
            [
              "id"=> 1206,
              "name"=> "Ithanawatta"
            ],
            [
              "id"=> 1207,
              "name"=> "Kadigawa"
            ],
            [
              "id"=> 1208,
              "name"=> "Kahapathwala"
            ],
            [
              "id"=> 1209,
              "name"=> "Kalugamuwa"
            ],
            [
              "id"=> 1210,
              "name"=> "Kanadeniyawala"
            ],
            [
              "id"=> 1211,
              "name"=> "Kanattewewa"
            ],
            [
              "id"=> 1212,
              "name"=> "Karagahagedara"
            ],
            [
              "id"=> 1213,
              "name"=> "Karambe"
            ],
            [
              "id"=> 1214,
              "name"=> "Katupota"
            ],
            [
              "id"=> 1215,
              "name"=> "Kekunagolla"
            ],
            [
              "id"=> 1216,
              "name"=> "Keppitiwalana"
            ],
            [
              "id"=> 1217,
              "name"=> "Kimbulwanaoya"
            ],
            [
              "id"=> 1218,
              "name"=> "Kirimetiyawa"
            ],
            [
              "id"=> 1219,
              "name"=> "Kirindawa"
            ],
            [
              "id"=> 1220,
              "name"=> "Kirindigalla"
            ],
            [
              "id"=> 1221,
              "name"=> "Kithalawa"
            ],
            [
              "id"=> 1222,
              "name"=> "Kobeigane"
            ],
            [
              "id"=> 1223,
              "name"=> "Kohilagedara"
            ],
            [
              "id"=> 1224,
              "name"=> "Konwewa"
            ],
            [
              "id"=> 1225,
              "name"=> "Kosdeniya"
            ],
            [
              "id"=> 1226,
              "name"=> "Kosgolla"
            ],
            [
              "id"=> 1227,
              "name"=> "Kotawehera"
            ],
            [
              "id"=> 1228,
              "name"=> "Kudagalgamuwa"
            ],
            [
              "id"=> 1229,
              "name"=> "Kudakatnoruwa"
            ],
            [
              "id"=> 1230,
              "name"=> "Kuliyapitiya"
            ],
            [
              "id"=> 1231,
              "name"=> "Kumbukgeta"
            ],
            [
              "id"=> 1232,
              "name"=> "Kumbukwewa"
            ],
            [
              "id"=> 1233,
              "name"=> "Kuratihena"
            ],
            [
              "id"=> 1234,
              "name"=> "Kurunegala"
            ],
            [
              "id"=> 1235,
              "name"=> "Labbala"
            ],
            [
              "id"=> 1236,
              "name"=> "lbbagamuwa"
            ],
            [
              "id"=> 1237,
              "name"=> "lhala Kadigamuwa"
            ],
            [
              "id"=> 1238,
              "name"=> "llukhena"
            ],
            [
              "id"=> 1239,
              "name"=> "Lonahettiya"
            ],
            [
              "id"=> 1240,
              "name"=> "Madahapola"
            ],
            [
              "id"=> 1241,
              "name"=> "Madakumburumulla"
            ],
            [
              "id"=> 1242,
              "name"=> "Maduragoda"
            ],
            [
              "id"=> 1243,
              "name"=> "Maeliya"
            ],
            [
              "id"=> 1244,
              "name"=> "Magulagama"
            ],
            [
              "id"=> 1245,
              "name"=> "Mahagalkadawala"
            ],
            [
              "id"=> 1246,
              "name"=> "Mahagirilla"
            ],
            [
              "id"=> 1247,
              "name"=> "Mahamukalanyaya"
            ],
            [
              "id"=> 1248,
              "name"=> "Mahananneriya"
            ],
            [
              "id"=> 1249,
              "name"=> "Maharachchimulla"
            ],
            [
              "id"=> 1250,
              "name"=> "Maho"
            ],
            [
              "id"=> 1251,
              "name"=> "Makulewa"
            ],
            [
              "id"=> 1252,
              "name"=> "Makulpotha"
            ],
            [
              "id"=> 1253,
              "name"=> "Makulwewa"
            ],
            [
              "id"=> 1254,
              "name"=> "Malagane"
            ],
            [
              "id"=> 1255,
              "name"=> "Mandapola"
            ],
            [
              "id"=> 1256,
              "name"=> "Maspotha"
            ],
            [
              "id"=> 1257,
              "name"=> "Mawathagama"
            ],
            [
              "id"=> 1258,
              "name"=> "Medivawa"
            ],
            [
              "id"=> 1259,
              "name"=> "Meegalawa"
            ],
            [
              "id"=> 1260,
              "name"=> "Meetanwala"
            ],
            [
              "id"=> 1261,
              "name"=> "Meewellawa"
            ],
            [
              "id"=> 1262,
              "name"=> "Melsiripura"
            ],
            [
              "id"=> 1263,
              "name"=> "Metikumbura"
            ],
            [
              "id"=> 1264,
              "name"=> "Metiyagane"
            ],
            [
              "id"=> 1265,
              "name"=> "Minhettiya"
            ],
            [
              "id"=> 1266,
              "name"=> "Minuwangete"
            ],
            [
              "id"=> 1267,
              "name"=> "Mirihanagama"
            ],
            [
              "id"=> 1268,
              "name"=> "Monnekulama"
            ],
            [
              "id"=> 1269,
              "name"=> "Moragane"
            ],
            [
              "id"=> 1270,
              "name"=> "Moragollagama"
            ],
            [
              "id"=> 1271,
              "name"=> "Morathiha"
            ],
            [
              "id"=> 1272,
              "name"=> "Munamaldeniya"
            ],
            [
              "id"=> 1273,
              "name"=> "Muruthenge"
            ],
            [
              "id"=> 1274,
              "name"=> "Nabadewa"
            ],
            [
              "id"=> 1275,
              "name"=> "Nagollagama"
            ],
            [
              "id"=> 1276,
              "name"=> "Nagollagoda"
            ],
            [
              "id"=> 1277,
              "name"=> "Nakkawatta"
            ],
            [
              "id"=> 1278,
              "name"=> "Narammala"
            ],
            [
              "id"=> 1279,
              "name"=> "Narangoda"
            ],
            [
              "id"=> 1280,
              "name"=> "Nawatalwatta"
            ],
            [
              "id"=> 1281,
              "name"=> "Nelliya"
            ],
            [
              "id"=> 1282,
              "name"=> "Nikadalupotha"
            ],
            [
              "id"=> 1283,
              "name"=> "Nikaweratiya"
            ],
            [
              "id"=> 1284,
              "name"=> "Padeniya"
            ],
            [
              "id"=> 1285,
              "name"=> "Padiwela"
            ],
            [
              "id"=> 1286,
              "name"=> "Pahalagiribawa"
            ],
            [
              "id"=> 1287,
              "name"=> "Pahamune"
            ],
            [
              "id"=> 1288,
              "name"=> "Palukadawala"
            ],
            [
              "id"=> 1289,
              "name"=> "Panadaragama"
            ],
            [
              "id"=> 1290,
              "name"=> "Panagamuwa"
            ],
            [
              "id"=> 1291,
              "name"=> "Panaliya"
            ],
            [
              "id"=> 1292,
              "name"=> "Panliyadda"
            ],
            [
              "id"=> 1293,
              "name"=> "Pannala"
            ],
            [
              "id"=> 1294,
              "name"=> "Pansiyagama"
            ],
            [
              "id"=> 1295,
              "name"=> "Periyakadneluwa"
            ],
            [
              "id"=> 1296,
              "name"=> "Pihimbiya Ratmale"
            ],
            [
              "id"=> 1297,
              "name"=> "Pihimbuwa"
            ],
            [
              "id"=> 1298,
              "name"=> "Pilessa"
            ],
            [
              "id"=> 1299,
              "name"=> "Polgahawela"
            ],
            [
              "id"=> 1300,
              "name"=> "Polpitigama"
            ],
            [
              "id"=> 1301,
              "name"=> "Pothuhera"
            ],
            [
              "id"=> 1302,
              "name"=> "Puswelitenna"
            ],
            [
              "id"=> 1303,
              "name"=> "Ridibendiella"
            ],
            [
              "id"=> 1304,
              "name"=> "Ridigama"
            ],
            [
              "id"=> 1305,
              "name"=> "Saliya Asokapura"
            ],
            [
              "id"=> 1306,
              "name"=> "Sandalankawa"
            ],
            [
              "id"=> 1307,
              "name"=> "Sirisetagama"
            ],
            [
              "id"=> 1308,
              "name"=> "Siyambalangamuwa"
            ],
            [
              "id"=> 1309,
              "name"=> "Solepura"
            ],
            [
              "id"=> 1310,
              "name"=> "Solewewa"
            ],
            [
              "id"=> 1311,
              "name"=> "Sunandapura"
            ],
            [
              "id"=> 1312,
              "name"=> "Talawattegedara"
            ],
            [
              "id"=> 1313,
              "name"=> "Tambutta"
            ],
            [
              "id"=> 1314,
              "name"=> "Thalahitimulla"
            ],
            [
              "id"=> 1315,
              "name"=> "Thalakolawewa"
            ],
            [
              "id"=> 1316,
              "name"=> "Thalwita"
            ],
            [
              "id"=> 1317,
              "name"=> "Thambagalla"
            ],
            [
              "id"=> 1318,
              "name"=> "Tharana Udawela"
            ],
            [
              "id"=> 1319,
              "name"=> "Thimbiriyawa"
            ],
            [
              "id"=> 1320,
              "name"=> "Tisogama"
            ],
            [
              "id"=> 1321,
              "name"=> "Torayaya"
            ],
            [
              "id"=> 1322,
              "name"=> "Tuttiripitigama"
            ],
            [
              "id"=> 1323,
              "name"=> "Udubaddawa"
            ],
            [
              "id"=> 1324,
              "name"=> "Uhumiya"
            ],
            [
              "id"=> 1325,
              "name"=> "Ulpotha Pallekele"
            ],
            [
              "id"=> 1326,
              "name"=> "Usgala Siyabmalangamuwa"
            ],
            [
              "id"=> 1327,
              "name"=> "Wadakada"
            ],
            [
              "id"=> 1328,
              "name"=> "Wadumunnegedara"
            ],
            [
              "id"=> 1329,
              "name"=> "Walakumburumulla"
            ],
            [
              "id"=> 1330,
              "name"=> "Wannigama"
            ],
            [
              "id"=> 1331,
              "name"=> "Wannikudawewa"
            ],
            [
              "id"=> 1332,
              "name"=> "Wannilhalagama"
            ],
            [
              "id"=> 1333,
              "name"=> "Wannirasnayakapura"
            ],
            [
              "id"=> 1334,
              "name"=> "Warawewa"
            ],
            [
              "id"=> 1335,
              "name"=> "Wariyapola"
            ],
            [
              "id"=> 1336,
              "name"=> "Watuwatta"
            ],
            [
              "id"=> 1337,
              "name"=> "Weerapokuna"
            ],
            [
              "id"=> 1338,
              "name"=> "Welawa Juncton"
            ],
            [
              "id"=> 1339,
              "name"=> "Welipennagahamulla"
            ],
            [
              "id"=> 1340,
              "name"=> "Wellagala"
            ],
            [
              "id"=> 1341,
              "name"=> "Wellarawa"
            ],
            [
              "id"=> 1342,
              "name"=> "Wellawa"
            ],
            [
              "id"=> 1343,
              "name"=> "Welpalla"
            ],
            [
              "id"=> 1344,
              "name"=> "Wennoruwa"
            ],
            [
              "id"=> 1345,
              "name"=> "Weuda"
            ],
            [
              "id"=> 1346,
              "name"=> "Wewagama"
            ],
            [
              "id"=> 1347,
              "name"=> "Yakwila"
            ],
            [
              "id"=> 1348,
              "name"=> "Yatigaloluwa"
            ],
            [
              "id"=> 1349,
              "name"=> "Adippala"
            ],
            [
              "id"=> 1350,
              "name"=> "Ambakandawila"
            ],
            [
              "id"=> 1351,
              "name"=> "Anamaduwa"
            ],
            [
              "id"=> 1352,
              "name"=> "Andigama"
            ],
            [
              "id"=> 1353,
              "name"=> "Angunawila"
            ],
            [
              "id"=> 1354,
              "name"=> "Arachchikattuwa"
            ],
            [
              "id"=> 1355,
              "name"=> "Attawilluwa"
            ],
            [
              "id"=> 1356,
              "name"=> "Bangadeniya"
            ],
            [
              "id"=> 1357,
              "name"=> "Baranankattuwa"
            ],
            [
              "id"=> 1358,
              "name"=> "Battuluoya"
            ],
            [
              "id"=> 1359,
              "name"=> "Bujjampola"
            ],
            [
              "id"=> 1360,
              "name"=> "Chilaw"
            ],
            [
              "id"=> 1361,
              "name"=> "Dankotuwa"
            ],
            [
              "id"=> 1362,
              "name"=> "Dunkannawa"
            ],
            [
              "id"=> 1363,
              "name"=> "Eluwankulama"
            ],
            [
              "id"=> 1364,
              "name"=> "Ettale"
            ],
            [
              "id"=> 1365,
              "name"=> "Galmuruwa"
            ],
            [
              "id"=> 1366,
              "name"=> "Ihala Kottaramulla"
            ],
            [
              "id"=> 1367,
              "name"=> "Ihala Puliyankulama"
            ],
            [
              "id"=> 1368,
              "name"=> "Ilippadeniya"
            ],
            [
              "id"=> 1369,
              "name"=> "Inginimitiya"
            ],
            [
              "id"=> 1370,
              "name"=> "Ismailpuram"
            ],
            [
              "id"=> 1371,
              "name"=> "Kakkapalliya"
            ],
            [
              "id"=> 1372,
              "name"=> "Kalladiya"
            ],
            [
              "id"=> 1373,
              "name"=> "Kalpitiya"
            ],
            [
              "id"=> 1374,
              "name"=> "Kandakuliya"
            ],
            [
              "id"=> 1375,
              "name"=> "Karativponparappi"
            ],
            [
              "id"=> 1376,
              "name"=> "Karawitagara"
            ],
            [
              "id"=> 1377,
              "name"=> "Karuwalagaswewa"
            ],
            [
              "id"=> 1378,
              "name"=> "Katuneriya"
            ],
            [
              "id"=> 1379,
              "name"=> "Kirimundalama"
            ],
            [
              "id"=> 1380,
              "name"=> "Koswatta"
            ],
            [
              "id"=> 1381,
              "name"=> "Kottantivu"
            ],
            [
              "id"=> 1382,
              "name"=> "Kottukachchiya"
            ],
            [
              "id"=> 1383,
              "name"=> "Kudawewa"
            ],
            [
              "id"=> 1384,
              "name"=> "Kumarakattuwa"
            ],
            [
              "id"=> 1385,
              "name"=> "Kurinjanpitiya"
            ],
            [
              "id"=> 1386,
              "name"=> "Kuruketiyawa"
            ],
            [
              "id"=> 1387,
              "name"=> "Lihiriyagama"
            ],
            [
              "id"=> 1388,
              "name"=> "Lunuwila"
            ],
            [
              "id"=> 1389,
              "name"=> "Madampe"
            ],
            [
              "id"=> 1390,
              "name"=> "Madurankuliya"
            ],
            [
              "id"=> 1391,
              "name"=> "Mahakumbukkadawala"
            ],
            [
              "id"=> 1392,
              "name"=> "Mahauswewa"
            ],
            [
              "id"=> 1393,
              "name"=> "Mahawewa"
            ],
            [
              "id"=> 1394,
              "name"=> "Mampuri"
            ],
            [
              "id"=> 1395,
              "name"=> "Mangalaeliya"
            ],
            [
              "id"=> 1396,
              "name"=> "Marawila"
            ],
            [
              "id"=> 1397,
              "name"=> "Mudalakkuliya"
            ],
            [
              "id"=> 1398,
              "name"=> "Mugunuwatawana"
            ],
            [
              "id"=> 1399,
              "name"=> "Mukkutoduwawa"
            ],
            [
              "id"=> 1400,
              "name"=> "Mundel"
            ],
            [
              "id"=> 1401,
              "name"=> "Muttibendiwila"
            ],
            [
              "id"=> 1402,
              "name"=> "Nainamadama"
            ],
            [
              "id"=> 1403,
              "name"=> "Nalladarankattuwa"
            ],
            [
              "id"=> 1404,
              "name"=> "Nattandiya"
            ],
            [
              "id"=> 1405,
              "name"=> "Nawagattegama"
            ],
            [
              "id"=> 1406,
              "name"=> "Norachcholai"
            ],
            [
              "id"=> 1407,
              "name"=> "Palaviya"
            ],
            [
              "id"=> 1408,
              "name"=> "Pallama"
            ],
            [
              "id"=> 1409,
              "name"=> "Palliwasalturai"
            ],
            [
              "id"=> 1410,
              "name"=> "Panirendawa"
            ],
            [
              "id"=> 1411,
              "name"=> "Pothuwatawana"
            ],
            [
              "id"=> 1412,
              "name"=> "Puttalam"
            ],
            [
              "id"=> 1413,
              "name"=> "Puttalam Cement Factory"
            ],
            [
              "id"=> 1414,
              "name"=> "Rajakadaluwa"
            ],
            [
              "id"=> 1415,
              "name"=> "Saliyawewa Junction"
            ],
            [
              "id"=> 1416,
              "name"=> "Serukele"
            ],
            [
              "id"=> 1417,
              "name"=> "Sirambiadiya"
            ],
            [
              "id"=> 1418,
              "name"=> "Siyambalagashene"
            ],
            [
              "id"=> 1419,
              "name"=> "Tabbowa"
            ],
            [
              "id"=> 1420,
              "name"=> "Talawila Church"
            ],
            [
              "id"=> 1421,
              "name"=> "Toduwawa"
            ],
            [
              "id"=> 1422,
              "name"=> "Udappuwa"
            ],
            [
              "id"=> 1423,
              "name"=> "Uridyawa"
            ],
            [
              "id"=> 1424,
              "name"=> "Vanathawilluwa"
            ],
            [
              "id"=> 1425,
              "name"=> "Waikkal"
            ],
            [
              "id"=> 1426,
              "name"=> "Watugahamulla"
            ],
            [
              "id"=> 1427,
              "name"=> "Wennappuwa"
            ],
            [
              "id"=> 1428,
              "name"=> "Wijeyakatupotha"
            ],
            [
              "id"=> 1429,
              "name"=> "Wilpotha"
            ],
            [
              "id"=> 1430,
              "name"=> "Yogiyana"
            ],
            [
              "id"=> 1431,
              "name"=> "Akarella"
            ],
            [
              "id"=> 1432,
              "name"=> "Atakalanpanna"
            ],
            [
              "id"=> 1433,
              "name"=> "Ayagama"
            ],
            [
              "id"=> 1434,
              "name"=> "Balangoda"
            ],
            [
              "id"=> 1435,
              "name"=> "Batatota"
            ],
            [
              "id"=> 1436,
              "name"=> "Belihuloya"
            ],
            [
              "id"=> 1437,
              "name"=> "Bolthumbe"
            ],
            [
              "id"=> 1438,
              "name"=> "Bomluwageaina"
            ],
            [
              "id"=> 1439,
              "name"=> "Bulutota"
            ],
            [
              "id"=> 1440,
              "name"=> "Dambuluwana"
            ],
            [
              "id"=> 1441,
              "name"=> "Daugala"
            ],
            [
              "id"=> 1442,
              "name"=> "Dela"
            ],
            [
              "id"=> 1443,
              "name"=> "Delwala"
            ],
            [
              "id"=> 1444,
              "name"=> "Demuwatha"
            ],
            [
              "id"=> 1445,
              "name"=> "Dodampe"
            ],
            [
              "id"=> 1446,
              "name"=> "Doloswalakanda"
            ],
            [
              "id"=> 1447,
              "name"=> "Dumbara Manana"
            ],
            [
              "id"=> 1448,
              "name"=> "Eheliyagoda"
            ],
            [
              "id"=> 1449,
              "name"=> "Elapatha"
            ],
            [
              "id"=> 1450,
              "name"=> "Ellagawa"
            ],
            [
              "id"=> 1451,
              "name"=> "Ellaulla"
            ],
            [
              "id"=> 1452,
              "name"=> "Ellawala"
            ],
            [
              "id"=> 1453,
              "name"=> "Embilipitiya"
            ],
            [
              "id"=> 1454,
              "name"=> "Eratna"
            ],
            [
              "id"=> 1455,
              "name"=> "Erepola"
            ],
            [
              "id"=> 1456,
              "name"=> "Gabbela"
            ],
            [
              "id"=> 1457,
              "name"=> "Gallella"
            ],
            [
              "id"=> 1458,
              "name"=> "Gangeyaya"
            ],
            [
              "id"=> 1459,
              "name"=> "Gawaragiriya"
            ],
            [
              "id"=> 1460,
              "name"=> "Getahetta"
            ],
            [
              "id"=> 1461,
              "name"=> "Gillimale"
            ],
            [
              "id"=> 1462,
              "name"=> "Godagampola"
            ],
            [
              "id"=> 1463,
              "name"=> "Godakawela"
            ],
            [
              "id"=> 1464,
              "name"=> "Gurubewilagama"
            ],
            [
              "id"=> 1465,
              "name"=> "Halpe"
            ],
            [
              "id"=> 1466,
              "name"=> "Halwinna"
            ],
            [
              "id"=> 1467,
              "name"=> "Handagiriya"
            ],
            [
              "id"=> 1468,
              "name"=> "Hapugastenna"
            ],
            [
              "id"=> 1469,
              "name"=> "Hatangala"
            ],
            [
              "id"=> 1470,
              "name"=> "Hatarabage"
            ],
            [
              "id"=> 1471,
              "name"=> "Hidellana"
            ],
            [
              "id"=> 1472,
              "name"=> "Hiramadagama"
            ],
            [
              "id"=> 1473,
              "name"=> "Ihalagama"
            ],
            [
              "id"=> 1474,
              "name"=> "Ittakanda"
            ],
            [
              "id"=> 1475,
              "name"=> "Kahangama"
            ],
            [
              "id"=> 1476,
              "name"=> "Kahawatta"
            ],
            [
              "id"=> 1477,
              "name"=> "Kalawana"
            ],
            [
              "id"=> 1478,
              "name"=> "Kaltota"
            ],
            [
              "id"=> 1479,
              "name"=> "Karandana"
            ],
            [
              "id"=> 1480,
              "name"=> "Karangoda"
            ],
            [
              "id"=> 1481,
              "name"=> "Kella Junction"
            ],
            [
              "id"=> 1482,
              "name"=> "Kiriella"
            ],
            [
              "id"=> 1483,
              "name"=> "Kolambageara"
            ],
            [
              "id"=> 1484,
              "name"=> "Kolombugama"
            ],
            [
              "id"=> 1485,
              "name"=> "Kolonna"
            ],
            [
              "id"=> 1486,
              "name"=> "Kudawa"
            ],
            [
              "id"=> 1487,
              "name"=> "Kuruwita"
            ],
            [
              "id"=> 1488,
              "name"=> "Lellopitiya"
            ],
            [
              "id"=> 1489,
              "name"=> "lmbulpe"
            ],
            [
              "id"=> 1490,
              "name"=> "Madalagama"
            ],
            [
              "id"=> 1491,
              "name"=> "Mahawalatenna"
            ],
            [
              "id"=> 1492,
              "name"=> "Makandura Sabara"
            ],
            [
              "id"=> 1493,
              "name"=> "Malwala Junction"
            ],
            [
              "id"=> 1494,
              "name"=> "Marapana"
            ],
            [
              "id"=> 1495,
              "name"=> "Matuwagalagama"
            ],
            [
              "id"=> 1496,
              "name"=> "Medagalatur"
            ],
            [
              "id"=> 1497,
              "name"=> "Meddekanda"
            ],
            [
              "id"=> 1498,
              "name"=> "Minipura Dumbara"
            ],
            [
              "id"=> 1499,
              "name"=> "Mitipola"
            ],
            [
              "id"=> 1500,
              "name"=> "Morahela"
            ],
            [
              "id"=> 1501,
              "name"=> "Mulendiyawala"
            ],
            [
              "id"=> 1502,
              "name"=> "Mulgama"
            ],
            [
              "id"=> 1503,
              "name"=> "Nawalakanda"
            ],
            [
              "id"=> 1504,
              "name"=> "NawinnaPinnakanda"
            ],
            [
              "id"=> 1505,
              "name"=> "Niralagama"
            ],
            [
              "id"=> 1506,
              "name"=> "Nivitigala"
            ],
            [
              "id"=> 1507,
              "name"=> "Omalpe"
            ],
            [
              "id"=> 1508,
              "name"=> "Opanayaka"
            ],
            [
              "id"=> 1509,
              "name"=> "Padalangala"
            ],
            [
              "id"=> 1510,
              "name"=> "Pallebedda"
            ],
            [
              "id"=> 1511,
              "name"=> "Pambagolla"
            ],
            [
              "id"=> 1512,
              "name"=> "Panamura"
            ],
            [
              "id"=> 1513,
              "name"=> "Panapitiya"
            ],
            [
              "id"=> 1514,
              "name"=> "Panapola"
            ],
            [
              "id"=> 1515,
              "name"=> "Panawala"
            ],
            [
              "id"=> 1516,
              "name"=> "Parakaduwa"
            ],
            [
              "id"=> 1517,
              "name"=> "Pebotuwa"
            ],
            [
              "id"=> 1518,
              "name"=> "Pelmadulla"
            ],
            [
              "id"=> 1519,
              "name"=> "Pimbura"
            ],
            [
              "id"=> 1520,
              "name"=> "Pinnawala"
            ],
            [
              "id"=> 1521,
              "name"=> "Pothupitiya"
            ],
            [
              "id"=> 1522,
              "name"=> "Rajawaka"
            ],
            [
              "id"=> 1523,
              "name"=> "Rakwana"
            ],
            [
              "id"=> 1524,
              "name"=> "Ranwala"
            ],
            [
              "id"=> 1525,
              "name"=> "Rassagala"
            ],
            [
              "id"=> 1526,
              "name"=> "Ratna Hangamuwa"
            ],
            [
              "id"=> 1527,
              "name"=> "Ratnapura"
            ],
            [
              "id"=> 1528,
              "name"=> "Samanalawewa"
            ],
            [
              "id"=> 1529,
              "name"=> "Sri Palabaddala"
            ],
            [
              "id"=> 1530,
              "name"=> "Sudagala"
            ],
            [
              "id"=> 1531,
              "name"=> "Talakolahinna"
            ],
            [
              "id"=> 1532,
              "name"=> "Tanjantenna"
            ],
            [
              "id"=> 1533,
              "name"=> "Teppanawa"
            ],
            [
              "id"=> 1534,
              "name"=> "Tunkama"
            ],
            [
              "id"=> 1535,
              "name"=> "Udaha Hawupe"
            ],
            [
              "id"=> 1536,
              "name"=> "Udakarawita"
            ],
            [
              "id"=> 1537,
              "name"=> "Udaniriella"
            ],
            [
              "id"=> 1538,
              "name"=> "Udawalawe"
            ],
            [
              "id"=> 1539,
              "name"=> "Ullinduwawa"
            ],
            [
              "id"=> 1540,
              "name"=> "Veddagala"
            ],
            [
              "id"=> 1541,
              "name"=> "Vijeriya"
            ],
            [
              "id"=> 1542,
              "name"=> "Waleboda"
            ],
            [
              "id"=> 1543,
              "name"=> "Watapotha"
            ],
            [
              "id"=> 1544,
              "name"=> "Waturawa"
            ],
            [
              "id"=> 1545,
              "name"=> "Weligepola"
            ],
            [
              "id"=> 1546,
              "name"=> "Welipathayaya"
            ],
            [
              "id"=> 1547,
              "name"=> "Wewelwatta"
            ],
            [
              "id"=> 1548,
              "name"=> "Wikiliya"
            ],
            [
              "id"=> 1549,
              "name"=> "Alawatura"
            ],
            [
              "id"=> 1550,
              "name"=> "Algama"
            ],
            [
              "id"=> 1551,
              "name"=> "Alutnuwara"
            ],
            [
              "id"=> 1552,
              "name"=> "Ambalakanda"
            ],
            [
              "id"=> 1553,
              "name"=> "Ambulugala"
            ],
            [
              "id"=> 1554,
              "name"=> "Amitirigala"
            ],
            [
              "id"=> 1555,
              "name"=> "Ampagala"
            ],
            [
              "id"=> 1556,
              "name"=> "Anhettigama"
            ],
            [
              "id"=> 1557,
              "name"=> "Aranayaka"
            ],
            [
              "id"=> 1558,
              "name"=> "Aruggammana"
            ],
            [
              "id"=> 1559,
              "name"=> "Atale"
            ],
            [
              "id"=> 1560,
              "name"=> "Batuwita"
            ],
            [
              "id"=> 1561,
              "name"=> "BeligalaSab"
            ],
            [
              "id"=> 1562,
              "name"=> "Berannawa"
            ],
            [
              "id"=> 1563,
              "name"=> "Bopitiya SAB"
            ],
            [
              "id"=> 1564,
              "name"=> "Boralankada"
            ],
            [
              "id"=> 1565,
              "name"=> "Bossella"
            ],
            [
              "id"=> 1566,
              "name"=> "Bulathkohupitiya"
            ],
            [
              "id"=> 1567,
              "name"=> "Damunupola"
            ],
            [
              "id"=> 1568,
              "name"=> "Debathgama"
            ],
            [
              "id"=> 1569,
              "name"=> "Dedugala"
            ],
            [
              "id"=> 1570,
              "name"=> "Deewala Pallegama"
            ],
            [
              "id"=> 1571,
              "name"=> "Dehiowita"
            ],
            [
              "id"=> 1572,
              "name"=> "Deldeniya"
            ],
            [
              "id"=> 1573,
              "name"=> "Deloluwa"
            ],
            [
              "id"=> 1574,
              "name"=> "Deraniyagala"
            ],
            [
              "id"=> 1575,
              "name"=> "Dewalegama"
            ],
            [
              "id"=> 1576,
              "name"=> "Dewanagala"
            ],
            [
              "id"=> 1577,
              "name"=> "Dombemada"
            ],
            [
              "id"=> 1578,
              "name"=> "Dorawaka"
            ],
            [
              "id"=> 1579,
              "name"=> "Dunumala"
            ],
            [
              "id"=> 1580,
              "name"=> "Galapitamada"
            ],
            [
              "id"=> 1581,
              "name"=> "Galatara"
            ],
            [
              "id"=> 1582,
              "name"=> "Galigamuwa Town"
            ],
            [
              "id"=> 1583,
              "name"=> "GalpathaSab"
            ],
            [
              "id"=> 1584,
              "name"=> "Gantuna"
            ],
            [
              "id"=> 1585,
              "name"=> "Gonagala"
            ],
            [
              "id"=> 1586,
              "name"=> "Hakahinna"
            ],
            [
              "id"=> 1587,
              "name"=> "Hakbellawaka"
            ],
            [
              "id"=> 1588,
              "name"=> "Helamada"
            ],
            [
              "id"=> 1589,
              "name"=> "Hemmatagama"
            ],
            [
              "id"=> 1590,
              "name"=> "Hettimulla"
            ],
            [
              "id"=> 1591,
              "name"=> "Hewadiwela"
            ],
            [
              "id"=> 1592,
              "name"=> "Hingula"
            ],
            [
              "id"=> 1593,
              "name"=> "Hinguralakanda"
            ],
            [
              "id"=> 1594,
              "name"=> "Hiriwadunna"
            ],
            [
              "id"=> 1595,
              "name"=> "Imbulana"
            ],
            [
              "id"=> 1596,
              "name"=> "Imbulgasdeniya"
            ],
            [
              "id"=> 1597,
              "name"=> "Kabagamuwa"
            ],
            [
              "id"=> 1598,
              "name"=> "Kannattota"
            ],
            [
              "id"=> 1599,
              "name"=> "Kegalle"
            ],
            [
              "id"=> 1600,
              "name"=> "Kehelpannala"
            ],
            [
              "id"=> 1601,
              "name"=> "Kitulgala"
            ],
            [
              "id"=> 1602,
              "name"=> "Kondeniya"
            ],
            [
              "id"=> 1603,
              "name"=> "Kotiyakumbura"
            ],
            [
              "id"=> 1604,
              "name"=> "Lewangama"
            ],
            [
              "id"=> 1605,
              "name"=> "Mahabage"
            ],
            [
              "id"=> 1606,
              "name"=> "Mahapallegama"
            ],
            [
              "id"=> 1607,
              "name"=> "Maharangalla"
            ],
            [
              "id"=> 1608,
              "name"=> "Makehelwala"
            ],
            [
              "id"=> 1609,
              "name"=> "Malalpola"
            ],
            [
              "id"=> 1610,
              "name"=> "Maliboda"
            ],
            [
              "id"=> 1611,
              "name"=> "Malmaduwa"
            ],
            [
              "id"=> 1612,
              "name"=> "Mawanella"
            ],
            [
              "id"=> 1613,
              "name"=> "Migastenna Sabara"
            ],
            [
              "id"=> 1614,
              "name"=> "Miyanawita"
            ],
            [
              "id"=> 1615,
              "name"=> "Molagoda"
            ],
            [
              "id"=> 1616,
              "name"=> "Morontota"
            ],
            [
              "id"=> 1617,
              "name"=> "Nelundeniya"
            ],
            [
              "id"=> 1618,
              "name"=> "Niyadurupola"
            ],
            [
              "id"=> 1619,
              "name"=> "Noori"
            ],
            [
              "id"=> 1620,
              "name"=> "Parape"
            ],
            [
              "id"=> 1621,
              "name"=> "Pattampitiya"
            ],
            [
              "id"=> 1622,
              "name"=> "Pinnawala"
            ],
            [
              "id"=> 1623,
              "name"=> "Pitagaldeniya"
            ],
            [
              "id"=> 1624,
              "name"=> "Pothukoladeniya"
            ],
            [
              "id"=> 1625,
              "name"=> "Rambukkana"
            ],
            [
              "id"=> 1626,
              "name"=> "Ruwanwella"
            ],
            [
              "id"=> 1627,
              "name"=> "Seaforth Colony"
            ],
            [
              "id"=> 1628,
              "name"=> "Talgaspitiya"
            ],
            [
              "id"=> 1629,
              "name"=> "Teligama"
            ],
            [
              "id"=> 1630,
              "name"=> "Tholangamuwa"
            ],
            [
              "id"=> 1631,
              "name"=> "Thotawella"
            ],
            [
              "id"=> 1632,
              "name"=> "Tulhiriya"
            ],
            [
              "id"=> 1633,
              "name"=> "Tuntota"
            ],
            [
              "id"=> 1634,
              "name"=> "Udagaldeniya"
            ],
            [
              "id"=> 1635,
              "name"=> "Udapotha"
            ],
            [
              "id"=> 1636,
              "name"=> "Udumulla"
            ],
            [
              "id"=> 1637,
              "name"=> "Undugoda"
            ],
            [
              "id"=> 1638,
              "name"=> "Ussapitiya"
            ],
            [
              "id"=> 1639,
              "name"=> "Wahakula"
            ],
            [
              "id"=> 1640,
              "name"=> "Waharaka"
            ],
            [
              "id"=> 1641,
              "name"=> "Warakapola"
            ],
            [
              "id"=> 1642,
              "name"=> "Watura"
            ],
            [
              "id"=> 1643,
              "name"=> "Weeoya"
            ],
            [
              "id"=> 1644,
              "name"=> "Wegalla"
            ],
            [
              "id"=> 1645,
              "name"=> "Welihelatenna"
            ],
            [
              "id"=> 1646,
              "name"=> "Weragala"
            ],
            [
              "id"=> 1647,
              "name"=> "Yatagama"
            ],
            [
              "id"=> 1648,
              "name"=> "Yatapana"
            ],
            [
              "id"=> 1649,
              "name"=> "Yatiyantota"
            ],
            [
              "id"=> 1650,
              "name"=> "Yattogoda"
            ],
            [
              "id"=> 1651,
              "name"=> "Agaliya"
            ],
            [
              "id"=> 1652,
              "name"=> "Ahangama"
            ],
            [
              "id"=> 1653,
              "name"=> "Ahungalla"
            ],
            [
              "id"=> 1654,
              "name"=> "Akmeemana"
            ],
            [
              "id"=> 1655,
              "name"=> "Aluthwala"
            ],
            [
              "id"=> 1656,
              "name"=> "Ambalangoda"
            ],
            [
              "id"=> 1657,
              "name"=> "Ampegama"
            ],
            [
              "id"=> 1658,
              "name"=> "Amugoda"
            ],
            [
              "id"=> 1659,
              "name"=> "Anangoda"
            ],
            [
              "id"=> 1660,
              "name"=> "Angulugaha"
            ],
            [
              "id"=> 1661,
              "name"=> "Ankokkawala"
            ],
            [
              "id"=> 1662,
              "name"=> "Baddegama"
            ],
            [
              "id"=> 1663,
              "name"=> "Balapitiya"
            ],
            [
              "id"=> 1664,
              "name"=> "Banagala"
            ],
            [
              "id"=> 1665,
              "name"=> "Batapola"
            ],
            [
              "id"=> 1666,
              "name"=> "Bentota"
            ],
            [
              "id"=> 1667,
              "name"=> "Boossa"
            ],
            [
              "id"=> 1668,
              "name"=> "Dikkumbura"
            ],
            [
              "id"=> 1669,
              "name"=> "Dodanduwa"
            ],
            [
              "id"=> 1670,
              "name"=> "Ella Tanabaddegama"
            ],
            [
              "id"=> 1671,
              "name"=> "Elpitiya"
            ],
            [
              "id"=> 1672,
              "name"=> "Ethkandura"
            ],
            [
              "id"=> 1673,
              "name"=> "Galle"
            ],
            [
              "id"=> 1674,
              "name"=> "Ganegoda"
            ],
            [
              "id"=> 1675,
              "name"=> "Ginimellagaha"
            ],
            [
              "id"=> 1676,
              "name"=> "Gintota"
            ],
            [
              "id"=> 1677,
              "name"=> "Godahena"
            ],
            [
              "id"=> 1678,
              "name"=> "Gonagalpura"
            ],
            [
              "id"=> 1679,
              "name"=> "Gonamulla Junction"
            ],
            [
              "id"=> 1680,
              "name"=> "Gonapinuwala"
            ],
            [
              "id"=> 1681,
              "name"=> "Habaraduwa"
            ],
            [
              "id"=> 1682,
              "name"=> "Haburugala"
            ],
            [
              "id"=> 1683,
              "name"=> "Halvitigala Colony"
            ],
            [
              "id"=> 1684,
              "name"=> "Hapugala"
            ],
            [
              "id"=> 1685,
              "name"=> "Hawpe"
            ],
            [
              "id"=> 1686,
              "name"=> "Hikkaduwa"
            ],
            [
              "id"=> 1687,
              "name"=> "Hinatigala"
            ],
            [
              "id"=> 1688,
              "name"=> "Hiniduma"
            ],
            [
              "id"=> 1689,
              "name"=> "Hiyare"
            ],
            [
              "id"=> 1690,
              "name"=> "Ihala Walpola"
            ],
            [
              "id"=> 1691,
              "name"=> "Induruwa"
            ],
            [
              "id"=> 1692,
              "name"=> "Kahaduwa"
            ],
            [
              "id"=> 1693,
              "name"=> "Kahawa"
            ],
            [
              "id"=> 1694,
              "name"=> "Kananke Bazaar"
            ],
            [
              "id"=> 1695,
              "name"=> "Karagoda"
            ],
            [
              "id"=> 1696,
              "name"=> "Karandeniya"
            ],
            [
              "id"=> 1697,
              "name"=> "Karapitiya"
            ],
            [
              "id"=> 1698,
              "name"=> "Koggala"
            ],
            [
              "id"=> 1699,
              "name"=> "Kosgoda"
            ],
            [
              "id"=> 1700,
              "name"=> "Kottawagama"
            ],
            [
              "id"=> 1701,
              "name"=> "Kuleegoda"
            ],
            [
              "id"=> 1702,
              "name"=> "lhalahewessa"
            ],
            [
              "id"=> 1703,
              "name"=> "lmaduwa"
            ],
            [
              "id"=> 1704,
              "name"=> "lnduruwa"
            ],
            [
              "id"=> 1705,
              "name"=> "Magedara"
            ],
            [
              "id"=> 1706,
              "name"=> "Malgalla Talangalla"
            ],
            [
              "id"=> 1707,
              "name"=> "Mapalagama"
            ],
            [
              "id"=> 1708,
              "name"=> "Mapalagama Central"
            ],
            [
              "id"=> 1709,
              "name"=> "Mattaka"
            ],
            [
              "id"=> 1710,
              "name"=> "Meda-Keembiya"
            ],
            [
              "id"=> 1711,
              "name"=> "Meetiyagoda"
            ],
            [
              "id"=> 1712,
              "name"=> "Miriswatta"
            ],
            [
              "id"=> 1713,
              "name"=> "Nagoda"
            ],
            [
              "id"=> 1714,
              "name"=> "Nakiyadeniya"
            ],
            [
              "id"=> 1715,
              "name"=> "Nawadagala"
            ],
            [
              "id"=> 1716,
              "name"=> "Neluwa"
            ],
            [
              "id"=> 1717,
              "name"=> "Nindana"
            ],
            [
              "id"=> 1718,
              "name"=> "Opatha"
            ],
            [
              "id"=> 1719,
              "name"=> "Panangala"
            ],
            [
              "id"=> 1720,
              "name"=> "Pannimulla Panagoda"
            ],
            [
              "id"=> 1721,
              "name"=> "Parana ThanaYamgoda"
            ],
            [
              "id"=> 1722,
              "name"=> "Pitigala"
            ],
            [
              "id"=> 1723,
              "name"=> "Poddala"
            ],
            [
              "id"=> 1724,
              "name"=> "Porawagama"
            ],
            [
              "id"=> 1725,
              "name"=> "Rantotuwila"
            ],
            [
              "id"=> 1726,
              "name"=> "Ratgama"
            ],
            [
              "id"=> 1727,
              "name"=> "Talagampola"
            ],
            [
              "id"=> 1728,
              "name"=> "Talgaspe"
            ],
            [
              "id"=> 1729,
              "name"=> "Talgaswela"
            ],
            [
              "id"=> 1730,
              "name"=> "Talpe"
            ],
            [
              "id"=> 1731,
              "name"=> "Tawalama"
            ],
            [
              "id"=> 1732,
              "name"=> "Tiranagama"
            ],
            [
              "id"=> 1733,
              "name"=> "Udalamatta"
            ],
            [
              "id"=> 1734,
              "name"=> "Udugama"
            ],
            [
              "id"=> 1735,
              "name"=> "Uluvitike"
            ],
            [
              "id"=> 1736,
              "name"=> "Unawatuna"
            ],
            [
              "id"=> 1737,
              "name"=> "Unenwitiya"
            ],
            [
              "id"=> 1738,
              "name"=> "Uragaha"
            ],
            [
              "id"=> 1739,
              "name"=> "Uragasmanhandiya"
            ],
            [
              "id"=> 1740,
              "name"=> "Wakwella"
            ],
            [
              "id"=> 1741,
              "name"=> "Walahanduwa"
            ],
            [
              "id"=> 1742,
              "name"=> "Wanchawela"
            ],
            [
              "id"=> 1743,
              "name"=> "Wanduramba"
            ],
            [
              "id"=> 1744,
              "name"=> "Warukandeniya"
            ],
            [
              "id"=> 1745,
              "name"=> "Watugedara"
            ],
            [
              "id"=> 1746,
              "name"=> "Weihena"
            ],
            [
              "id"=> 1747,
              "name"=> "Yakkalamulla"
            ],
            [
              "id"=> 1748,
              "name"=> "Yatalamatta"
            ],
            [
              "id"=> 1749,
              "name"=> "Akuressa"
            ],
            [
              "id"=> 1750,
              "name"=> "Alapaladeniya"
            ],
            [
              "id"=> 1751,
              "name"=> "Aparekka"
            ],
            [
              "id"=> 1752,
              "name"=> "Athuraliya"
            ],
            [
              "id"=> 1753,
              "name"=> "Bengamuwa"
            ],
            [
              "id"=> 1754,
              "name"=> "Beralapanathara"
            ],
            [
              "id"=> 1755,
              "name"=> "Bopagoda"
            ],
            [
              "id"=> 1756,
              "name"=> "Dampahala"
            ],
            [
              "id"=> 1757,
              "name"=> "Deegala Lenama"
            ],
            [
              "id"=> 1758,
              "name"=> "Deiyandara"
            ],
            [
              "id"=> 1759,
              "name"=> "Dellawa"
            ],
            [
              "id"=> 1760,
              "name"=> "Denagama"
            ],
            [
              "id"=> 1761,
              "name"=> "Denipitiya"
            ],
            [
              "id"=> 1762,
              "name"=> "Deniyaya"
            ],
            [
              "id"=> 1763,
              "name"=> "Derangala"
            ],
            [
              "id"=> 1764,
              "name"=> "Devinuwara Dondra"
            ],
            [
              "id"=> 1765,
              "name"=> "Dikwella"
            ],
            [
              "id"=> 1766,
              "name"=> "Diyagaha"
            ],
            [
              "id"=> 1767,
              "name"=> "Diyalape"
            ],
            [
              "id"=> 1768,
              "name"=> "Gandara"
            ],
            [
              "id"=> 1769,
              "name"=> "Godapitiya"
            ],
            [
              "id"=> 1770,
              "name"=> "Gomilamawarala"
            ],
            [
              "id"=> 1771,
              "name"=> "Hakmana"
            ],
            [
              "id"=> 1772,
              "name"=> "Handugala"
            ],
            [
              "id"=> 1773,
              "name"=> "Horapawita"
            ],
            [
              "id"=> 1774,
              "name"=> "Kalubowitiyana"
            ],
            [
              "id"=> 1775,
              "name"=> "Kamburugamuwa"
            ],
            [
              "id"=> 1776,
              "name"=> "Kamburupitiya"
            ],
            [
              "id"=> 1777,
              "name"=> "Karagoda Uyangoda"
            ],
            [
              "id"=> 1778,
              "name"=> "Karaputugala"
            ],
            [
              "id"=> 1779,
              "name"=> "Karatota"
            ],
            [
              "id"=> 1780,
              "name"=> "Kekanadurra"
            ],
            [
              "id"=> 1781,
              "name"=> "Kiriweldola"
            ],
            [
              "id"=> 1782,
              "name"=> "Kiriwelkele"
            ],
            [
              "id"=> 1783,
              "name"=> "Kolawenigama"
            ],
            [
              "id"=> 1784,
              "name"=> "Kotapola"
            ],
            [
              "id"=> 1785,
              "name"=> "Kottegoda"
            ],
            [
              "id"=> 1786,
              "name"=> "Lankagama"
            ],
            [
              "id"=> 1787,
              "name"=> "Makandura"
            ],
            [
              "id"=> 1788,
              "name"=> "Maliduwa"
            ],
            [
              "id"=> 1789,
              "name"=> "Maramba"
            ],
            [
              "id"=> 1790,
              "name"=> "Matara"
            ],
            [
              "id"=> 1791,
              "name"=> "Mediripitiya"
            ],
            [
              "id"=> 1792,
              "name"=> "Miella"
            ],
            [
              "id"=> 1793,
              "name"=> "Mirissa"
            ],
            [
              "id"=> 1794,
              "name"=> "Moragala Kirillapone"
            ],
            [
              "id"=> 1795,
              "name"=> "Morawaka"
            ],
            [
              "id"=> 1796,
              "name"=> "Mulatiyana Junction"
            ],
            [
              "id"=> 1797,
              "name"=> "Nadugala"
            ],
            [
              "id"=> 1798,
              "name"=> "Naimana"
            ],
            [
              "id"=> 1799,
              "name"=> "Narawelpita"
            ],
            [
              "id"=> 1800,
              "name"=> "Pahala Millawa"
            ],
            [
              "id"=> 1801,
              "name"=> "Palatuwa"
            ],
            [
              "id"=> 1802,
              "name"=> "Paragala"
            ],
            [
              "id"=> 1803,
              "name"=> "Parapamulla"
            ],
            [
              "id"=> 1804,
              "name"=> "Pasgoda"
            ],
            [
              "id"=> 1805,
              "name"=> "Penetiyana"
            ],
            [
              "id"=> 1806,
              "name"=> "Pitabeddara"
            ],
            [
              "id"=> 1807,
              "name"=> "Polhena"
            ],
            [
              "id"=> 1808,
              "name"=> "Pothdeniya"
            ],
            [
              "id"=> 1809,
              "name"=> "Puhulwella"
            ],
            [
              "id"=> 1810,
              "name"=> "Radawela"
            ],
            [
              "id"=> 1811,
              "name"=> "Ransegoda"
            ],
            [
              "id"=> 1812,
              "name"=> "Ratmale"
            ],
            [
              "id"=> 1813,
              "name"=> "Rotumba"
            ],
            [
              "id"=> 1814,
              "name"=> "Siyambalagoda"
            ],
            [
              "id"=> 1815,
              "name"=> "Sultanagoda"
            ],
            [
              "id"=> 1816,
              "name"=> "Telijjawila"
            ],
            [
              "id"=> 1817,
              "name"=> "Thihagoda"
            ],
            [
              "id"=> 1818,
              "name"=> "Urubokka"
            ],
            [
              "id"=> 1819,
              "name"=> "Urugamuwa"
            ],
            [
              "id"=> 1820,
              "name"=> "Urumutta"
            ],
            [
              "id"=> 1821,
              "name"=> "Viharahena"
            ],
            [
              "id"=> 1822,
              "name"=> "Walakanda"
            ],
            [
              "id"=> 1823,
              "name"=> "Walasgala"
            ],
            [
              "id"=> 1824,
              "name"=> "Walpola"
            ],
            [
              "id"=> 1825,
              "name"=> "Waralla"
            ],
            [
              "id"=> 1826,
              "name"=> "Weligama"
            ],
            [
              "id"=> 1827,
              "name"=> "Wilpita"
            ],
            [
              "id"=> 1828,
              "name"=> "Yatiyana"
            ],
            [
              "id"=> 1829,
              "name"=> "Ambalantota"
            ],
            [
              "id"=> 1830,
              "name"=> "Angunakolapelessa"
            ],
            [
              "id"=> 1831,
              "name"=> "Bandagiriya Colony"
            ],
            [
              "id"=> 1832,
              "name"=> "Barawakumbuka"
            ],
            [
              "id"=> 1833,
              "name"=> "Beliatta"
            ],
            [
              "id"=> 1834,
              "name"=> "Beragama"
            ],
            [
              "id"=> 1835,
              "name"=> "Beralihela"
            ],
            [
              "id"=> 1836,
              "name"=> "Bowalagama"
            ],
            [
              "id"=> 1837,
              "name"=> "Bundala"
            ],
            [
              "id"=> 1838,
              "name"=> "Ellagala"
            ],
            [
              "id"=> 1839,
              "name"=> "Gangulandeniya"
            ],
            [
              "id"=> 1840,
              "name"=> "Getamanna"
            ],
            [
              "id"=> 1841,
              "name"=> "Goda Koggalla"
            ],
            [
              "id"=> 1842,
              "name"=> "Gonagamuwa Uduwila"
            ],
            [
              "id"=> 1843,
              "name"=> "Gonnoruwa"
            ],
            [
              "id"=> 1844,
              "name"=> "Hakuruwela"
            ],
            [
              "id"=> 1845,
              "name"=> "Hambantota"
            ],
            [
              "id"=> 1846,
              "name"=> "Horewelagoda"
            ],
            [
              "id"=> 1847,
              "name"=> "Hungama"
            ],
            [
              "id"=> 1848,
              "name"=> "Ihala Beligalla"
            ],
            [
              "id"=> 1849,
              "name"=> "Ittademaliya"
            ],
            [
              "id"=> 1850,
              "name"=> "Julampitiya"
            ],
            [
              "id"=> 1851,
              "name"=> "Kahandamodara"
            ],
            [
              "id"=> 1852,
              "name"=> "Kariyamaditta"
            ],
            [
              "id"=> 1853,
              "name"=> "Katuwana"
            ],
            [
              "id"=> 1854,
              "name"=> "Kawantissapura"
            ],
            [
              "id"=> 1855,
              "name"=> "Kirama"
            ],
            [
              "id"=> 1856,
              "name"=> "Kirinda"
            ],
            [
              "id"=> 1857,
              "name"=> "Lunama"
            ],
            [
              "id"=> 1858,
              "name"=> "Lunugamwehera"
            ],
            [
              "id"=> 1859,
              "name"=> "Magama"
            ],
            [
              "id"=> 1860,
              "name"=> "Mahagalwewa"
            ],
            [
              "id"=> 1861,
              "name"=> "Mamadala"
            ],
            [
              "id"=> 1862,
              "name"=> "Medamulana"
            ],
            [
              "id"=> 1863,
              "name"=> "Middeniya"
            ],
            [
              "id"=> 1864,
              "name"=> "Migahajandur"
            ],
            [
              "id"=> 1865,
              "name"=> "Modarawana"
            ],
            [
              "id"=> 1866,
              "name"=> "Mulkirigala"
            ],
            [
              "id"=> 1867,
              "name"=> "Nakulugamuwa"
            ],
            [
              "id"=> 1868,
              "name"=> "Netolpitiya"
            ],
            [
              "id"=> 1869,
              "name"=> "Nihiluwa"
            ],
            [
              "id"=> 1870,
              "name"=> "Padawkema"
            ],
            [
              "id"=> 1871,
              "name"=> "Pahala Andarawewa"
            ],
            [
              "id"=> 1872,
              "name"=> "Pallekanda"
            ],
            [
              "id"=> 1873,
              "name"=> "Rammalawarapitiya"
            ],
            [
              "id"=> 1874,
              "name"=> "Ranakeliya"
            ],
            [
              "id"=> 1875,
              "name"=> "Ranmuduwewa"
            ],
            [
              "id"=> 1876,
              "name"=> "Ranna"
            ],
            [
              "id"=> 1877,
              "name"=> "Ratmalwala"
            ],
            [
              "id"=> 1878,
              "name"=> "RU/Ridiyagama"
            ],
            [
              "id"=> 1879,
              "name"=> "Sooriyawewa Town"
            ],
            [
              "id"=> 1880,
              "name"=> "Tangalla"
            ],
            [
              "id"=> 1881,
              "name"=> "Tissamaharama"
            ],
            [
              "id"=> 1882,
              "name"=> "Uda Gomadiya"
            ],
            [
              "id"=> 1883,
              "name"=> "Udamattala"
            ],
            [
              "id"=> 1884,
              "name"=> "Uswewa"
            ],
            [
              "id"=> 1885,
              "name"=> "Vitharandeniya"
            ],
            [
              "id"=> 1886,
              "name"=> "Walasmulla"
            ],
            [
              "id"=> 1887,
              "name"=> "Weeraketiya"
            ],
            [
              "id"=> 1888,
              "name"=> "Weerawila"
            ],
            [
              "id"=> 1889,
              "name"=> "Weerawila NewTown"
            ],
            [
              "id"=> 1890,
              "name"=> "Wekandawela"
            ],
            [
              "id"=> 1891,
              "name"=> "Weligatta"
            ],
            [
              "id"=> 1892,
              "name"=> "Yala"
            ],
            [
              "id"=> 1893,
              "name"=> "Yatigala"
            ],
            [
              "id"=> 1894,
              "name"=> "Akkarasiyaya"
            ],
            [
              "id"=> 1895,
              "name"=> "Aluketiyawa"
            ],
            [
              "id"=> 1896,
              "name"=> "Aluttaramma"
            ],
            [
              "id"=> 1897,
              "name"=> "Ambadandegama"
            ],
            [
              "id"=> 1898,
              "name"=> "Ambagahawatta"
            ],
            [
              "id"=> 1899,
              "name"=> "Ambagasdowa"
            ],
            [
              "id"=> 1900,
              "name"=> "Amunumulla"
            ],
            [
              "id"=> 1901,
              "name"=> "Arawa"
            ],
            [
              "id"=> 1902,
              "name"=> "Arawakumbura"
            ],
            [
              "id"=> 1903,
              "name"=> "Arawatta"
            ],
            [
              "id"=> 1904,
              "name"=> "Atakiriya"
            ],
            [
              "id"=> 1905,
              "name"=> "Badulla"
            ],
            [
              "id"=> 1906,
              "name"=> "Baduluoya"
            ],
            [
              "id"=> 1907,
              "name"=> "Ballaketuwa"
            ],
            [
              "id"=> 1908,
              "name"=> "Bambarapana"
            ],
            [
              "id"=> 1909,
              "name"=> "Bandarawela"
            ],
            [
              "id"=> 1910,
              "name"=> "Beramada"
            ],
            [
              "id"=> 1911,
              "name"=> "Bibilegama"
            ],
            [
              "id"=> 1912,
              "name"=> "Bogahakumbura"
            ],
            [
              "id"=> 1913,
              "name"=> "Boralanda"
            ],
            [
              "id"=> 1914,
              "name"=> "Bowela"
            ],
            [
              "id"=> 1915,
              "name"=> "Dambana"
            ],
            [
              "id"=> 1916,
              "name"=> "Demodara"
            ],
            [
              "id"=> 1917,
              "name"=> "Diganatenna"
            ],
            [
              "id"=> 1918,
              "name"=> "Dikkapitiya"
            ],
            [
              "id"=> 1919,
              "name"=> "Dimbulana"
            ],
            [
              "id"=> 1920,
              "name"=> "Divulapelessa"
            ],
            [
              "id"=> 1921,
              "name"=> "Diyatalawa"
            ],
            [
              "id"=> 1922,
              "name"=> "Dulgolla"
            ],
            [
              "id"=> 1923,
              "name"=> "Egodawela"
            ],
            [
              "id"=> 1924,
              "name"=> "Ella"
            ],
            [
              "id"=> 1925,
              "name"=> "Ettampitiya"
            ],
            [
              "id"=> 1926,
              "name"=> "Galauda"
            ],
            [
              "id"=> 1927,
              "name"=> "Galedanda"
            ],
            [
              "id"=> 1928,
              "name"=> "Galporuyaya"
            ],
            [
              "id"=> 1929,
              "name"=> "Gamewela"
            ],
            [
              "id"=> 1930,
              "name"=> "Gawarawela"
            ],
            [
              "id"=> 1931,
              "name"=> "Girandurukotte"
            ],
            [
              "id"=> 1932,
              "name"=> "Godunna"
            ],
            [
              "id"=> 1933,
              "name"=> "Gurutalawa"
            ],
            [
              "id"=> 1934,
              "name"=> "Haldummulla"
            ],
            [
              "id"=> 1935,
              "name"=> "Hali Ela"
            ],
            [
              "id"=> 1936,
              "name"=> "Hangunnawa"
            ],
            [
              "id"=> 1937,
              "name"=> "Haputale"
            ],
            [
              "id"=> 1938,
              "name"=> "Hebarawa"
            ],
            [
              "id"=> 1939,
              "name"=> "Heeloya"
            ],
            [
              "id"=> 1940,
              "name"=> "Helahalpe"
            ],
            [
              "id"=> 1941,
              "name"=> "Helapupula"
            ],
            [
              "id"=> 1942,
              "name"=> "Hewanakumbura"
            ],
            [
              "id"=> 1943,
              "name"=> "Hingurukaduwa"
            ],
            [
              "id"=> 1944,
              "name"=> "Hopton"
            ],
            [
              "id"=> 1945,
              "name"=> "Idalgashinna"
            ],
            [
              "id"=> 1946,
              "name"=> "Jangulla"
            ],
            [
              "id"=> 1947,
              "name"=> "Kahataruppa"
            ],
            [
              "id"=> 1948,
              "name"=> "Kalubululanda"
            ],
            [
              "id"=> 1949,
              "name"=> "Kalugahakandura"
            ],
            [
              "id"=> 1950,
              "name"=> "Kalupahana"
            ],
            [
              "id"=> 1951,
              "name"=> "Kandaketya"
            ],
            [
              "id"=> 1952,
              "name"=> "Kandegedara"
            ],
            [
              "id"=> 1953,
              "name"=> "Kandepuhulpola"
            ],
            [
              "id"=> 1954,
              "name"=> "Kebillawela"
            ],
            [
              "id"=> 1955,
              "name"=> "Kendagolla"
            ],
            [
              "id"=> 1956,
              "name"=> "Keppetipola"
            ],
            [
              "id"=> 1957,
              "name"=> "Keselpotha"
            ],
            [
              "id"=> 1958,
              "name"=> "Ketawatta"
            ],
            [
              "id"=> 1959,
              "name"=> "Kiriwanagama"
            ],
            [
              "id"=> 1960,
              "name"=> "Koslanda"
            ],
            [
              "id"=> 1961,
              "name"=> "Kotamuduna"
            ],
            [
              "id"=> 1962,
              "name"=> "Kuruwitenna"
            ],
            [
              "id"=> 1963,
              "name"=> "Kuttiyagolla"
            ],
            [
              "id"=> 1964,
              "name"=> "Landewela"
            ],
            [
              "id"=> 1965,
              "name"=> "Liyangahawela"
            ],
            [
              "id"=> 1966,
              "name"=> "Lunugala"
            ],
            [
              "id"=> 1967,
              "name"=> "Lunuwatta"
            ],
            [
              "id"=> 1968,
              "name"=> "Madulsima"
            ],
            [
              "id"=> 1969,
              "name"=> "Mahiyanganaya"
            ],
            [
              "id"=> 1970,
              "name"=> "Makulella"
            ],
            [
              "id"=> 1971,
              "name"=> "Malgoda"
            ],
            [
              "id"=> 1972,
              "name"=> "Maliyadda"
            ],
            [
              "id"=> 1973,
              "name"=> "Mapakadawewa"
            ],
            [
              "id"=> 1974,
              "name"=> "Maspanna"
            ],
            [
              "id"=> 1975,
              "name"=> "Maussagolla"
            ],
            [
              "id"=> 1976,
              "name"=> "Medawela Udukinda"
            ],
            [
              "id"=> 1977,
              "name"=> "Medawelagama"
            ],
            [
              "id"=> 1978,
              "name"=> "Meegahakiula"
            ],
            [
              "id"=> 1979,
              "name"=> "Metigahatenna"
            ],
            [
              "id"=> 1980,
              "name"=> "Mirahawatta"
            ],
            [
              "id"=> 1981,
              "name"=> "Miriyabedda"
            ],
            [
              "id"=> 1982,
              "name"=> "Miyanakandura"
            ],
            [
              "id"=> 1983,
              "name"=> "Namunukula"
            ],
            [
              "id"=> 1984,
              "name"=> "Narangala"
            ],
            [
              "id"=> 1985,
              "name"=> "Nelumgama"
            ],
            [
              "id"=> 1986,
              "name"=> "Nikapotha"
            ],
            [
              "id"=> 1987,
              "name"=> "Nugatalawa"
            ],
            [
              "id"=> 1988,
              "name"=> "Ohiya"
            ],
            [
              "id"=> 1989,
              "name"=> "Pahalarathkinda"
            ],
            [
              "id"=> 1990,
              "name"=> "Pallekiruwa"
            ],
            [
              "id"=> 1991,
              "name"=> "Passara"
            ],
            [
              "id"=> 1992,
              "name"=> "Pathanewatta"
            ],
            [
              "id"=> 1993,
              "name"=> "Pattiyagedara"
            ],
            [
              "id"=> 1994,
              "name"=> "Pelagahatenna"
            ],
            [
              "id"=> 1995,
              "name"=> "Perawella"
            ],
            [
              "id"=> 1996,
              "name"=> "Pitamaruwa"
            ],
            [
              "id"=> 1997,
              "name"=> "Pitapola"
            ],
            [
              "id"=> 1998,
              "name"=> "Puhulpola"
            ],
            [
              "id"=> 1999,
              "name"=> "Ratkarawwa"
            ],
            [
              "id"=> 2000,
              "name"=> "Ridimaliyadda"
            ],
            [
              "id"=> 2001,
              "name"=> "Rilpola"
            ],
            [
              "id"=> 2002,
              "name"=> "Silmiyapura"
            ],
            [
              "id"=> 2003,
              "name"=> "Sirimalgoda"
            ],
            [
              "id"=> 2004,
              "name"=> "Sorabora Colony"
            ],
            [
              "id"=> 2005,
              "name"=> "Soragune"
            ],
            [
              "id"=> 2006,
              "name"=> "Soranatota"
            ],
            [
              "id"=> 2007,
              "name"=> "Spring Valley"
            ],
            [
              "id"=> 2008,
              "name"=> "Taldena"
            ],
            [
              "id"=> 2009,
              "name"=> "Tennepanguwa"
            ],
            [
              "id"=> 2010,
              "name"=> "Timbirigaspitiya"
            ],
            [
              "id"=> 2011,
              "name"=> "Uduhawara"
            ],
            [
              "id"=> 2012,
              "name"=> "Uraniya"
            ],
            [
              "id"=> 2013,
              "name"=> "Uva Deegalla"
            ],
            [
              "id"=> 2014,
              "name"=> "Uva Karandagolla"
            ],
            [
              "id"=> 2015,
              "name"=> "Uva Mawelagama"
            ],
            [
              "id"=> 2016,
              "name"=> "Uva Tenna"
            ],
            [
              "id"=> 2017,
              "name"=> "Uva Tissapura"
            ],
            [
              "id"=> 2018,
              "name"=> "Uva Uduwara"
            ],
            [
              "id"=> 2019,
              "name"=> "Uvaparanagama"
            ],
            [
              "id"=> 2020,
              "name"=> "Welimada"
            ],
            [
              "id"=> 2021,
              "name"=> "Wewatta"
            ],
            [
              "id"=> 2022,
              "name"=> "Wineethagama"
            ],
            [
              "id"=> 2023,
              "name"=> "Yalagamuwa"
            ],
            [
              "id"=> 2024,
              "name"=> "Yalwela"
            ],
            [
              "id"=> 2025,
              "name"=> "Angunakolawewa"
            ],
            [
              "id"=> 2026,
              "name"=> "Ayiwela"
            ],
            [
              "id"=> 2027,
              "name"=> "Badalkumbura"
            ],
            [
              "id"=> 2028,
              "name"=> "Baduluwela"
            ],
            [
              "id"=> 2029,
              "name"=> "Bakinigahawela"
            ],
            [
              "id"=> 2030,
              "name"=> "Balaharuwa"
            ],
            [
              "id"=> 2031,
              "name"=> "Bibile"
            ],
            [
              "id"=> 2032,
              "name"=> "Boragas"
            ],
            [
              "id"=> 2033,
              "name"=> "Buddama"
            ],
            [
              "id"=> 2034,
              "name"=> "Buttala"
            ],
            [
              "id"=> 2035,
              "name"=> "Dambagalla"
            ],
            [
              "id"=> 2036,
              "name"=> "Diyakobala"
            ],
            [
              "id"=> 2037,
              "name"=> "Dombagahawela"
            ],
            [
              "id"=> 2038,
              "name"=> "Ekamutugama"
            ],
            [
              "id"=> 2039,
              "name"=> "Ekiriyankumbura"
            ],
            [
              "id"=> 2040,
              "name"=> "Ethimalewewa"
            ],
            [
              "id"=> 2041,
              "name"=> "Ettiliwewa"
            ],
            [
              "id"=> 2042,
              "name"=> "Galabedda"
            ],
            [
              "id"=> 2043,
              "name"=> "Hambegamuwa"
            ],
            [
              "id"=> 2044,
              "name"=> "Hulandawa"
            ],
            [
              "id"=> 2045,
              "name"=> "Inginiyagala"
            ],
            [
              "id"=> 2046,
              "name"=> "Kandaudapanguwa"
            ],
            [
              "id"=> 2047,
              "name"=> "Kandawinna"
            ],
            [
              "id"=> 2048,
              "name"=> "Kataragama"
            ],
            [
              "id"=> 2049,
              "name"=> "Kiriibbanwewa"
            ],
            [
              "id"=> 2050,
              "name"=> "Kotagama"
            ],
            [
              "id"=> 2051,
              "name"=> "Kotawehera Mankada"
            ],
            [
              "id"=> 2052,
              "name"=> "Kotiyagala"
            ],
            [
              "id"=> 2053,
              "name"=> "Kumbukkana"
            ],
            [
              "id"=> 2054,
              "name"=> "Mahagama Colony"
            ],
            [
              "id"=> 2055,
              "name"=> "Marawa"
            ],
            [
              "id"=> 2056,
              "name"=> "Mariarawa"
            ],
            [
              "id"=> 2057,
              "name"=> "Medagana"
            ],
            [
              "id"=> 2058,
              "name"=> "Monaragala"
            ],
            [
              "id"=> 2059,
              "name"=> "Moretuwegama"
            ],
            [
              "id"=> 2060,
              "name"=> "Nakkala"
            ],
            [
              "id"=> 2061,
              "name"=> "Nannapurawa"
            ],
            [
              "id"=> 2062,
              "name"=> "Nelliyadda"
            ],
            [
              "id"=> 2063,
              "name"=> "Nilgala"
            ],
            [
              "id"=> 2064,
              "name"=> "Obbegoda"
            ],
            [
              "id"=> 2065,
              "name"=> "Okkampitiya"
            ],
            [
              "id"=> 2066,
              "name"=> "Pangura"
            ],
            [
              "id"=> 2067,
              "name"=> "Pitakumbura"
            ],
            [
              "id"=> 2068,
              "name"=> "Randeniya"
            ],
            [
              "id"=> 2069,
              "name"=> "Ruwalwela"
            ],
            [
              "id"=> 2070,
              "name"=> "Sella Kataragama"
            ],
            [
              "id"=> 2071,
              "name"=> "Sewanagala"
            ],
            [
              "id"=> 2072,
              "name"=> "Siyambalagune"
            ],
            [
              "id"=> 2073,
              "name"=> "Siyambalanduwa"
            ],
            [
              "id"=> 2074,
              "name"=> "Suriara"
            ],
            [
              "id"=> 2075,
              "name"=> "Tanamalwila"
            ],
            [
              "id"=> 2076,
              "name"=> "Uva Gangodagama"
            ],
            [
              "id"=> 2077,
              "name"=> "Uva Kudaoya"
            ],
            [
              "id"=> 2078,
              "name"=> "Uva Pelwatta"
            ],
            [
              "id"=> 2079,
              "name"=> "Warunagama"
            ],
            [
              "id"=> 2080,
              "name"=> "Wedikumbura"
            ],
            [
              "id"=> 2081,
              "name"=> "Weherayaya Handapanagala"
            ],
            [
              "id"=> 2082,
              "name"=> "Wellawaya"
            ],
            [
              "id"=> 2083,
              "name"=> "Wilaoya"
            ],
            [
              "id"=> 2084,
              "name"=> "Sarikkamulla"
            ],
            [
              "id"=> 2085,
              "name"=> "Heenatigala"
            ],
            [
              "id"=> 2086,
              "name"=> "Mraketiara"
            ],
            [
              "id"=> 2087,
              "name"=> "Mirihana"
            ],
            [
              "id"=> 2088,
              "name"=> "Mahara"
            ]
          ];
        DB::table('cities')->insert($data);
    }
}
