<?php
namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class RoleUserTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'user_id' => 1,
                'role_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            
        ];
        DB::table('role_user')->insert($data);

    }
}
