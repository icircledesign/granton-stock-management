<?php
namespace Database\Seeders;

use DB;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionRoleTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionCount = Permission::count();
        $permissionRole = [];
        for ($i=1; $i <= $permissionCount; $i++) { 
            $item = array(
                'role_id' => 1,
                'permission_id' => $i
            );
            $permissionRole[] = $item;
        }
        DB::table('permission_role')->insert($permissionRole);
        $data = [
            ['role_id'=>2,'permission_id'=>1],
            ['role_id'=>3,'permission_id'=>1],
            ['role_id'=>4,'permission_id'=>1],
            ['role_id'=>5,'permission_id'=>1]
        ];
        DB::table('permission_role')->insert($data);
        DB::table('permission_role')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
