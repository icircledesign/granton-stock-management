<?php
namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'name' => 'Sameera Madushan',
                'email' => 'sameeramadushan0812@gmail.com',
                'password' => bcrypt('password'),
                'image' => 'avatar.jpg',
                'status_id' => 1,
                'email_verified_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by'=> 1,
                'updated_by'=> 1
            ]

        ];
        DB::table('users')->insert($data);
    }
}
