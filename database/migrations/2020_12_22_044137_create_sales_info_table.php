<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_info', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('promotion_card_id');
            $table->string('customer_name');
            $table->string('phone_no');
            $table->string('nic');
            $table->string('email');
            $table->string('address');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('distributor_id');
            $table->unsignedInteger('status_id')->default(1);
            $table->timestamps();
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by')->nullable();
            $table->foreign('promotion_card_id')->references('id')->on('promotion_cards')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_info');
    }
}
