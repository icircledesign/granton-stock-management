<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
    Route::get('/grns/new',[
        'as' => 'New GRN',
        'uses' => 'GRNController@create'
    ]);

    Route::post('/grns/store', [
    	'as' => 'New GRN',
    	'uses' => 'GRNController@store'
    ]);//ajax
    Route::get('/grns', [
    	'as' => 'GRN History',
    	'uses' => 'GRNController@index'
    ]);

    Route::get('/grns/get_grn_list', [
    	'as' => 'GRN History',
    	'uses' => 'GRNController@getGRNList'
    ]);//ajax

});
