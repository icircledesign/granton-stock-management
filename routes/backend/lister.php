<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/listers', [
    	'as' => 'View Listers', 
    	'uses' => 'ListerController@index'
    ]);

    Route::get('/listers/get_lister_list', [
    	'as' => 'View Listers', 
    	'uses' => 'ListerController@getListerList'
    ]);//ajax

    Route::get('/listers/create',[
        'as' => 'Create Lister',
        'uses' => 'ListerController@create'
    ]);

    Route::post('/listers/store', [
    	'as' => 'Create Lister', 
    	'uses' => 'ListerController@store'
    ]);//ajax

    Route::get('/listers/{id}/edit', [
        'as' => 'Edit Lister',
        'uses' => 'ListerController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/listers/update', [
        'as' => 'Edit Lister',
        'uses' => 'ListerController@update',
    ]);//ajax

    
});
