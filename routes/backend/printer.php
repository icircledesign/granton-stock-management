<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {

    Route::get('/printers', [
    	'as' => 'View Printers',
    	'uses' => 'PrinterController@index'
    ]);

    Route::get('/printers/get_printer_list', [
    	'as' => 'View Printers',
    	'uses' => 'PrinterController@getPrinterList'
    ]);//ajax

    Route::get('/printers/create',[
        'as' => 'Create Printer',
        'uses' => 'PrinterController@create'
    ]);

    Route::post('/printers/store', [
    	'as' => 'Create Printer',
    	'uses' => 'PrinterController@store'
    ]);//ajax

    Route::get('/printers/{id}/edit', [
        'as' => 'Edit Printer',
        'uses' => 'PrinterController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/printers/update', [
        'as' => 'Edit Printer',
        'uses' => 'PrinterController@update',
    ]);//ajax


});
