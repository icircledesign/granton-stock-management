<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/managers', [
    	'as' => 'View Managers', 
    	'uses' => 'ManagerController@index'
    ]);

    Route::get('/managers/get_manager_list', [
    	'as' => 'View Managers', 
    	'uses' => 'ManagerController@getManagerList'
    ]);//ajax

    Route::get('/managers/create',[
        'as' => 'Create Manager',
        'uses' => 'ManagerController@create'
    ]);

    Route::post('/managers/store', [
    	'as' => 'Create Manager', 
    	'uses' => 'ManagerController@store'
    ]);//ajax

    Route::get('/managers/{id}/edit', [
        'as' => 'Edit Manager',
        'uses' => 'ManagerController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/managers/update', [
        'as' => 'Edit Manager',
        'uses' => 'ManagerController@update',
    ]);//ajax

    
});
