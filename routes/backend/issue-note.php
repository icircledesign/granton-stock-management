<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
    Route::get('/issue-notes/new',[
        'as' => 'New Issue Note',
        'uses' => 'IssueNoteController@create'
    ]);

    Route::post('/issue-notes/store', [
    	'as' => 'New Issue Note',
    	'uses' => 'IssueNoteController@store'
    ]);//ajax
    Route::get('/issue-notes', [
    	'as' => 'Issue Note History',
    	'uses' => 'IssueNoteController@index'
    ]);

    Route::get('/issue-notes/get_issue_note_list', [
    	'as' => 'Issue Note History',
    	'uses' => 'IssueNoteController@getIssueNoteList'
    ]);//ajax



});
