<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {

    Route::get('/update-profile', [
    	'as' => 'Update Profile',
    	'uses' => 'UpdateProfileController@index'
    ]);

    Route::patch('/update-profile/update', [
        'as' => 'Update Profile',
        'uses' => 'UpdateProfileController@update',
    ]);//ajax
});

Route::group(['middleware' => ['auth','verified']], function () {

    Route::get('/logout', function () {
        Auth::logout();
        Session::flush();
        return redirect()->route('login');
    });

});
