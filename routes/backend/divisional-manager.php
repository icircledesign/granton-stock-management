<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/divisional-managers', [
    	'as' => 'View Divisional Managers', 
    	'uses' => 'DivisionalManagerController@index'
    ]);

    Route::get('/divisional-managers/get_divisionalManager_list', [
    	'as' => 'View Divisional Managers', 
    	'uses' => 'DivisionalManagerController@getDivisionalManagerList'
    ]);//ajax

    Route::get('/divisional-managers/create',[
        'as' => 'Create Divisional Manager',
        'uses' => 'DivisionalManagerController@create'
    ]);

    Route::post('/divisional-managers/store', [
    	'as' => 'Create Divisional Manager', 
    	'uses' => 'DivisionalManagerController@store'
    ]);//ajax

    Route::get('/divisional-managers/{id}/edit', [
        'as' => 'Edit Divisional Manager',
        'uses' => 'DivisionalManagerController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/divisional-managers/update', [
        'as' => 'Edit Divisional Manager',
        'uses' => 'DivisionalManagerController@update',
    ]);//ajax

    
});
