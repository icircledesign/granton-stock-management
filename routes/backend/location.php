<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/locations', [
    	'as' => 'View Locations', 
    	'uses' => 'LocationController@index'
    ]);

    Route::get('/locations/get_location_list', [
    	'as' => 'View Locations', 
    	'uses' => 'LocationController@getLocationList'
    ]);//ajax

    Route::get('/locations/create',[
        'as' => 'Create Location',
        'uses' => 'LocationController@create'
    ]);

    Route::post('/locations/store', [
    	'as' => 'Create Location', 
    	'uses' => 'LocationController@store'
    ]);//ajax

    Route::get('/locations/{id}/edit', [
        'as' => 'Edit Location',
        'uses' => 'LocationController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/locations/update', [
        'as' => 'Edit Location',
        'uses' => 'LocationController@update',
    ]);//ajax

    
});
