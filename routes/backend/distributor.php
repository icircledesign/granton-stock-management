<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {

    Route::get('/distributors', [
    	'as' => 'View Distributors',
    	'uses' => 'DistributorController@index'
    ]);

    Route::get('/distributors/get_distributor_list', [
    	'as' => 'View Distributors',
    	'uses' => 'DistributorController@getDistributorList'
    ]);//ajax

    Route::get('/distributors/create',[
        'as' => 'Create Distributor',
        'uses' => 'DistributorController@create'
    ]);

    Route::post('/distributors/store', [
    	'as' => 'Create Distributor',
    	'uses' => 'DistributorController@store'
    ]);//ajax

    Route::get('/distributors/{id}/edit', [
        'as' => 'Edit Distributor',
        'uses' => 'DistributorController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/distributors/update', [
        'as' => 'Edit Distributor',
        'uses' => 'DistributorController@update',
    ]);//ajax


});
