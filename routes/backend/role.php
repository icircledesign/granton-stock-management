<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/roles', [
    	'as' => 'View Roles', 
    	'uses' => 'RoleController@index'
    ]);

    Route::get('/roles/get_role_list', [
    	'as' => 'View Roles', 
    	'uses' => 'RoleController@getRoleList'
    ]);//ajax

    Route::get('/roles/create',[
        'as' => 'Create Role',
        'uses' => 'RoleController@create'
    ]);

    Route::post('/roles/store', [
    	'as' => 'Create Role', 
    	'uses' => 'RoleController@store'
    ]);//ajax

    Route::get('/roles/{id}/edit', [
        'as' => 'Edit Role',
        'uses' => 'RoleController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/roles/update', [
        'as' => 'Edit Role',
        'uses' => 'RoleController@update',
    ]);//ajax

    
});
