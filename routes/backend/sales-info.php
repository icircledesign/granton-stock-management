<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
    Route::get('/sales-info/new',[
        'as' => 'New Sales Info',
        'uses' => 'SalesInfoController@create'
    ]);

    Route::post('/sales-info/get_serial_numbers', [
    	'as' => 'New Sales Info',
    	'uses' => 'SalesInfoController@getSerialNumbers'
    ]);//ajax

    Route::post('/sales-info/store', [
    	'as' => 'New Sales Info',
    	'uses' => 'SalesInfoController@store'
    ]);//ajax
    Route::get('/sales-info', [
    	'as' => 'Sales Info History',
    	'uses' => 'SalesInfoController@index'
    ]);

    Route::get('/sales-info/get_issue_note_list', [
    	'as' => 'Sales Info History',
    	'uses' => 'SalesInfoController@getIssueNoteList'
    ]);//ajax



});
