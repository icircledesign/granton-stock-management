<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/permissions', [
    	'as' => 'Register Permissions', 
    	'uses' => 'PermissionController@index'
    ]);


    Route::post('/permissions/store', [
    	'as' => 'Register Permissions', 
    	'uses' => 'PermissionController@store'
    ]);//ajax

    
});
