<?php
Route::group(['middleware' => ['auth','UserPermission','verified']], function () {
	
    Route::get('/promotions', [
    	'as' => 'View Promotions', 
    	'uses' => 'PromotionController@index'
    ]);

    Route::get('/promotions/get_promotion_list', [
    	'as' => 'View Promotions', 
    	'uses' => 'PromotionController@getPromotionList'
    ]);//ajax

    Route::get('/promotions/create',[
        'as' => 'Create Promotion',
        'uses' => 'PromotionController@create'
    ]);

    Route::post('/promotions/store', [
    	'as' => 'Create Promotion', 
    	'uses' => 'PromotionController@store'
    ]);//ajax

    Route::get('/promotions/{id}/edit', [
        'as' => 'Edit Promotion',
        'uses' => 'PromotionController@edit',
    ])->where('id', '[0-9]+');

    Route::patch('/promotions/update', [
        'as' => 'Edit Promotion',
        'uses' => 'PromotionController@update',
    ]);//ajax

    
});
